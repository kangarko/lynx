package org.mineacademy.lynx.api;

import org.bukkit.Bukkit;
import org.bukkit.entity.Entity;
import org.bukkit.event.Cancellable;
import org.bukkit.event.HandlerList;
import org.mineacademy.fo.Common;
import org.mineacademy.fo.event.SimpleEvent;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

/**
 * An example of creating a custom event. Custom events are never called,
 * you must call them using {@link Common#callEvent(org.bukkit.event.Event)} or native Bukkit
 * {@link Bukkit#getPluginManager()}.callEvent() method.
 *
 * Every custom event class "extends SimpleEvent" and optionally "implements Cancellable" if you want
 * plugin developers able to cancel your event (and possily, game action associated with it).
 */
@Getter
@Setter
// Generates constructor to create a new event (this does not do anything really, you need to call this, see above.
// Don't forget to store the event as variable before you call the event and then use the fields in the class
// to get their modified values in case other plugins modify your explosion power, for example.
@RequiredArgsConstructor
public final class CowExplodeEvent extends SimpleEvent implements Cancellable {

	// MANDATORY: Every custom event must have the handlers list, this makes Bukkit remember
	// which plugins listen to this event (Bukkit will do the rest for you, just have this field here)
	//
	// PLEASE ALSO SEE THE BOTTOM OF THIS FILE FOR 2 MANDATORY METHODS
	private static final HandlerList handlers = new HandlerList();

	/**
	 * The entity about to explode
	 */
	private final Entity cow;

	/**
	 * The power of the explosion (20 is TNT)
	 */
	private float power = 5;

	/**
	 * Cancel the explosion? Don't forget to call your event BEFORE the explosion to make it cancelable.
	 */
	private boolean cancelled;

	// MANDATORY: Every event class must have this method
	@Override
	public HandlerList getHandlers() {
		return handlers;
	}

	// MANDATORY: Every event class must have this method
	public static HandlerList getHandlerList() {
		return handlers;
	}
}
