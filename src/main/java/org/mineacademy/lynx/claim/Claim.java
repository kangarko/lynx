package org.mineacademy.lynx.claim;

import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.UUID;

import javax.annotation.Nullable;

import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.mineacademy.fo.Common;
import org.mineacademy.fo.Valid;
import org.mineacademy.fo.region.Region;
import org.mineacademy.fo.settings.ConfigItems;
import org.mineacademy.fo.settings.YamlConfig;
import org.mineacademy.fo.visual.VisualizedRegion;

import lombok.Getter;
import lombok.NonNull;

/**
 * The permanent server cache storing disk data.
 */
@Getter
public final class Claim extends YamlConfig {

	private static final ConfigItems<Claim> loadedClaims = ConfigItems.fromFolder("claims", Claim.class);

	private UUID ownerId;
	private String ownerName;
	private VisualizedRegion region;

	/*
	 * Create a new location, used when loading from disk
	 */
	private Claim(String name) {
		this(name, null, null);
	}

	/*
	 * Create a new location, used when creating new
	 */
	private Claim(String name, @Nullable Player owner, @Nullable VisualizedRegion region) {
		this.ownerId = owner == null ? null : owner.getUniqueId();
		this.ownerName = owner == null ? null : owner.getName();
		this.region = region;

		setHeader(
				Common.configLine(),
				"This file stores a single claim.",
				Common.configLine() + "\n");

		loadConfiguration(NO_DEFAULT, "claims/" + name + ".yml");
	}

	/**
	 * @see YamlConfig#onLoad()
	 */
	@Override
	protected void onLoad() {

		// Only load if not created via command
		if (this.region != null) {
			this.save();

			return;
		}

		this.ownerId = get("Owner_UUID", UUID.class);
		this.ownerName = getString("Owner_Name");
		this.region = get("Region", VisualizedRegion.class);
	}

	/**
	 * @see YamlConfig#serialize()
	 */
	@Override
	protected void onSave() {
		this.set("Owner_UUID", this.ownerId);
		this.set("Owner_Name", this.ownerName);
		this.set("Region", this.region);
	}

	public void setRegion(@NonNull VisualizedRegion region) {
		Valid.checkBoolean(region.isWhole(), "Can only set claim region that is whole");
		this.region = region;

		this.save();
	}

	public void setOwner(Player owner) {
		this.ownerId = owner.getUniqueId();
		this.ownerName = owner.getName();

		this.save();
	}

	public Location getCenter() {
		return this.region.getCenter();
	}

	@Override
	public boolean equals(Object obj) {
		return obj instanceof Claim && ((Claim) obj).getRegion().equals(this.region) && ((Claim) obj).getName().equals(this.getName());
	}

	@Override
	public int hashCode() {
		return Objects.hash(this.region, this.getName());
	}

	/* ------------------------------------------------------------------------------- */
	/* Static */
	/* ------------------------------------------------------------------------------- */

	/**
	 * @param name
	 * @param owner
	 * @param region
	 * @return
	 * @see ConfigItems#loadOrCreateItem(String)
	 */
	public static Claim createClaim(@NonNull String name, Player owner, @NonNull VisualizedRegion region) {
		Valid.checkBoolean(region.isWhole(), "Can only create claim from whole regions");

		return loadedClaims.loadOrCreateItem(name, () -> new Claim(name, owner, region));
	}

	/**
	 * @see ConfigItems#loadItems()
	 */
	public static void loadClaims() {
		loadedClaims.loadItems();
	}

	/**
	 * @param item
	 * @see ConfigItems#removeItem(YamlConfig)
	 */
	public static void removeClaim(Claim item) {
		loadedClaims.removeItem(item);
	}

	/**
	 * @param name
	 * @return
	 * @see ConfigItems#isItemLoaded(String)
	 */
	public static boolean isClaimLoaded(String name) {
		return loadedClaims.isItemLoaded(name);
	}

	/**
	 * @param name
	 * @return
	 * @see ConfigItems#findItem(String)
	 */
	public static Claim findClaim(@NonNull String name) {
		return loadedClaims.findItem(name);
	}

	public static Claim findClaim(@NonNull Location location) {
		for (Claim otherItem : getClaims())
			if (otherItem.getRegion().isWithin(location))
				return otherItem;

		return null;
	}

	/**
	 * Return a list of Bukkit locations.
	 *
	 * @return
	 */
	public static List<Region> getRegions() {
		return Common.convert(getClaims(), Claim::getRegion);
	}

	/**
	 * @return
	 * @see ConfigItems#getItems()
	 */
	public static Collection<Claim> getClaims() {
		return loadedClaims.getItems();
	}

	/**
	 * @return
	 * @see ConfigItems#getItemNames()
	 */
	public static Set<String> getClaimsNames() {
		return loadedClaims.getItemNames();
	}
}
