package org.mineacademy.lynx.hook;

import org.bukkit.event.Listener;
import org.mineacademy.fo.plugin.SimplePlugin;

import de.slikey.effectlib.EffectManager;

// Example of using an external library
// Credits and tutorial here: https://dev.bukkit.org/projects/effectlib
public final class EffectLibHook implements Listener {

	private final EffectManager effectManager;

	public EffectLibHook() {
		this.effectManager = new EffectManager(SimplePlugin.getInstance());
	}

	public void shutdown() {
		this.effectManager.dispose();
	}

	/*@EventHandler
	public void onPlayerJoin(final PlayerJoinEvent event) {

		final TextEffect effect = new TextEffect(this.effectManager);

		effect.particle = Particle.VILLAGER_HAPPY;
		effect.period = 10;
		effect.iterations = -1;
		effect.font = new Font("Arial", Font.BOLD, 35);
		effect.realtime = true;
		effect.size = (float) 1 / 6;

		effect.setLocation(event.getPlayer().getLocation().add(0, 2, 0));
		effect.start();

		final DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");

		Common.runTimer(20, new SimpleRunnable() {

			@Override
			public void run() {

				// Update the effect here
				effect.text = dateFormat.format(System.currentTimeMillis());

				if (!event.getPlayer().isOnline()) {

					// Cancel the effect
					effect.cancel();

					// Cancel this repeating task
					this.cancel();
				}
			}
		});
	}*/
}