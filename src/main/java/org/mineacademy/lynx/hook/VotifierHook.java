package org.mineacademy.lynx.hook;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.mineacademy.fo.Common;
import org.mineacademy.fo.Messenger;
import org.mineacademy.fo.TimeUtil;
import org.mineacademy.fo.menu.model.ItemCreator;
import org.mineacademy.fo.model.SimpleTime;
import org.mineacademy.fo.remain.CompMaterial;
import org.mineacademy.lynx.database.LynxAdvancedDatabase;
import org.mineacademy.lynx.model.PlayerVote;
import org.mineacademy.lynx.settings.PlayerData;

import com.vexsoftware.votifier.model.Vote;
import com.vexsoftware.votifier.model.VotifierEvent;

public final class VotifierHook implements Listener {

	private final LynxAdvancedDatabase database = LynxAdvancedDatabase.getInstance();

	@EventHandler
	public void onVote(VotifierEvent event) {

		Vote vote = event.getVote();
		Player player = Bukkit.getPlayer(vote.getUsername());
		boolean isOnline = player != null && player.isOnline();

		PlayerVote wrappedVote = PlayerVote.create(vote.getUsername(), vote.getServiceName(), isOnline ? PlayerVote.Status.GIVEN : PlayerVote.Status.PENDING);
		System.out.println("Received vote from " + vote.getUsername() + " at " + vote.getServiceName());

		Common.runLaterAsync(10, () -> {
			this.database.addVote(wrappedVote);

			if (isOnline)
				this.giveRewards(player);
		});
	}

	@EventHandler
	public void onJoin(PlayerJoinEvent event) {
		Player player = event.getPlayer();

		Common.runAsync(() -> {
			int pendingVotes = this.database.countPendingVotes(player);

			if (pendingVotes > 0) {
				System.out.println("Found " + pendingVotes + " for " + player.getName());

				this.database.convertPendingVotesToGiven(player);
				this.giveRewards(player);

			} else
				System.out.println("No pending votes for " + player.getName());
		});
	}

	private void giveRewards(Player player) {
		PlayerData cache = PlayerData.from(player);
		int voteAmount = this.database.pollVotes(player, PlayerVote.Status.GIVEN).size();

		if (cache.getActiveQuest() != null)
			cache.getActiveQuest().onVote(player);

		if (voteAmount == 1) {
			Messenger.success(player, "Thank you for voting for our server for the first time!");

			CompMaterial.DIAMOND.give(player);

		} else if (voteAmount < 3) {
			Messenger.success(player, "(2) Thank you for voting for our server for the " + voteAmount + ". time!");

			//PlayerData.from(player).getRank().forceUpgrade(player);

		} else {

			long lastVoteTime = cache.getLastVoteRewardTime();
			long threshold = SimpleTime.from("15 seconds").getTimeMilliseconds();
			long delay = System.currentTimeMillis() - lastVoteTime;

			if (lastVoteTime == 0 || delay > threshold) {
				Messenger.success(player, "(3) Thank you for voting for our server for the " + voteAmount + ". time!");

				ItemCreator.of(CompMaterial.BEACON, "&4Custom Beacon").give(player);
				cache.setLastVoteRewardTime(System.currentTimeMillis());

			} else
				Messenger.success(player, "(4) Thank you for voting, but you must wait "
						+ TimeUtil.formatTimeGeneric((threshold - delay) / 1000) + " before receiving vote reward again.");
		}
	}
}
