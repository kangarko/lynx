package org.mineacademy.lynx.hook;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import org.bukkit.GameMode;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.mineacademy.fo.Common;
import org.mineacademy.fo.Messenger;
import org.mineacademy.fo.PlayerUtil;
import org.mineacademy.fo.model.DiscordListener;
import org.mineacademy.fo.remain.Remain;
import org.mineacademy.lynx.ChannelManager;

import github.scarsz.discordsrv.DiscordSRV;
import github.scarsz.discordsrv.api.events.DiscordGuildMessagePreProcessEvent;
import github.scarsz.discordsrv.api.events.GameChatMessagePreProcessEvent;
import github.scarsz.discordsrv.dependencies.jda.api.entities.Member;
import github.scarsz.discordsrv.dependencies.jda.api.entities.Message;
import github.scarsz.discordsrv.dependencies.jda.api.entities.TextChannel;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class DiscordSRVHook extends DiscordListener {

	@Getter
	private static final DiscordSRVHook instance = new DiscordSRVHook();

	// Minecraft > Discord
	@Override
	protected void onMessageSent(GameChatMessagePreProcessEvent event) {
	}

	// Discord > Minecraft
	@Override
	protected void onMessageReceived(DiscordGuildMessagePreProcessEvent event) {
		TextChannel channel = event.getChannel();
		Member member = event.getMember();
		Message message = event.getMessage();
		String chatMessage = message.getContentDisplay();

		// Example 1: Filtering profanity

		List<String> curseWords = Arrays.asList("trump is good", "biden is good", "hillary is good", "fuck");

		System.out.println("received message");

		for (String curseWord : curseWords)
			if (curseWord.equalsIgnoreCase(chatMessage)) {
				//message.delete().complete();
				//Message warningMessage = channel.sendMessage("PLEASE DO NOT CURSE OR USE INAPPROPRIATE WORDS HERE!").complete();
				//warningMessage.delete().completeAfter(3, TimeUnit.SECONDS);

				// Shortcut for the three complicated methods above
				this.removeAndWarn("(2) PLEASE DO NOT CURSE OR USE INAPPROPRIATE WORDS HERE!");
			}

		// Example 2: Running commands on your Minecraft server invoked from Discord
		if (chatMessage.equalsIgnoreCase("/creative")) {
			String permission = "lynx.discordcommand.creative";
			Player player = null;

			// Support Discord linking feature for players that have different discord name from their Minecraft one
			final UUID linkedId = DiscordSRV.getPlugin().getAccountLinkManager().getUuid(event.getAuthor().getId());
			//String minecraftName = Common.getOrDefaultStrict(event.getMember().getEffectiveName(), event.getAuthor().getName());

			OfflinePlayer offlinePlayer = null;

			if (linkedId != null) {

				// May involve a blocking request, which is fine, since Discord calls this async
				offlinePlayer = Remain.getOfflinePlayerByUUID(linkedId);

				if (offlinePlayer != null && offlinePlayer.isOnline())
					player = offlinePlayer.getPlayer();
			}

			if (player == null)
				player = this.findPlayer(Common.getOrDefaultStrict(event.getMember().getEffectiveName(), event.getAuthor().getName()),
						"You are not connected to the Minecraft server with the username " + member.getEffectiveName());

			this.checkBoolean(PlayerUtil.hasPerm(player, permission), "Insufficient permission (" + permission + ").");

			final Player player1 = player;
			Common.runLater(() -> player1.setGameMode(GameMode.CREATIVE));

			this.removeAndWarn("Your gamemode has been updated to creative.");
		}

		// Example 3: Clear a chat channel

		if (chatMessage.equalsIgnoreCase("/clear")) {
			this.checkBoolean(this.hasRole(event.getMember(), "beachboy"), "Only moderators and admins can clear chat history.");

			for (Message oldMessage : channel.getIterableHistory().complete()) {
				long distanceFromCurrentTime = (System.currentTimeMillis() / 1000) - oldMessage.getTimeCreated().toEpochSecond();

				if (distanceFromCurrentTime > 30 * 24 * 3600) // only remove messages older than 30 days
					oldMessage.delete().complete();
			}

			this.returnHandled("Channel has been cleared off of old messages.");
		}

		// Example 4: Checking for role and preventing chatting if no role

		if (channel.getName().equalsIgnoreCase("boyztalk") && !this.hasRole(member, "beachboy"))
			this.removeAndWarn("Only beach boys can post to this channel pal ;)");

		// Example 5: Broadcast message

		if (channel.getName().equalsIgnoreCase("general"))
			for (Player online : Remain.getOnlinePlayers())
				Common.tellNoPrefix(online, "&8[&3Discord&8 = &3" + channel.getName() + "&8] &7" + member.getEffectiveName() + "&8: &f" + chatMessage);
	}

	@EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
	public void onMinecraftChat(AsyncPlayerChatEvent event) {
		Player player = event.getPlayer();
		String message = event.getMessage();

		String channelName = ChannelManager.getChannel(player);
		List<TextChannel> matchingChannels = this.findChannels(channelName);

		if (matchingChannels.size() != 1) {
			Messenger.warn(player, "Your message could not be delivered to Discord because channel '" + channelName + "' is not linked: "
					+ Common.join(this.getChannelsNames()));

			return;
		}

		//this.sendMessage(channelName, player.getName() + ": " + message);
		//this.sendMessage(player, channelName, message);
		this.sendWebhookMessage(player, channelName, message);
	}

}
