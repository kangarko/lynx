package org.mineacademy.lynx.conversation;

import org.bukkit.conversations.ConversationContext;
import org.bukkit.conversations.Prompt;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.mineacademy.fo.Valid;
import org.mineacademy.fo.conversation.SimplePrompt;
import org.mineacademy.fo.remain.CompSound;

public class ExpPrompt extends SimplePrompt {

	/*public ExpPrompt() {
		super(false);
	}*/

	@Override
	protected String getPrompt(ConversationContext context) {
		return "&6Write the amount of levels you want to receive.";
	}

	@Override
	protected boolean isInputValid(ConversationContext context, String input) {
		if (!Valid.isInteger(input))
			return false;

		final int level = Integer.parseInt(input);
		return level > 0 && level < 9000;
	}

	@Override
	protected String getFailedValidationText(ConversationContext context, String invalidInput) {
		return "Invalid level: '" + invalidInput + "'. Only enter whole numbers";
	}

	@Nullable
	@Override
	protected Prompt acceptValidatedInput(@NotNull ConversationContext context, @NotNull String input) {
		final int level = Integer.parseInt(input);
		final Player player = this.getPlayer(context);

		player.setLevel(level);
		CompSound.ENTITY_PLAYER_LEVELUP.play(player);

		tell("&6You now have &7" + player.getLevel() + "&6 experience levels!");
		return END_OF_CONVERSATION;
	}
}
