package org.mineacademy.lynx.menu;

import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.inventory.ItemStack;
import org.mineacademy.fo.BungeeUtil;
import org.mineacademy.fo.Common;
import org.mineacademy.fo.menu.Menu;
import org.mineacademy.fo.menu.button.Button;
import org.mineacademy.fo.menu.button.StartPosition;
import org.mineacademy.fo.menu.button.annotation.Position;
import org.mineacademy.fo.menu.model.ItemCreator;
import org.mineacademy.fo.remain.CompMaterial;
import org.mineacademy.lynx.bungee.RemoteGame;

public final class MinigamesMenu extends Menu {

	@Position(start = StartPosition.CENTER, value = -1)
	private final Button minigamesServer;

	@Position(start = StartPosition.CENTER, value = 1)
	private final Button hubServer;

	public MinigamesMenu() {
		this.setTitle("Select Server To Connect To");
		this.setSize(9 * 3);

		final String serverName = "bedwars1";
		final RemoteGame minigames = RemoteGame.findGame(serverName);

		this.minigamesServer = minigames == null ? Button.makeDummy(ItemCreator.of(CompMaterial.RED_BED,
				"Server Unavailable",
				"",
				"Minigames server is down",
				"for maintenance, please",
				"try again later."))

				: new Button() {

					@Override
					public void onClickedInMenu(Player player, Menu menu, ClickType click) {
						if (minigames.canPlay() || minigames.canSpectate()) {
							animateTitle("&4Connecting...");

							BungeeUtil.connect(player, minigames.getServerName());

						} else
							animateTitle("&4Server is full!");
					}

					@Override
					public ItemStack getItem() {
						return ItemCreator.of(CompMaterial.RED_BED,
								"BedWars Server",
								"",
								"&fPlaying: &7" + minigames.getGameName(),
								(minigames.getPlayers() == 0 ? "&7" : "&a") + Common.plural(minigames.getPlayers(), "player"),
								"",
								minigames.canPlay() ? "&6Click to join." : minigames.canSpectate() ? "&3Click to spectate." : "&7This game is full!")
								.make();
					}
				};

		this.hubServer = new Button() {

			@Override
			public void onClickedInMenu(Player player, Menu menu, ClickType click) {
				BungeeUtil.connect(player, "hub");
			}

			@Override
			public ItemStack getItem() {
				return ItemCreator.of(CompMaterial.OAK_DOOR,
						"Return To Lobby",
						"",
						"Click to return",
						"to lobby server.").make();
			}
		};
	}
}
