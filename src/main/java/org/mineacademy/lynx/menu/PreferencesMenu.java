package org.mineacademy.lynx.menu;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.bukkit.ChatColor;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.event.inventory.InventoryAction;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.mineacademy.fo.ItemUtil;
import org.mineacademy.fo.MathUtil;
import org.mineacademy.fo.collection.StrictMap;
import org.mineacademy.fo.menu.Menu;
import org.mineacademy.fo.menu.MenuContainer;
import org.mineacademy.fo.menu.MenuContainerChances;
import org.mineacademy.fo.menu.MenuPagged;
import org.mineacademy.fo.menu.MenuQuantitable;
import org.mineacademy.fo.menu.MenuTools;
import org.mineacademy.fo.menu.button.Button;
import org.mineacademy.fo.menu.button.ButtonConversation;
import org.mineacademy.fo.menu.button.ButtonMenu;
import org.mineacademy.fo.menu.button.StartPosition;
import org.mineacademy.fo.menu.button.annotation.Position;
import org.mineacademy.fo.menu.model.ItemCreator;
import org.mineacademy.fo.menu.model.MenuClickLocation;
import org.mineacademy.fo.menu.model.MenuQuantity;
import org.mineacademy.fo.model.Tuple;
import org.mineacademy.fo.remain.CompChatColor;
import org.mineacademy.fo.remain.CompColor;
import org.mineacademy.fo.remain.CompMaterial;
import org.mineacademy.lynx.conversation.ExpPrompt;
import org.mineacademy.lynx.item.KittyCannon;
import org.mineacademy.lynx.settings.PlayerData;

import lombok.Getter;
import lombok.Setter;

public class PreferencesMenu extends Menu {

	@Position(9 * 1 + 1)
	private final Button timeButton;

	@Position(9 * 1 + 3)
	private final Button expButton;

	@Position(9 * 1 + 5)
	private final Button toolsButton;

	@Position(9 * 1 + 7)
	private final Button chatColorSelectionButton;

	@Position(9 * 3 + 1)
	private final Button mobEggSelectionButton;

	@Position(9 * 3 + 3)
	private final Button potionSelectionButton;

	@Position(9 * 3 + 5)
	private final Button containerEggButton;

	@Position(9 * 3 + 7)
	private final Button containerEggChanceButton;

	public PreferencesMenu(Player player) {

		this.setTitle("&9Preferences");
		this.setSize(9 * 6);
		this.setSlotNumbersVisible();
		this.setViewer(player);

		this.timeButton = new Button() {

			@Override
			public void onClickedInMenu(Player player, Menu menu, ClickType click) {
				final boolean isDay = this.isDay();

				player.getWorld().setFullTime(isDay ? 13000 : 1000);
				restartMenu("&2Set the time to " + (isDay ? "night" : "day"));
			}

			@Override
			public ItemStack getItem() {
				final boolean isDay = this.isDay();

				return ItemCreator.of(
						isDay ? CompMaterial.SUNFLOWER : CompMaterial.WHITE_BED,
						"Change the time",
						"",
						"Current: " + (isDay ? "&eday" : "&3night"),
						"",
						"Click to switch between",
						"the day and the night.")
						.make();
			}

			private boolean isDay() {
				return getViewer().getWorld().getFullTime() < 12500;
			}
		};

		this.expButton = new ButtonConversation(new ExpPrompt(), CompMaterial.EXPERIENCE_BOTTLE,
				"Experience Prompt",
				"",
				"Current: " + this.getViewer().getLevel() + " levels",
				"",
				"Click to open a new prompt",
				"giving you experience levels.");

		this.toolsButton = new ButtonMenu(new CustomToolsMenu(), CompMaterial.IRON_PICKAXE,
				"Tools",
				"",
				"Open another menu",
				"to select custom",
				"tools.");

		this.chatColorSelectionButton = new ButtonMenu(new ChatColorSelectionMenu(player), CompMaterial.RED_DYE,
				"Chat Color",
				"",
				"Open menu to select",
				"custom chat color.");

		this.mobEggSelectionButton = new ButtonMenu(new MobEggSelectionMenu(), CompMaterial.ENDERMAN_SPAWN_EGG,
				"Select Mob Eggs",
				"",
				"Open menu to select",
				"mob eggs.");

		this.potionSelectionButton = new ButtonMenu(new PotionEffectsMenu(), CompMaterial.POTION,
				"Select Potion Effects",
				"",
				"Open menu to select",
				"custom potion effects.");

		this.containerEggButton = new ButtonMenu(new EggContainerMenu(), CompMaterial.CHEST,
				"Mob Egg Container",
				"",
				"Open menu to open",
				"egg container menu.");

		this.containerEggChanceButton = new ButtonMenu(new EggChancesContainerMenu(), CompMaterial.ENDER_CHEST,
				"Mob Egg Chances Container",
				"",
				"Open menu to open container",
				"egg chances menu.");
	}

	/*@Override
	protected int getReturnButtonPosition() {
		return super.getReturnButtonPosition();
	}*/

	@Override
	protected String[] getInfo() {
		return new String[] {
				"This is the main menu page",
				"where you can customize your",
				"player options and see Foundation",
				"menu system opportunities."
		};
	}

	@Override
	public ItemStack getItemAt(int slot) {

		//if (slot == 10)
		//	return this.timeButton.getItem();

		//if (slot == this.getCenterSlot())

		return NO_ITEM;
	}

	@Override
	public Menu newInstance() {
		return new PreferencesMenu(this.getViewer());
	}

	private class CustomToolsMenu extends MenuTools {

		CustomToolsMenu() {
			super(PreferencesMenu.this);

			this.setTitle("Tools Menu");
		}

		@Override
		protected Object[] compileTools() {
			return new Object[] {
					0, 0, 0, 0, KittyCannon.class
			};
		}

		@Override
		protected String[] getInfo() {
			return new String[] {
					"Click a tool to get it!"
			};
		}
	}

	/*private static List<ChatColor> compileColors() {
		List<ChatColor> colors = new ArrayList<>();

		for (ChatColor color : ChatColor.values())
			if (color.isColor())
				colors.add(color);

		return colors;
	}*/

	private class ChatColorSelectionMenu extends MenuPagged<ChatColor> {

		@Position(start = StartPosition.BOTTOM_CENTER)
		private final Button resetButton;

		ChatColorSelectionMenu(Player player) {
			super(9 * 3, PreferencesMenu.this, Arrays.asList(ChatColor.values())
					.stream()
					.filter(ChatColor::isColor)
					.collect(Collectors.toList()));

			this.setViewer(player);
			this.setTitle("Select a Chat Color");

			this.resetButton = Button.makeSimple(CompMaterial.FEATHER, "Reset", "Click to reset\nyour color", player2 -> {
				final PlayerData cache = PlayerData.from(player);

				cache.setColor(null);
				restartMenu("&2Color has been reset!");
			});

			/*PlayerData cache = PlayerData.from(this.getViewer());

			this.resetButton = cache.getColor() != null ? new ButtonRemove(this, "color", ItemUtil.bountifyCapitalized(cache.getColor().name()), () -> {
				cache.setColor(null);

				new ChatColorSelectionMenu(player).displayTo(player);
			}) : Button.makeEmpty();*/
		}

		@Override
		protected ItemStack convertToItemStack(ChatColor item) {
			final PlayerData cache = PlayerData.from(this.getViewer());

			return ItemCreator.ofWool(CompColor.fromChatColor(CompChatColor.of(item.name())))
					.name(item + "Select " + ItemUtil.bountifyCapitalized(item.name()))
					.glow(cache.getColor() == item)
					.make();
		}

		@Override
		protected void onPageClick(Player player, ChatColor clickedColor, ClickType click) {
			final PlayerData cache = PlayerData.from(player);

			cache.setColor(clickedColor);
			restartMenu("&2Picked " + ItemUtil.bountifyCapitalized(clickedColor.name()) + " color!");
		}
	}

	private class MobEggSelectionMenu extends MenuPagged<EntityType> {

		private MobEggSelectionMenu() {
			super(9 * 1, PreferencesMenu.this, Arrays.asList(EntityType.values())
					.stream()
					.filter(type -> type.isSpawnable() && type.isAlive())
					.collect(Collectors.toList()));

			this.setTitle("Select Mob Egg");
		}

		@Override
		protected ItemStack convertToItemStack(EntityType item) {
			return ItemCreator.ofEgg(item, "Spawn " + ItemUtil.bountifyCapitalized(item)).make();
		}

		@Override
		protected void onPageClick(Player player, EntityType item, ClickType click) {
			player.getInventory().addItem(ItemCreator.ofEgg(item).make());

			this.animateTitle("&2Egg added to your inventory!");
		}

		@Override
		protected String[] getInfo() {
			return new String[] {
					"Click on mob eggs in this menu",
					"to automatically receive a spawn",
					"egg to your inventory."
			};
		}
	}

	private class PotionEffectsMenu extends MenuPagged<PotionEffectType> implements MenuQuantitable {

		@Getter
		@Setter
		private MenuQuantity quantity = MenuQuantity.ONE;

		PotionEffectsMenu() {
			super(9 * 3, PreferencesMenu.this, Arrays.asList(PotionEffectType.values()));

			this.setTitle("Select Your Potion Effects");
		}

		@Override
		protected ItemStack convertToItemStack(PotionEffectType item) {
			final int currentLevel = this.getLevelOf(item);

			return this.addLevelToItem(ItemCreator.ofPotion(item, currentLevel)
					.name(ItemUtil.bountifyCapitalized(item))
					.amount(currentLevel > 0 ? currentLevel : 1)
					.make(), currentLevel);
		}

		@Override
		protected void onPageClick(Player player, PotionEffectType item, ClickType click) {
			final int nextLevel = (int) MathUtil.range(this.getLevelOf(item) + this.getNextQuantityPercent(click), 0, 10);

			if (player.hasPotionEffect(item))
				player.removePotionEffect(item);

			if (nextLevel > 0)
				player.addPotionEffect(new PotionEffect(item, Integer.MAX_VALUE, nextLevel - 1), true);
		}

		private int getLevelOf(PotionEffectType type) {
			final PotionEffect effect = this.getPotionEffect(type);
			final int currentLevel = effect == null ? 0 : effect.getAmplifier() + 1;

			return currentLevel;
		}

		private PotionEffect getPotionEffect(PotionEffectType type) {
			for (final PotionEffect effect : this.getViewer().getActivePotionEffects())
				if (effect.getType() == type)
					return effect;

			return null;
		}
	}

	private class EggContainerMenu extends MenuContainer {

		EggContainerMenu() {
			super(PreferencesMenu.this);

			this.setTitle("Place Mob Eggs Here");
		}

		@Override
		protected boolean canEditItem(MenuClickLocation location, int slot, ItemStack clicked, ItemStack cursor, InventoryAction action) {
			final ItemStack placedItem = clicked != null && !CompMaterial.isAir(clicked) ? clicked : cursor;

			if (placedItem != null && !CompMaterial.isAir(placedItem)) {
				if (placedItem.getAmount() > 1 && action != InventoryAction.PLACE_ONE) {
					this.animateTitle("&4Amount must be 1!");

					return false;
				}

				if (!CompMaterial.isMonsterEgg(placedItem.getType())) {
					this.animateTitle("&4Item must be a mob egg!");

					return false;
				}
			}

			return super.canEditItem(location, slot, clicked, cursor, action);
		}

		@Override
		protected ItemStack getDropAt(int slot) {
			final PlayerData cache = PlayerData.from(this.getViewer());
			final List<ItemStack> items = cache.getEggs();

			return slot < items.size() ? items.get(slot) : NO_ITEM;
		}

		@Override
		protected void onMenuClose(StrictMap<Integer, ItemStack> items) {
			final PlayerData cache = PlayerData.from(this.getViewer());

			cache.setEggs(new ArrayList<>(items.values()));
		}
	}

	private class EggChancesContainerMenu extends MenuContainerChances {

		EggChancesContainerMenu() {
			super(PreferencesMenu.this);

			this.setTitle("(Pro) Place Mob Eggs Here");
		}

		@Override
		protected boolean canEditItem(MenuClickLocation location, int slot, ItemStack clicked, ItemStack cursor, InventoryAction action) {
			final ItemStack placedItem = clicked != null && !CompMaterial.isAir(clicked) ? clicked : cursor;

			if (placedItem != null && !CompMaterial.isAir(placedItem)) {
				if (placedItem.getAmount() > 1 && action != InventoryAction.PLACE_ONE) {
					this.animateTitle("&4Amount must be 1!");

					return false;
				}

				if (!CompMaterial.isMonsterEgg(placedItem.getType())) {
					this.animateTitle("&4Item must be a mob egg!");

					return false;
				}
			}

			return true;
		}

		@Override
		protected ItemStack getDropAt(int slot) {
			final Tuple<ItemStack, Double> tuple = this.getTuple(slot);

			return tuple != null ? tuple.getKey() : NO_ITEM;
		}

		@Override
		protected double getDropChance(int slot) {
			final Tuple<ItemStack, Double> tuple = this.getTuple(slot);

			return tuple != null ? tuple.getValue() : 0;
		}

		private Tuple<ItemStack, Double> getTuple(int slot) {
			final PlayerData cache = PlayerData.from(this.getViewer());
			final List<Tuple<ItemStack, Double>> items = cache.getEggChances();

			return slot < items.size() ? items.get(slot) : null;
		}

		@Override
		protected void onMenuClose(StrictMap<Integer, Tuple<ItemStack, Double>> items) {
			final PlayerData cache = PlayerData.from(this.getViewer());

			cache.setEggChances(new ArrayList<>(items.values()));
		}

		@Override
		public boolean allowDecimalQuantities() {
			return true;
		}
	}
}
