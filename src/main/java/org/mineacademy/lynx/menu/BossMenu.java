package org.mineacademy.lynx.menu;

import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.inventory.ItemStack;
import org.mineacademy.fo.menu.Menu;
import org.mineacademy.fo.menu.MenuPagged;
import org.mineacademy.fo.menu.button.Button;
import org.mineacademy.fo.menu.button.ButtonMenu;
import org.mineacademy.fo.menu.button.StartPosition;
import org.mineacademy.fo.menu.button.annotation.Position;
import org.mineacademy.fo.menu.model.ItemCreator;
import org.mineacademy.fo.model.RangedValue;
import org.mineacademy.fo.remain.CompColor;
import org.mineacademy.fo.remain.CompMaterial;
import org.mineacademy.fo.remain.Remain;
import org.mineacademy.lynx.boss.Boss;
import org.mineacademy.lynx.quest.model.Quest;
import org.mineacademy.lynx.rank.Rank;

public class BossMenu extends MenuPagged<Boss> {

	public BossMenu() {
		super(Boss.getBosses());
	}

	@Override
	protected ItemStack convertToItemStack(Boss item) {
		return ItemCreator.ofEgg(item.getType(), "Open " + item.getName() + " Menu").make();
	}

	@Override
	protected void onPageClick(Player player, Boss item, ClickType click) {
		new BossIndividualMenu(item).displayTo(player);
	}

	private class BossIndividualMenu extends Menu {

		private final Boss boss;

		@Position(start = StartPosition.CENTER, value = -2)
		private final Button healthButton;

		@Position(start = StartPosition.CENTER)
		private final Button rankButton;

		@Position(start = StartPosition.CENTER, value = +2)
		private final Button questButton;

		BossIndividualMenu(Boss boss) {
			super(BossMenu.this);

			this.boss = boss;

			this.healthButton = Button.makeDecimalPrompt(ItemCreator.of(
					CompMaterial.RED_DYE,
					"Health",
					"",
					"Current health: " + boss.getHealth(),
					"",
					"Click to edit"),
					"Type new Boss health and press enter.",
					RangedValue.parse("1 - " + Remain.getMaxHealth()),
					boss::setHealth);

			this.rankButton = new ButtonMenu(new BossRankUpgradeSelectionMenu(), ItemCreator.of(
					CompMaterial.IRON_SWORD,
					"Select Rank",
					"",
					"Select rank that the",
					"killer of this Boss",
					"must have to get their",
					"upgrade to their next",
					"rank."));

			this.questButton = new ButtonMenu(new BossQuestSelectionMenu(), ItemCreator.of(
					CompMaterial.WRITTEN_BOOK,
					"Select Quest",
					"",
					"If a player kills this boss,",
					"select a quest that the player",
					"will complete automatically",
					"if he has it as active."));
		}

		@Override
		public Menu newInstance() {
			return new BossIndividualMenu(this.boss);
		}

		private class BossRankUpgradeSelectionMenu extends MenuPagged<Rank> {

			public BossRankUpgradeSelectionMenu() {
				super(BossIndividualMenu.this, Rank.getRanks());
			}

			@Override
			protected ItemStack convertToItemStack(Rank rank) {
				return ItemCreator
						.ofWool(CompColor.fromChatColor(rank.getColor()))
						.name(rank.toString())
						.lore("", "Click to select")
						.glow(rank.getName().equals(boss.getRankToUpgradeFrom()))
						.make();
			}

			@Override
			protected void onPageClick(Player player, Rank item, ClickType click) {
				boss.setRankToUpgradeFrom(item.getName());

				restartMenu("&2Rank set to " + item.getName());
			}
		}

		private class BossQuestSelectionMenu extends MenuPagged<Quest> {

			public BossQuestSelectionMenu() {
				super(BossIndividualMenu.this, Quest.getQuests());
			}

			@Override
			protected ItemStack convertToItemStack(Quest currentItem) {
				final boolean equalsToCurrent = currentItem.getName().equals(boss.getQuestToCompleteWhenKilled());

				return ItemCreator
						.of(currentItem.getIcon())
						.name(currentItem.getName())
						.lore("", equalsToCurrent ? "&cClick to remove." : "&aClick to select.")
						.glow(equalsToCurrent)
						.make();
			}

			@Override
			protected void onPageClick(Player player, Quest currentItem, ClickType click) {
				final boolean equalsToCurrent = currentItem.getName().equals(boss.getQuestToCompleteWhenKilled());

				if (equalsToCurrent)
					boss.setQuestToCompleteWhenKilled(null);
				else
					boss.setQuestToCompleteWhenKilled(currentItem.getName());

				restartMenu(equalsToCurrent ? "&4Quest unset!" : "&2Quest set to " + currentItem.getName());
			}
		}
	}

}
