package org.mineacademy.lynx.menu;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.inventory.ItemStack;
import org.mineacademy.fo.Common;
import org.mineacademy.fo.TimeUtil;
import org.mineacademy.fo.menu.MenuPagged;
import org.mineacademy.fo.menu.model.ItemCreator;
import org.mineacademy.lynx.quest.QuestStartPrompt;
import org.mineacademy.lynx.quest.model.Quest;
import org.mineacademy.lynx.settings.PlayerData;

public final class QuestsMenu extends MenuPagged<Quest> {

	private final PlayerData cache;

	private QuestsMenu(Player player) {
		super(Quest.getQuests());

		this.cache = PlayerData.from(player);

		this.setTitle("Select Your Quests");
	}

	@Override
	protected ItemStack convertToItemStack(Quest quest) {
		boolean isActive = quest.equals(cache.getActiveQuest());
		final List<String> lore = new ArrayList<>();

		// Add description
		lore.add(" ");

		for (final String line : Common.split(quest.getDescription(), 25))
			lore.add(line);

		lore.add(" ");

		// Add action or date of completion
		long completionDate = cache.getCompletedQuests().getOrDefault(quest.getName(), 0L);

		if (completionDate > 0)
			lore.add("&6Completed: " + TimeUtil.getFormattedDateShort(completionDate));

		else
			lore.add(isActive ? "&cClick to cancel." : "&aClick to begin.");

		return ItemCreator.of(quest.getIcon()).name(quest.getName()).lore(lore).glow(isActive).make();
	}

	@Override
	protected void onPageClick(Player player, Quest quest, ClickType click) {
		Quest activeQuest = cache.getActiveQuest();
		boolean isActive = quest.equals(activeQuest);
		long completionDate = cache.getCompletedQuests().getOrDefault(quest.getName(), 0L);

		if (isActive) {
			cache.cancelQuest();

			restartMenu("&4Quest has been cancelled!");

		} else if (completionDate == 0) {
			if (activeQuest != null)
				animateTitle("&4Complete active quest first!");
			else
				QuestStartPrompt.show(player, new QuestStartPrompt(quest));

		} else {
			animateTitle("&4Already completed!");
		}
	}

	public static void showTo(Player player) {
		new QuestsMenu(player).displayTo(player);
	}
}
