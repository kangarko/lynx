package org.mineacademy.lynx.settings;

import java.util.Arrays;
import java.util.List;

import org.mineacademy.fo.settings.SimpleSettings;

/**
 * Example configuration suited for read-only, very useful for
 * storing the main settings file which you do not need to update
 * at runtime and need to access from anywhere.
 */
@SuppressWarnings("unused")
public final class Settings extends SimpleSettings {

	@Override
	protected boolean saveComments() {
		return false;
	}

	@Override
	protected List<String> getUncommentedSections() {
		return Arrays.asList("Custom_Map");
	}

	public static Integer PLAYER_HEALTH;
	public static String PLAYER_NAME;
	public static List<String> PLAYER_ACHIEVEMENTS;
	public static String[] MULTILINE_MESSAGE;

	// I recommend using subsections for organizational purposes.
	public final static class Boss {
		public static Double DAMAGE_MULTIPLER;
		public static String ALIAS;

		private static void init() {
			setPathPrefix("Boss");

			DAMAGE_MULTIPLER = getDouble("Damage_Multiplier");
			ALIAS = getString("Alias");
		}
	}

	/**
	 * (Re)load configuration using the default settings.yml file
	 */
	private static void init() {
		setPathPrefix(null);

		PLAYER_HEALTH = getInteger("Player_Health");
		PLAYER_NAME = getString("Player_Name");
		PLAYER_ACHIEVEMENTS = getStringList("Player_Achievements");
		MULTILINE_MESSAGE = getString("Multiline_Message").split("\n");
	}
}
