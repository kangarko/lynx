package org.mineacademy.lynx.settings;

import java.io.File;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.function.Consumer;

import org.bukkit.ChatColor;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.mineacademy.fo.Common;
import org.mineacademy.fo.FileUtil;
import org.mineacademy.fo.Valid;
import org.mineacademy.fo.collection.SerializedMap;
import org.mineacademy.fo.model.ConfigSerializable;
import org.mineacademy.fo.model.Tuple;
import org.mineacademy.fo.settings.YamlConfig;
import org.mineacademy.fo.visual.VisualizedRegion;
import org.mineacademy.lynx.database.LynxAdvancedDatabase;
import org.mineacademy.lynx.quest.model.Quest;
import org.mineacademy.lynx.rank.Rank;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * A second example of storing/loading settings using a custom file for each player.
 */
@Getter
public final class PlayerData extends YamlConfig {

	@Getter
	private static Map<UUID, PlayerData> playerData = new HashMap<>();

	private String playerName;
	private final UUID uuid;

	private double health;
	private String tabListName;

	private Kit kit;
	private ChatColor color;
	private List<ItemStack> eggs;
	private List<Tuple<ItemStack, Double>> eggChances;
	private long lastVoteRewardTime;
	private Rank rank;
	private int deaths;
	private int playerKills;
	private Quest activeQuest;
	private Object currentQuestData;
	private Map<String, Long> completedQuests;
	private Map<String, Double> healthInWorlds;

	private boolean loadedFromFile;

	// NOT SAVED TO DISK
	private final VisualizedRegion region = new VisualizedRegion();

	@Getter
	@ToString
	@AllArgsConstructor(access = AccessLevel.PRIVATE)
	@NoArgsConstructor(access = AccessLevel.PRIVATE)
	public static class Kit implements ConfigSerializable {

		private String name;
		private List<ItemStack> items;

		@Override
		public SerializedMap serialize() {
			SerializedMap map = new SerializedMap();

			map.put("Name", this.name);
			map.put("Items", this.items);

			return map;
		}

		public static Kit deserialize(SerializedMap map) {
			Kit kit = new Kit();

			kit.name = map.getString("Name");
			kit.items = map.getList("Items", ItemStack.class);

			return kit;
		}
	}

	private PlayerData(String playerName, UUID uuid, boolean loadFromFile) {
		this.playerName = playerName;
		this.uuid = uuid;

		if (loadFromFile) {
			this.setHeader("My\nHeader");
			this.loadConfiguration(NO_DEFAULT, "players/" + uuid + ".yml");
			this.save();

			this.loadedFromFile = loadFromFile;
		}
	}

	@Override
	protected void onLoad() {
		this.loadFromMap(this.getMap(""));
	}

	private void loadFromMap(SerializedMap map) {

		if (this.playerName == null)
			this.playerName = map.getString("Player_Name");

		this.health = map.getDouble("Health", 20D);
		this.tabListName = map.getString("Tablist_Name", this.playerName);
		this.kit = map.get("Kit", Kit.class);
		this.color = map.get("Color", ChatColor.class);
		this.eggs = map.getList("Eggs", ItemStack.class);
		this.eggChances = map.getTupleList("Egg_Chances", ItemStack.class, Double.class);
		this.lastVoteRewardTime = map.getLong("Last_Vote_Reward_Time", 0L);
		this.rank = map.get("Rank", Rank.class);
		this.deaths = map.getInteger("Deaths", 0);
		this.playerKills = map.getInteger("Player_Kills", 0);
		this.activeQuest = map.get("Quest_Active", Quest.class);
		this.currentQuestData = map.getObject("Quest_Current_Data");
		this.completedQuests = map.getMap("Quest_Completed", String.class, Long.class);
		this.healthInWorlds = map.getMap("Health_In_Worlds", String.class, Double.class);
	}

	@Override
	protected void onSave() {
		super.onSave();
	}

	@Override
	public SerializedMap saveToMap() {
		SerializedMap map = new SerializedMap();

		if (this.loadedFromFile)
			map.put("Player_Name", this.playerName);

		map.putIfNonZero("Health", this.health);
		map.putIfExist("Tablist_Name", this.tabListName);
		map.putIfExist("Kit", this.kit);
		map.putIfExist("Color", this.color);
		map.putIfExist("Eggs", this.eggs);
		map.putIfExist("Egg_Chances", this.eggChances);
		map.putIfNonZero("Last_Vote_Reward_Time", this.lastVoteRewardTime);
		map.override("Rank", this.rank == null ? null : this.rank.getName());
		map.putIfNonZero("Deaths", this.deaths);
		map.putIfNonZero("Player_Kills", this.playerKills);
		map.override("Quest_Active", this.activeQuest == null ? null : this.activeQuest.getName());
		map.override("Quest_Current_Data", this.currentQuestData);
		map.override("Quest_Completed", this.completedQuests);
		map.override("Health_In_Worlds", this.healthInWorlds);

		return map;
	}

	// TODO Make sure to implement all data you want saved to your database because here
	// we only save a few as an example
	/*public String saveToJson() {
		JSONObject map = new JSONObject();

		if (this.health != 0)
			map.put("Health", this.health);

		map.put("Tablist_Name", this.tabListName);
		//map.put("Kit", this.kit);
		map.put("Color", this.color);

		JSONArray items = new JSONArray();

		for (ItemStack item : this.eggs)
			items.add(JsonItemStack.toJson(item));

		map.put("Eggs", items);
		//map.put("Egg_Chances", this.eggChances);

		return map.toString();
	}*/

	public void setHealth(double health) {
		this.health = health;

		this.save();
	}

	public void setTabListName(String tabListName) {
		this.tabListName = tabListName;

		this.save();
	}

	public void setKit(String kitName, List<ItemStack> items) {
		this.kit = new Kit(kitName, items);

		this.save();
	}

	public void setColor(ChatColor color) {
		this.color = color;

		this.save();
	}

	public void setEggs(List<ItemStack> eggs) {
		this.eggs = eggs;

		this.save();
	}

	public void setEggChances(List<Tuple<ItemStack, Double>> eggChances) {
		this.eggChances = eggChances;

		this.save();
	}

	public void setLastVoteRewardTime(long lastVoteRewardTime) {
		this.lastVoteRewardTime = lastVoteRewardTime;

		this.save();
	}

	public Rank getRank() {
		return Common.getOrDefault(this.rank, Rank.getFirstRank());
	}

	public void setRank(Rank rank) {
		this.rank = rank;

		this.save();
	}

	public void increaseDeaths() {
		this.deaths++;

		this.save();
	}

	public void increasePlayerKills() {
		this.playerKills++;

		this.save();
	}

	public void setActiveQuest(Quest activeQuest) {
		this.activeQuest = activeQuest;

		this.save();
	}

	public void setCurrentQuestData(Object currentQuestData) {
		this.currentQuestData = currentQuestData;

		this.save();
	}

	public void completeQuest() {
		Valid.checkBoolean(this.activeQuest != null, "Cannot complete a quest for " + this.playerName + " since the player has no active quest!");

		// TODO Turn this into a list of a tuple if you want players able to complete the same quest multiple times
		this.completedQuests.put(this.activeQuest.getName(), System.currentTimeMillis());
		this.cancelQuest();
	}

	public void cancelQuest() {
		this.activeQuest = null;
		this.currentQuestData = null;

		this.save();
	}

	public void setHealth(World world, double health) {
		this.healthInWorlds.put(world.getName(), health);

		this.save();
	}

	public double getHealth(World world) {
		return this.healthInWorlds.getOrDefault(world.getName(), 20.D);
	}

	@Override
	public int hashCode() {
		return this.uuid.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		return obj instanceof PlayerData && ((PlayerData) obj).uuid.equals(this.uuid);
	}

	public static void poll(String playerName, Consumer<PlayerData> syncCallback) {
		pollAll(allData -> {
			for (PlayerData cache : allData)
				if (cache.getPlayerName() != null && cache.getPlayerName().equalsIgnoreCase(playerName)) {
					syncCallback.accept(cache);

					return;
				}

			syncCallback.accept(null);
		});
	}

	public static void pollAll(Consumer<Set<PlayerData>> syncCallback) {
		Common.runAsync(() -> {
			final Set<PlayerData> data = new HashSet<>();

			LynxAdvancedDatabase.getInstance().pollAllCaches(databaseData -> {

				// Merge disk data with database
				data.addAll(databaseData);

				// Add from the disk

				for (File playerFile : FileUtil.getFiles("players", "yml"))
					data.add(PlayerData.fromPlayerFile(playerFile));

				syncCallback.accept(data);
			});
		});
	}

	public static PlayerData from(Player player) {
		UUID uuid = player.getUniqueId();
		PlayerData data = playerData.get(uuid);

		if (data == null) {
			data = new PlayerData(player.getName(), uuid, true);

			playerData.put(uuid, data);
		}

		return data;
	}

	public static PlayerData fromDatabase(UUID uuid, String playerName, SerializedMap dataRaw) {
		PlayerData data = playerData.get(uuid);

		if (data == null)
			data = new PlayerData(playerName, uuid, false);

		// Always load
		data.loadFromMap(dataRaw);

		return data;
	}

	public static PlayerData fromPlayerFile(File file) {
		return fromPlayerFile(UUID.fromString(file.getName().replace(".yml", "")));
	}

	public static PlayerData fromPlayerFile(UUID uuid) {
		PlayerData data = playerData.get(uuid);

		if (data == null)
			data = new PlayerData(null, uuid, true);

		return data;
	}

	public static void remove(Player player) {
		playerData.remove(player.getUniqueId());
	}
}
