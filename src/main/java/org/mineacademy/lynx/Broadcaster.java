package org.mineacademy.lynx;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.entity.Player;
import org.mineacademy.fo.Common;
import org.mineacademy.fo.model.SimpleRunnable;
import org.mineacademy.fo.remain.Remain;

/**
 * A simple task showing messages in the game chat
 */
public final class Broadcaster extends SimpleRunnable {

	/**
	 * The messages to show
	 */
	private final List<String> messages = new ArrayList<>();

	/**
	 * The current position signaling which message to pick, we pick messages from top to bottom
	 */
	private int index = 0;

	/**
	 * Create a new instance and put some example messages in
	 */
	public Broadcaster() {
		this.messages.add("1) Please visit our website and purchase VIP to &6continue playing&7...");
		this.messages.add("2) kangarko is a really good coder bawls");
		this.messages.add("3) Please obey the rules otherwise you shall be &cpunished&7!");
	}

	@Override
	public void run() {

		// If we have reached the last message, we need to reset
		if (this.index == this.messages.size())
			this.index = 0;

		// Send all players the same message at the given index
		for (Player player : Remain.getOnlinePlayers())
			Common.tell(player, "&8[&d!&8] &7" + this.messages.get(index));

		// Increase position
		this.index++;
	}
}
