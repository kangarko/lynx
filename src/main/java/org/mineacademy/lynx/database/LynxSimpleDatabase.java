package org.mineacademy.lynx.database;

import org.bukkit.ChatColor;
import org.mineacademy.fo.collection.SerializedMap;
import org.mineacademy.fo.database.SimpleFlatDatabase;
import org.mineacademy.lynx.settings.PlayerData;

import lombok.Getter;

/**
 * Handles MySQL database for player data, votes and pending votes.
 */
public final class LynxSimpleDatabase extends SimpleFlatDatabase<PlayerData> {

	/**
	 * The instance for singleton access
	 */
	@Getter
	private final static LynxSimpleDatabase instance = new LynxSimpleDatabase();

	private LynxSimpleDatabase() {
		this.addVariable("table", "Lynx");
	}

	/**
	 * Load the player data
	 *
	 * @param map
	 * @param data
	 */
	@Override
	protected void onLoad(final SerializedMap map, final PlayerData data) {
		final ChatColor color = map.get("Color", ChatColor.class);

		if (color != null)
			data.setColor(color);

		// We must make the Bukkit API methods run on the main thread by synchronizing them
		//Bukkit.getWorld("world").getBlockAt(0,0,0).setType(Material.FLOWER_POT);
	}

	/**
	 * Save the player data for the given cache
	 *
	 * @param data
	 */
	@Override
	protected SerializedMap onSave(final PlayerData data) {
		final SerializedMap map = new SerializedMap();

		if (data.getColor() != null)
			map.put("Color", data.getColor());

		return map;
	}

	/**
	 * Return the amount of days after which we remove the player data (NOT vote data, just player data)
	 *
	 * @return
	 */
	@Override
	protected int getExpirationDays() {
		return 90; // 90 is the default value
	}
}