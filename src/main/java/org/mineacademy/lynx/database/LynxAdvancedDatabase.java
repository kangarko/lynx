package org.mineacademy.lynx.database;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.function.Consumer;

import org.bukkit.entity.Player;
import org.mineacademy.fo.Common;
import org.mineacademy.fo.Valid;
import org.mineacademy.fo.collection.SerializedMap;
import org.mineacademy.fo.database.SimpleDatabase;
import org.mineacademy.fo.remain.Remain;
import org.mineacademy.lynx.model.PlayerVote;
import org.mineacademy.lynx.settings.PlayerData;

import lombok.Getter;

public class LynxAdvancedDatabase extends SimpleDatabase {

	/**
	 * The instance for singleton access
	 */
	@Getter
	private final static LynxAdvancedDatabase instance = new LynxAdvancedDatabase();

	private LynxAdvancedDatabase() {
		this.addVariable("table", "Lynx");
		this.addVariable("votes", "Lynx_Votes");
	}

	@Override
	protected void onConnected() {
		this.createTable(TableCreator.of("{table}")
				.addNotNull("UUID", "VARCHAR(64)")
				.add("Name", "TEXT")
				.add("Data", "LONGTEXT")
				//.addDefault("Color", "TEXT", "WHITE")
				.add("Updated", "BIGINT")
				.setPrimaryColumn("UUID"));

		this.createTable(TableCreator.of("{votes}")
				.addAutoIncrement("Id", "INT")
				.add("Player", "TEXT")
				.add("Website", "TEXT")
				.add("Date", "DATETIME")
				.add("Status", "TEXT")
				.setPrimaryColumn("Id"));

		/*this.update("CREATE TABLE IF NOT EXISTS `Lynx` " +
				"(`UUID` VARCHAR(64) NOT NULL, " +
				"`Name` TEXT, `Data` LONGTEXT, " +
				"`Color` TEXT, " +
				"`Updated` BIGINT, PRIMARY KEY (`UUID`));");*/
	}

	public void loadCache(Player player, Consumer<PlayerData> callThisWithDataLoaded) {
		Valid.checkSync("Please call loadCache on the main thread");

		if (!this.isConnected()) {
			Common.runLater(() -> callThisWithDataLoaded.accept(PlayerData.from(player)));

			return;
		}

		Common.runAsync(() -> {

			try {
				ResultSet resultSet = this.query("SELECT * FROM {table} WHERE UUID='" + player.getUniqueId() + "'");

				if (!resultSet.next()) {
					Common.runLater(() -> callThisWithDataLoaded.accept(PlayerData.from(player)));

					return;
				}

				Common.runLater(() -> callThisWithDataLoaded.accept(this.createData(resultSet)));

			} catch (Throwable t) {
				Common.error(t, "Unable to load player data for " + player.getName());
			}
		});
	}

	public void saveCache(Player player, Consumer<PlayerData> callThisWithDataLoaded) {
		Valid.checkSync("Please call loadCache on the main thread");
		PlayerData cache = PlayerData.from(player);

		if (!this.isConnected()) {
			cache.save();

			return;
		}

		Common.runAsync(() -> {

			try {
				this.insert(SerializedMap.ofArray(
						"UUID", player.getUniqueId(),
						"Name", player.getName(),
						//"Color", cache.getColor().name(),
						"Updated", System.currentTimeMillis(),
						"Data", cache.saveToMap().toJson()
				));

				/*// ONLY INTENDED TO BE USED WITH LOCAL STORAGE SUCH AS SQLITE
				SerializedMap map = SerializedMap.ofArray(
						"UUID", player.getUniqueId(),
						"Name", player.getName(),
						//"Color", cache.getColor().name(),
						"Updated", System.currentTimeMillis(),
						"Data", cache.saveToMap().toJson()
				);

				final String columns = Common.join(map.keySet());
				final String values = Common.join(map.values(), ", ", value -> value == null || value.equals("NULL") ? "NULL" : "'" + value + "'");

				this.update("INSERT OR REPLACE INTO {table} (" + columns + ") VALUES (" + values + ");");*/

				Common.runLater(() -> callThisWithDataLoaded.accept(cache));

			} catch (Throwable t) {
				Common.error(t, "Unable to load player data for " + player.getName());
			}
		});
	}

	// /browsestats <playerName>
	public void pollCache(String playerName, Consumer<PlayerData> callThisWithDataLoaded) {
		Valid.checkSync("Please call pollCache on the main thread");

		if (!this.isConnected()) {
			PlayerData.poll(playerName, callThisWithDataLoaded);

			return;
		}

		Common.runAsync(() -> {

			try {
				ResultSet resultSet = this.query("SELECT * FROM {table} WHERE Name='" + playerName + "'");

				if (!resultSet.next()) {
					Common.runLater(() -> callThisWithDataLoaded.accept(null));

					return;
				}

				Common.runLater(() -> callThisWithDataLoaded.accept(this.createData(resultSet)));

			} catch (Throwable t) {
				Common.error(t, "Unable to load player data for " + playerName);
			}
		});
	}

	public void pollAllCaches(Consumer<List<PlayerData>> callThisWithDataLoaded) {
		//Valid.checkSync("Please call pollAllCaches on the main thread");

		if (!this.isConnected()) {
			callThisWithDataLoaded.accept(new ArrayList<>());

			return;
		}

		Common.runAsync(() -> {
			List<PlayerData> data = new ArrayList<>();

			try {
				this.selectAll("{table}", resultSet -> data.add(this.createData(resultSet.getDelegate())));

			} catch (Throwable t) {
				Common.error(t, "Unable to load player datas");
			}

			Common.runLater(() -> callThisWithDataLoaded.accept(data));
		});
	}

	private PlayerData createData(ResultSet resultSet) {
		try {
			UUID uuid = UUID.fromString(resultSet.getString("UUID"));
			String name = resultSet.getString("Name");
			//ChatColor color = ReflectionUtil.lookupEnum(ChatColor.class, resultSet.getString("Color"));
			SerializedMap data = SerializedMap.fromJson(resultSet.getString("Data"));

			//data.override("Color", color);
			return PlayerData.fromDatabase(uuid, name, data);

		} catch (Throwable t) {
			Remain.sneaky(t);

			return null;
		}
	}

	// Vote management

	public List<PlayerVote> pollVotes(Player player, PlayerVote.Status status) {
		this.checkLoadedAndAsync();

		try {
			List<PlayerVote> votes = new ArrayList<>();
			ResultSet resultSet = this.query("SELECT * FROM {votes} WHERE Player='" + player.getName() + "' AND Status='" + status + "'");

			while (resultSet.next())
				votes.add(PlayerVote.fromMySQL(resultSet));

			return votes;

		} catch (Throwable t) {
			Common.throwError(t, "Failed to load votes from database!",
					"Player: " + player.getName(),
					"Error: %error%");

			return null;
		}
	}

	public void addVote(PlayerVote vote) {
		this.checkLoadedAndAsync();

		try {
			this.insert("{votes}", vote);

		} catch (Throwable t) {
			Common.throwError(t, "Failed to load votes from database!",
					"Vote " + vote,
					"Error: %error%");

		}
	}

	public int countPendingVotes(Player player) {
		this.checkLoadedAndAsync();

		return this.count("{votes}",
				"Player", player.getName(),
				"Status", PlayerVote.Status.PENDING);
	}

	public void convertPendingVotesToGiven(Player player) {
		this.update("UPDATE {votes} SET Status = '" + PlayerVote.Status.GIVEN + "' WHERE Player = '" + player.getName() + "'");
	}

	public void removeVotes(Player player) {
		this.update("DELETE FROM {votes} WHERE Player='" + player.getName() + "'");
	}

	private void checkLoadedAndAsync() {
		Valid.checkAsync("Database calls to vote database must happen sync, NOT async!");

		if (!this.isConnected())
			this.connectUsingLastCredentials();

		Valid.checkBoolean(this.isConnected(), "Database not loaded, please call LynxAdvancedDatabase.getInstance()#connect() in your onPluginStart() first!");
	}
}
