package org.mineacademy.lynx.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;

import org.mineacademy.fo.ReflectionUtil;
import org.mineacademy.fo.collection.SerializedMap;
import org.mineacademy.fo.model.ConfigSerializable;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * Represents a simple player vote
 */
@Getter
@Setter
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@ToString
public final class PlayerVote implements ConfigSerializable {

	/**
	 * The player name
	 */
	private final String playerName;

	/**
	 * The website he used to vote from
	 */
	private final String website;

	/**
	 * The timestamp of when we registered the vote
	 */
	private final Timestamp date;

	/**
	 * The vote status
	 */
	private Status status;

	@Override
	public SerializedMap serialize() {
		return SerializedMap.ofArray(
				"Player", this.playerName,
				"Website", this.website,
				"Date", this.date,
				"Status", this.status);
	}

	public static PlayerVote create(String playerName, String website, Status status) {
		return new PlayerVote(playerName, website, new Timestamp(System.currentTimeMillis()), status);
	}

	public static PlayerVote fromMySQL(ResultSet resultSet) throws SQLException {
		String playerName = resultSet.getString("Player");
		String website = resultSet.getString("Website");
		Timestamp date = resultSet.getTimestamp("Date");
		Status status = ReflectionUtil.lookupEnum(Status.class, resultSet.getString("Status"));

		return new PlayerVote(playerName, website, date, status);
	}

	public enum Status {
		PENDING,
		GIVEN
	}
}