package org.mineacademy.lynx.rank;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import org.bukkit.entity.Player;
import org.mineacademy.fo.Messenger;
import org.mineacademy.fo.remain.CompChatColor;
import org.mineacademy.lynx.settings.PlayerData;

import lombok.Getter;

public abstract class Rank {

	private static final Map<String, Rank> byName = new LinkedHashMap<>();

	public static Rank STUDENT = new RankStudent();
	public static Rank CRAFTSMAN = new RankCraftsman();
	public static Rank HERO = new RankHero();
	public static Rank MASTER = new RankMaster();

	@Getter
	private final String name;

	@Getter
	private final CompChatColor color;

	protected Rank(String name, CompChatColor color) {
		this.name = name;
		this.color = color;

		byName.put(name, this);
	}

	public Rank getNext() {
		return null;
	}

	public boolean forceUpgrade(Player player) {
		return this.upgrade(player, true);
	}

	public boolean upgrade(Player player) {
		return this.upgrade(player, false);
	}

	private boolean upgrade(Player player, boolean force) {
		final Rank nextRank = this.getNext();

		if (nextRank == null)
			return false;

		final PlayerData cache = PlayerData.from(player);

		if (this.canUpgrade(player, cache, nextRank) || force) {
			cache.setRank(nextRank);
			this.onUpgrage(player, nextRank);

			Messenger.success(player, "&7You've been promoted to the " + nextRank.getFormattedName() + " &7rank!");
		}

		return false;
	}

	protected boolean canUpgrade(Player player, PlayerData cache, Rank nextRank) {
		return false;
	}

	protected void onUpgrage(Player player, Rank nextRank) {
	}

	public final String getFormattedName() {
		return this.color + this.name;
	}

	@Override
	public String toString() {
		return this.color + this.name;
	}

	public static Rank getFirstRank() {
		return STUDENT;
	}

	public static Rank getByName(String rankName) {
		return byName.get(rankName);
	}

	public static Collection<Rank> getRanks() {
		return byName.values();
	}

	public static Set<String> getRanksNames() {
		return byName.keySet();
	}
}
