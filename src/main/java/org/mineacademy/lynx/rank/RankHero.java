package org.mineacademy.lynx.rank;

import org.mineacademy.fo.remain.CompChatColor;

public class RankHero extends Rank {

	RankHero() {
		super("Hero", CompChatColor.AQUA);
	}

	// Only upgradable when the player killed our own Boss that has this setting
	/*@Override
	protected boolean canUpgrade(Player player, PlayerData cache, Rank nextRank) {
		return false;
	}*/

	@Override
	public Rank getNext() {
		return MASTER;
	}
}
