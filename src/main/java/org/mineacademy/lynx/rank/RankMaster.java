package org.mineacademy.lynx.rank;

import org.mineacademy.fo.remain.CompChatColor;

public class RankMaster extends Rank {

	RankMaster() {
		super("Master", CompChatColor.RED);
	}
}
