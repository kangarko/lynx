package org.mineacademy.lynx.rank;

import org.bukkit.entity.Player;
import org.mineacademy.fo.model.SimpleTime;
import org.mineacademy.fo.remain.CompChatColor;
import org.mineacademy.fo.remain.Remain;
import org.mineacademy.lynx.settings.PlayerData;

public class RankStudent extends Rank {

	private static final SimpleTime THRESHOLD = SimpleTime.from("1 hour");

	RankStudent() {
		super("Student", CompChatColor.GRAY);
	}

	@Override
	protected boolean canUpgrade(Player player, PlayerData cache, Rank nextRank) {
		final long playtimeMinutes = Remain.getPlaytimeMinutes(player);

		return playtimeMinutes > THRESHOLD.getTimeSeconds() / 60;
	}

	@Override
	public Rank getNext() {
		return CRAFTSMAN;
	}
}
