package org.mineacademy.lynx.rank;

import org.bukkit.Statistic;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.mineacademy.fo.PlayerUtil;
import org.mineacademy.fo.remain.CompChatColor;
import org.mineacademy.lynx.settings.PlayerData;

public class RankCraftsman extends Rank {

	private static final long THRESHOLD = 3;

	RankCraftsman() {
		super("Craftsman", CompChatColor.GOLD);
	}

	@Override
	protected boolean canUpgrade(Player player, PlayerData cache, Rank nextRank) {
		final long zombiesKilled = PlayerUtil.getStatistic(player, Statistic.KILL_ENTITY, EntityType.ZOMBIE);

		return zombiesKilled >= THRESHOLD;
	}

	@Override
	public Rank getNext() {
		return HERO;
	}
}
