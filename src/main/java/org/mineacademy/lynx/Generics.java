package org.mineacademy.lynx;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Location;
import org.bukkit.entity.Animals;
import org.bukkit.entity.Cow;
import org.bukkit.entity.Monster;
import org.bukkit.entity.Skeleton;
import org.bukkit.entity.Zombie;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public class Generics implements Listener {

	public static void main(String[] args) {
		// Without generics
		List<int[]> rawNumbers = new ArrayList<>();

		rawNumbers.add(new int[] { 1, 2, 3 });
		//rawNumbers.add("1"); // Wrong!

		//int number = rawNumbers.get(0);
		//System.out.println(rawNumbers.get(0));

		// With generics we would not add "1" but only Integer
	}

	@EventHandler
	public void onPlayerJoin(PlayerJoinEvent event) {
		this.spawnAnimal(event.getPlayer().getLocation(), Zombie.class, Cow.class);

		new Demo<Skeleton>();
	}

	private <T extends Monster, A extends Animals> void spawnAnimal(Location location, Class<T> animalClass, Class<A> mobClass) {
		location.getWorld().spawn(location, animalClass);

		demo(new DemoA());
	}

	/*private <T extends Animals> void spawnAnimal(Location location, Class<T> animalClass) {
		location.getWorld().spawn(location, animalClass);
	}*/

	class DemoA implements Strong, Healthy {

	}

	interface Strong {

	}

	interface Healthy {

	}

	private <T extends Strong & Healthy> void demo(T object) {

	}

	class Demo<T extends Monster> {
		T demoValue;

		public void doThings() {
			demoValue.getEntityId();
		}

		public T getDemoValue() {
			return demoValue;
		}
	}

}
