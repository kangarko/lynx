package org.mineacademy.lynx.antipiracy;

import java.io.PrintStream;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.net.URLConnection;
import java.net.UnknownHostException;
import java.util.Base64;
import java.util.Objects;

import javax.annotation.Nonnull;
import javax.script.ScriptContext;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineFactory;
import javax.script.ScriptEngineManager;

import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;
import org.mineacademy.fo.Common;
import org.mineacademy.fo.RandomUtil;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

/**
 * Cloud-based advanced antipiracy where we download and compile code
 * from an Internet based location to keep loading the plugin, or
 * corrupt the plugin file in case of known cracked plugin nonce.
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
final class AP_AdvancedCheck {

	/**
	 * The JavaScript engine singleton
	 */
	private static final ScriptEngine engine;

	/**
	 * The JavaScript download from the cloud service (can be your website).
	 */
	public static String script;

	/**
	 * The plugin we are connecting into
	 */
	private static JavaPlugin plugin;

	/**
	 * A few integrity checks to make it harder for crackers to tinker with this class
	 */
	public static boolean integrityCheck1 = false;
	public static boolean integrityCheck2 = false;

	// Load the engine
	static {
		Thread.currentThread().setContextClassLoader(JavaPlugin.class.getClassLoader());

		ScriptEngineManager engineManager = new ScriptEngineManager();
		ScriptEngine scriptEngine = engineManager.getEngineByName("Nashorn");

		// Workaround for newer Minecraft releases, still unsure what the cause is
		if (scriptEngine == null) {
			engineManager = new ScriptEngineManager(null);

			scriptEngine = engineManager.getEngineByName("Nashorn");
		}

		// If still fails, try to load our own library for Java 15 and up
		if (scriptEngine == null)
			try {
				final String nashorn = "org.openjdk.nashorn.api.scripting.NashornScriptEngineFactory";
				final ScriptEngineFactory engineFactory = (ScriptEngineFactory) Class.forName(nashorn).newInstance();

				engineManager.registerEngineName("Nashorn", engineFactory);
				scriptEngine = engineManager.getEngineByName("Nashorn");
			} catch (final Throwable t) {
				// Do not load
			}

		engine = scriptEngine;

		if (engine == null) {
			Bukkit.getLogger().severe("JavaScript compiler malfunction - could not find Nashorn!");
			Bukkit.getLogger().severe("If you're using Java 9+ on Minecraft older than 1.16,");
			Bukkit.getLogger().severe("please install NashornPlus from mineacademy.org/nashorn");

		}
	}

	// ------------------------------------------------------------------------------------
	// Direct access
	// ------------------------------------------------------------------------------------

	/**
	 * Called automatically early in plugin loading to download the script
	 * @param plugin
	 */
	public static void prepare(@Nonnull JavaPlugin plugin) {
		AP_AdvancedCheck.plugin = plugin;

		try {
			AP_Firewall.protectFromFirewall();
			setScript(AP_Cipher.decrypt(AP_Constants.URL_ADVANCED));
			AP_Firewall.unprotectFromFirewall();

		} catch (final Throwable t) {
			// homework tip: Post the throwable to your backup server via a POST mechanism
			integrityCheck1 = false;
		}
	}

	/*
	 * Reads and set the cloud location script
	 */
	private static void setScript(String urlAddress) {
		try {
			final URLConnection connection = new URL(urlAddress).openConnection();

			connection.setRequestProperty("User-Agent", AP_Cipher.decrypt(AP_Constants.USER_AGENT));
			connection.setConnectTimeout(6000);
			connection.setReadTimeout(6000);
			connection.setDoOutput(true);

			// Write parameters
			final PrintStream stream = new PrintStream(connection.getOutputStream());
			AP_Constants.collectParams(plugin).forEach((key, value) -> stream.println(key + value.toString()));

			// Read response
			setScript0(connection);

			// All good, Captain!
			ProtectedPlugin.CONNECTED_SUCCESSFULLY = true;

		} catch (UnknownHostException | SocketTimeoutException t) {

			// Case 1: Internet is blocked or unavailable
			logException(t, "<0> Network unreachable: ");

		} catch (final Throwable t) {

			// Case 2: Internal error
			logException(t, "<0> An error occured: ");

			Bukkit.getPluginManager().disablePlugin(plugin);
		}
	}

	/*
	 * Sets the script it got from an external server. Here is the catch: The script
	 * has the first letter indicating a random position that is used to verify the
	 * script's integrity.
	 */
	private static void setScript0(URLConnection connection) throws Exception {
		final String line = String.join("", ProtectedPlugin.readLines(connection));

		if (line != null && !line.isEmpty() && line.length() < 10_000)
			script = line;
		else
			throw new RuntimeException("Illegal response: " + line);
	}

	/**
	 * Attempts to call the script that was downloaded that should keep the plugin loading.
	 */
	public static void callScript() {
		Objects.requireNonNull(engine);

		try {

			if (script == null) {
				Bukkit.getLogger().warning(AP_Cipher.decrypt(AP_Constants.CONTACT_US));
				Bukkit.getPluginManager().disablePlugin(plugin);
				// homework tip: disabling plugin when no internet is reachable is against the rules of many marketplaces
				// so you may need to adjust accordingly

				return;
			}

			engine.getBindings(ScriptContext.ENGINE_SCOPE).clear();
			engine.put("cast", plugin);

			engine.eval(script);

		} catch (final Exception ex) {
			logException(ex, "<0> Runtime error: ");
		}

	}

	/*
	 * Logs the exception as base64 encoded
	 */
	private static void logException(Throwable t, String message) {
		// Homework: Be more creative and obscure the error message in another way
		Common.log(message + /* add random number to make it harder to decompile the error */ RandomUtil.nextString(1)
				+ Base64.getEncoder().encodeToString((t == null ? "" : t.toString()).getBytes()));
	}
}
