package org.mineacademy.lynx.antipiracy;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

import org.mineacademy.fo.plugin.SimplePlugin;

public abstract class ProtectedPlugin extends SimplePlugin {

	/**
	 * Successfully established connection with a PHP script?
	 */
	public static boolean CONNECTED_SUCCESSFULLY = false;

	/**
	 * Called externally to determine whether the internal firewall was loaded properly
	 */
	private boolean integrityCheck = false;

	/*
	 * Download our script
	 */
	@Override
	public final void onPluginLoad() {
		AP_AdvancedCheck.prepare(this);

		this.onProtectedPluginLoad();
	}

	/**
	 * Delegate method for {@link #onPluginLoad()}
	 */
	protected void onProtectedPluginLoad() {
	}

	/*
	 * Load the plugin from the script compiled at runtime
	 */
	@Override
	public final void onPluginStart() {
		if (AP_AdvancedCheck.script != null && AP_AdvancedCheck.integrityCheck1 && AP_AdvancedCheck.integrityCheck2) {
			AP_AdvancedCheck.callScript();

			new AP_SimpleCheck(this);
		}
	}

	/**
	 * A delegate method for {@link #onPluginStart()}
	 */
	protected abstract void onProtectedPluginStart();

	//
	// The following methods should be called from the script you download from the cloud!
	//

	// Fake name: Called externally to determine whether the internal firewall was loaded properly, allows for a() below to load
	public final boolean b() {
		return AP_Firewall.checkIntegrity();
	}

	// Fake name: Called externally, makes our plugin load
	public final boolean a(boolean setThisToTrueToLoadPlugin) {
		return this.integrityCheck = setThisToTrueToLoadPlugin;
	}

	// Fake name: Actually starts the plugin. Invoked from a PHP script.
	public final void a() throws Throwable {

		if (CONNECTED_SUCCESSFULLY)
			try {
				this.warmUpStart();

			} catch (final Throwable error) {
				this.displayError0(error);
			}
	}

	private void warmUpStart() throws Throwable {
		if (this.integrityCheck & this.b())
			this.onProtectedPluginStart();
	}

	// Fake name. Actually corrupts the plugin jar permanently. Invoked from a PHP script.
	public final void h() throws IOException {
		corrupt();
	}

	// Corrupts the plugin file, good-bye!
	private static void corrupt() throws IOException {
		final File file = SimplePlugin.getSource();
		final byte[] buf = new byte[1024];

		final File temp = new File(file.getParentFile(), "_" + file.getName());
		Files.copy(Paths.get(file.toURI()), Paths.get(temp.toURI()), StandardCopyOption.REPLACE_EXISTING);

		final ZipInputStream zin = new ZipInputStream(new FileInputStream(temp));
		final ZipOutputStream out = new ZipOutputStream(new FileOutputStream(file));

		ZipEntry entry = zin.getNextEntry();

		while (entry != null) {
			final String name = entry.getName();
			final boolean a = name.endsWith(".class");

			out.putNextEntry(new ZipEntry(name));

			int len;
			while ((len = zin.read(buf)) > 0)
				out.write(buf, 0, len - (a ? 1 : 0)); // random bytes o_o

			entry = zin.getNextEntry();
		}

		zin.close();
		out.close();
		temp.delete();
	}

	/**
	 * Return all lines from the given URL, opening a connection with a fake user agent first
	 *
	 * @param url
	 * @return
	 * @throws IOException
	 */
	static List<String> readLines(URL url) throws IOException {

		final URLConnection connection = url.openConnection();

		// Set a random user agent to prevent most webhostings from rejecting with 403
		connection.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.7) Gecko/20040803 Firefox/0.9.3");
		connection.setConnectTimeout(6000);
		connection.setReadTimeout(6000);
		connection.setDoOutput(true);

		return readLines(connection);
	}

	/**
	 * Return all lines from the given connection
	 *
	 * @param connection
	 * @return
	 */
	static List<String> readLines(URLConnection connection) {
		final List<String> lines = new ArrayList<>();

		try (BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()))) {
			String inputLine;

			while ((inputLine = reader.readLine()) != null)
				lines.add(inputLine);

		} catch (final IOException ex) {
			throw new RuntimeException(ex);
		}

		return lines;
	}
}
