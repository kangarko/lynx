package org.mineacademy.lynx.antipiracy;

import java.net.URL;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.java.JavaPlugin;
import org.mineacademy.fo.model.SimpleRunnable;


/**
 * A simple antipiracy check reading content from external Internet file source
 * and disabling the plugin and/or printing announcement messages according
 * to instructions in that file.
 */
final class AP_SimpleCheck extends SimpleRunnable {

	private final JavaPlugin plugin;

	public AP_SimpleCheck(JavaPlugin plugin) {
		this.plugin = plugin;

		this.runTask(plugin);
	}

	@Override
	public void run() {

		try {
			final YamlConfiguration config = new YamlConfiguration();

			// Disable firewall to prevent interception (or at least harden it)
			AP_Firewall.protectFromFirewall();

			// Read content of our antipiracy remote database
			final URL url = new URL(AP_Cipher.decrypt(AP_Constants.URL_SIMPLE));
			final String content = String.join("\n", ProtectedPlugin.readLines(url));

			config.loadFromString(content);

			// Enable back to whatever was set previously
			AP_Firewall.unprotectFromFirewall();

			// Disable accordingly
			this.checkKillswitch(config, "global");
			this.checkKillswitch(config, this.plugin.getDescription().getVersion().replace(".", "_"));
			this.checkVariables(config);

		} catch (final Throwable t) {
			//t.printStackTrace();
		}
	}

	/*
	 * Check if the given path contains our disable-message instructions
	 * and announces a message in the console, optionally disabling our plugin
	 */
	private void checkKillswitch(YamlConfiguration config, String path) {
		final String pluginName = this.plugin.getDescription().getName();

		// Look for our keys, case insensitive
		for (final String key : config.getKeys(false)) {
			if (key.equalsIgnoreCase(pluginName)) {
				final ConfigurationSection section = config.getConfigurationSection(key);

				for (final String sectionKey : section.getKeys(false)) {
					if (sectionKey.equals(path)) {
						final ConfigurationSection subsection = config.getConfigurationSection(key + "." + sectionKey);

						for (String subkey : subsection.getKeys(false)) {

							// Flag 1 : Remote kill-switch
							if (subkey.equalsIgnoreCase("disable")) {
								if (subsection.getBoolean(subkey, false))
									Bukkit.getPluginManager().disablePlugin(this.plugin);
							}

							// Flag 2 : Remote message
							else if (subkey.equalsIgnoreCase("message"))
								for (final String line : subsection.getString(subkey).split("\n"))
									Bukkit.getLogger().warning(line);
						}
					}
				}

				break;
			}
		}
	}

	/*
	 * Check if the config contains disabled ID or NONCE matching
	 * our translated variables in the source code.
	 */
	private void checkVariables(YamlConfiguration config) {
		final List<String> ids = config.getStringList("Disabled_Ids");
		final List<String> nonces = config.getStringList("Disabled_Nonces");

		if (ids.contains(AP_Constants.ID) || nonces.contains(AP_Constants.NONCE)) {
			Bukkit.getLogger().warning(config.getString("Disable_Message").replace("{plugin}", this.plugin.getDescription().getName()));

			Bukkit.getPluginManager().disablePlugin(this.plugin);
		}
	}

}