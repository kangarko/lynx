package org.mineacademy.lynx.antipiracy;

import java.lang.reflect.Field;
import java.net.ProxySelector;
import java.net.URL;
import java.net.URLStreamHandlerFactory;

/**
 * An advanced class that attempts to "remove" any firewall layer that may have been
 * added when opening URL connections from the machine.
 * <p>
 * Keep in mind this still doesn't prevent higher level interception plus can easily be
 * disabled.
 */
final class AP_Firewall {

	// Anticrack hardening: If crackers remove protectFromFirewall method, we'll break
	private static String integrityCheck = null;

	private static ProxySelector oldSelector = null;
	private static URLStreamHandlerFactory oldFactory = null;

	/**
	 * Called at random locations in the antipiracy to break it when crackers would remove our code below
	 *
	 * @return
	 */
	public static boolean checkIntegrity() {
		return integrityCheck != null;
	}

	public static void protectFromFirewall() {
		oldSelector = ProxySelector.getDefault();
		integrityCheck = oldSelector != null ? oldSelector.toString() : "null";

		try {
			final Class<?> selector = Class.forName(AP_Cipher.decrypt(AP_Constants.PROXY_SELECTOR));

			if (selector != null && ProxySelector.class.isAssignableFrom(selector))
				ProxySelector.setDefault((ProxySelector) selector.newInstance());

		} catch (final Exception ex) { // hide
		}

		try {
			final Field f = URL.class.getDeclaredField("factory");
			f.setAccessible(true);
			oldFactory = (URLStreamHandlerFactory) f.get(null);

			f.set(null, null);
			URL.setURLStreamHandlerFactory(protocol -> null);

		} catch (final Exception e) {
			if (!(e instanceof NoSuchFieldException))
				throw new RuntimeException(e);
		}

		AP_AdvancedCheck.integrityCheck1 = true;
	}

	public static void unprotectFromFirewall() {
		if (oldSelector != null)
			ProxySelector.setDefault(oldSelector);

		if (oldFactory != null)
			try {
				final Field f = URL.class.getDeclaredField("factory");

				f.setAccessible(true);
				f.set(null, null);

				// Check if it was unset
				try {
					URL.setURLStreamHandlerFactory(oldFactory);

				} catch (final Error err) {
					if (!err.getMessage().equals("factory already defined"))
						throw err;
				}

			} catch (final Exception ex) {
				if (!(ex instanceof NoSuchFieldException))
					throw new RuntimeException(ex);
			}

		AP_AdvancedCheck.integrityCheck2 = true;
	}
}
