package org.mineacademy.lynx.antipiracy;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.lang.reflect.Field;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Scanner;

import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;
import org.mineacademy.fo.remain.Remain;

/**
 * A simple class containing various antipiracy constants
 */
final class AP_Constants {

	/**
	 * These constants are replaced automatically by your marketplace, such as
	 * <p>
	 * https://www.spigotmc.org/wiki/premium-resource-placeholders-identifiers/
	 * https://www.mc-market.org/wiki/anti-piracy-placeholders
	 * https://polymart.org/wiki/placeholders
	 * <p>
	 * KEEP IN MIND THAT the placeholders will not work if your plugin is uploaded inside a .zip file !!
	 */
	public static final String ID = "%%__USER__%%";
	public static final String NONCE = "%%__NONCE__%%";

	// 1 - "The verification domain is unreachable. Please check your firewall."
	// 2 - Contact support@mineacademy.org with proof of your plugin purchase.
	public static final String UNREACHABLE_DOMAIN = "yMJ96eav+jz0oT3k44yENq5r7o3Zrbs5pfRZpV0gabGa/nKb3G2ScbHKXKgec+ZI/ItQ1PFc61Z8a01D3fyFJkLxdcOI48mZh1gGEfZC7yM=:MTAxMTAxMDAwMTExMTEwMQ==";
	public static final String CONTACT_US = "qqktVkI6XyomE19nHUF97vj2mrDeZAYoKNicOEZjT6WPtGHUHRW1gx7wntFqcVzFZJyYrvjCVyUtpK30qrTlYS8izfkfFUPwBLR/CFZc9No=:MTAxMTAxMDAwMTExMTEwMQ==";
	public static final String URL_SIMPLE = "9YxFcDtv8cq94Gu0Bmkw8Z6n6iylGFvFIPoF7lTN9+kGgUC7se5mwKDi92yPnTRUS0WcefM8SD8VRqCFfKlIYw==:MTAxMTAxMDAwMTExMTEwMQ==";
	public static final String URL_ADVANCED = "9YxFcDtv8cq94Gu0Bmkw8Z6n6iylGFvFIPoF7lTN9+lrQBwvt0H24vps1fYqQw/psicQ+dBGKu4zHltZvzcnNQ==:MTAxMTAxMDAwMTExMTEwMQ==";
	public static final String USER_AGENT = "ITeNcqLMkfhSZ0fId0liy+uKoNL3zF/NQbXr82RGSjaqe6mSbolndJbSj6hQkbnS59yjTO2w2qymtHxy1Sq48TxjTe33pBhAcU+4gmpdqsQqgwK5WxDUsJsJdiHg3lFG:MTAxMTAxMDAwMTExMTEwMQ==";
	public static final String PROXY_SELECTOR = "im1i3ZGiMl9eVU7Wq+JpmRb61ziAe7+s5eMzn9XentVD0qTHbr7IdKTEwgkMdoQ/:MTAxMTAxMDAwMTExMTEwMQ==";

	/**
	 * A random key we used for strings in AP_Constants, you can
	 * change it and regenerate all strings in said class.
	 */
	public static final String INIT_VECTOR = "1011010001111101";

	/**
	 * Build parameters to send online
	 * @param plugin
	 *
	 * @return
	 */
	public static Map<String, Object> collectParams(JavaPlugin plugin) {
		final Map<String, Object> map = new LinkedHashMap<>();

		map.put("resource=", plugin.getDescription().getName());
		map.put("&v=", plugin.getDescription().getVersion());
		map.put("&u=", ID);
		map.put("&n=", NONCE);
		map.put("&mcver=", Bukkit.getVersion());
		map.put("&online=", Remain.getOnlinePlayers().size());

		try {
			final Field f = JavaPlugin.class.getDeclaredField("file");
			f.setAccessible(true);
			final File file = (File) f.get(plugin);

			map.put("&s=", file.length());

		} catch (final ReflectiveOperationException ex) {
		}

		map.put("&a=", findSerialNumber());

		return map;
	}

	/*
	 * Return computer serial number. Used to whitelist local builds without %%
	 * strings replaced.
	 */
	static String findSerialNumber() {
		if (isOSNameMatch("Windows"))
			return getSerialWin0();

		if (isOSNameMatch("Mac OS X"))
			return getSerialMac0();

		if (isOSNameMatch("Linux") || isOSNameMatch("LINUX"))
			return getSerialLinux0();

		return "unknown-" + Bukkit.getIp();
	}

	private static boolean isOSNameMatch(String prefix) {
		final String osName = System.getProperty("os.name");

		return osName.startsWith(prefix);
	}

	private static String getSerialWin0() {
		String sn = null;
		OutputStream os = null;
		InputStream is = null;

		final Runtime runtime = Runtime.getRuntime();
		Process process = null;

		try {
			process = runtime.exec(new String[] { "wmic", "baseboard", "get", "serialnumber" });

		} catch (final IOException e) {
			if (e.toString().contains("Cannot run program"))
				return e.getMessage();

			throw new RuntimeException(e);
		}

		os = process.getOutputStream();
		is = process.getInputStream();

		try {
			os.close();
		} catch (final IOException e) {
			throw new RuntimeException(e);
		}

		try (Scanner sc = new Scanner(is)) {
			while (sc.hasNext()) {
				final String next = sc.next();

				if ("SerialNumber".equals(next)) {
					sn = sc.next().trim();
					break;
				}
			}
		} catch (final NoSuchElementException ex) {
			return "NSEE-" + Bukkit.getIp();

		} finally {
			try {
				is.close();
			} catch (final IOException e) {
				throw new RuntimeException(e);
			}
		}

		return sn != null ? sn : "Random banana error";
	}

	private static String getSerialMac0() {
		String sn = null;
		OutputStream os = null;
		InputStream is = null;

		final Runtime runtime = Runtime.getRuntime();
		Process process = null;

		try {
			process = runtime.exec(new String[] { "/usr/sbin/system_profiler", "SPHardwareDataType" });
		} catch (final IOException e) {
			throw new RuntimeException(e);
		}

		os = process.getOutputStream();
		is = process.getInputStream();

		try {
			os.close();
		} catch (final IOException e) {
			throw new RuntimeException(e);
		}

		final BufferedReader br = new BufferedReader(new InputStreamReader(is));
		String line = null;
		final String marker = "Serial Number";

		try {
			while ((line = br.readLine()) != null)
				if (line.contains(marker)) {
					sn = line.split(":")[1].trim();
					break;
				}
		} catch (final IOException e) {
			throw new RuntimeException(e);
		} finally {
			try {
				is.close();
			} catch (final IOException e) {
				throw new RuntimeException(e);
			}
		}

		return sn != null ? sn : "Random apple error";
	}

	private static String getSerialLinux0() {
		byte[] address = null;

		try {
			address = NetworkInterface.getNetworkInterfaces().nextElement().getHardwareAddress();

		} catch (final Throwable t) {
			try {
				address = NetworkInterface.getByInetAddress(InetAddress.getLocalHost()).getHardwareAddress();

			} catch (final Throwable tt) {
				return "NaA (" + tt + " / " + t + ")";
			}
		}

		if (address == null)
			return "0x0-" + Bukkit.getIp();

		String translatedAddress = "";

		for (final byte b : address)
			translatedAddress += b;

		return translatedAddress;
	}
}
