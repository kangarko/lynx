package org.mineacademy.lynx.antipiracy;

import java.io.UnsupportedEncodingException;
import java.security.GeneralSecurityException;
import java.util.Base64;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

/**
 * A simple AES cipher that converts strings into obfuscated characters.
 * <p>
 * Keep in mind that, since the decrypt mechanism must be available at runtime,
 * ANYONE can copy it over and invoke manually to decipher your strings,
 * however this process is fairly time consuming so it's pretty effective to
 * obfuscate them.
 * <p>
 * Obfuscating critical parts of your plugin such as URLs and using
 * reflection to load classes connecting to the Internet also prevents
 * your plugin from being scanned by antimalware/piracy removers and getting positives.
 */
final class AP_Cipher {

	// The cipher name
	private static String CIPHER_NAME = "AES/CBC/PKCS5PADDING";

	/**
	 * Call this to generate a random cipher first! And then make sure
	 * to set the same cipher in your .php file
	 *
	 * @return
	 */
	/*public static String generateCipher() {
		String key = "";
		int CIPHER_KEY_LEN = 16;

		if (key.length() < CIPHER_KEY_LEN) {
			final int numPad = CIPHER_KEY_LEN - key.length();

			for (int i = 0; i < numPad; i++)
				key += new Random().nextBoolean() ? "1" : "0"; // pad to len 16 bytes

		} else if (key.length() > CIPHER_KEY_LEN)
			key = key.substring(0, CIPHER_KEY_LEN); // truncate to 16 bytes

		return key;
	}

	public static void main(String[] args) {
		String key = "1011010001111101";

		System.out.println(encrypt(key, AP_Constants.USER_AGENT));
	}*/

	/**
	 * Encrypt data using AES Cipher (CBC) with 128 bit key
	 *
	 * @param key  - key to use should be 16 bytes long (128 bits)
	 * @param data - data to encrypt
	 * @return encryptedData data in base64 encoding with iv attached at end after a :
	 */
	/*public static String encrypt(String key, String data) {

		try {
			final IvParameterSpec initVector = new IvParameterSpec(key.getBytes("ISO-8859-1"));
			final SecretKeySpec skeySpec = new SecretKeySpec(key.getBytes("ISO-8859-1"), "AES");

			final Cipher cipher = Cipher.getInstance(AP_Cipher.CIPHER_NAME);
			cipher.init(Cipher.ENCRYPT_MODE, skeySpec, initVector);

			final byte[] encryptedData = cipher.doFinal((data.getBytes()));

			final String base64_EncryptedData = Base64.getEncoder().encodeToString(encryptedData);
			final String base64_IV = Base64.getEncoder().encodeToString(key.getBytes("ISO-8859-1"));

			return base64_EncryptedData + ":" + base64_IV;

		} catch (final Exception ex) {
			ex.printStackTrace();
		}

		return null;
	}*/

	/**
	 * Decrypt data using AES Cipher (CBC) with 128 bit key
	 *
	 * @param data - encrypted data with iv at the end separate by :
	 * @return decrypted data string
	 * @throws BadPaddingException
	 * @throws GeneralSecurityException
	 * @throws UnsupportedEncodingException
	 */
	public static String decrypt(String data) throws BadPaddingException, GeneralSecurityException, UnsupportedEncodingException {
		final String[] parts = data.split(":");

		final IvParameterSpec iv = new IvParameterSpec(Base64.getDecoder().decode(parts[1]));
		final SecretKeySpec skeySpec = new SecretKeySpec(AP_Constants.INIT_VECTOR.getBytes("ISO-8859-1"), "AES");

		final Cipher cipher = Cipher.getInstance(AP_Cipher.CIPHER_NAME);
		cipher.init(Cipher.DECRYPT_MODE, skeySpec, iv);

		final byte[] decodedEncryptedData = Base64.getDecoder().decode(parts[0]);
		final byte[] original = cipher.doFinal(decodedEncryptedData);

		return new String(original);
	}

}