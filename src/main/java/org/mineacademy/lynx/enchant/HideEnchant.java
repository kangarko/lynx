package org.mineacademy.lynx.enchant;

import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.event.entity.ProjectileLaunchEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.mineacademy.fo.annotation.AutoRegister;
import org.mineacademy.fo.enchant.SimpleEnchantment;
import org.mineacademy.fo.remain.CompSound;
import org.mineacademy.fo.remain.Remain;

import lombok.Getter;

@AutoRegister(hideIncompatibilityWarnings = true)
public final class HideEnchant extends SimpleEnchantment {

	@Getter
	private static final HideEnchant instance = new HideEnchant();

	private HideEnchant() {
		super("Hide", 5);
	}

	// Called automatically only when the shooter has this particular enchant on his hand item
	@Override
	protected void onShoot(int level, LivingEntity shooter, ProjectileLaunchEvent event) {
		if (shooter instanceof Player) {
			final Player player = (Player) shooter;

			CompSound.BLOCK_ANVIL_BREAK.play(player);
		}
	}

	// Called automatically only when the shooter had this particular enchant on his hand item
	@Override
	protected void onHit(int level, LivingEntity shooter, ProjectileHitEvent event) {
		final Entity hitEntityRaw = Remain.getHitEntity(event);

		if (hitEntityRaw instanceof LivingEntity) {
			final LivingEntity hitEntity = (LivingEntity) hitEntityRaw;

			hitEntity.addPotionEffect(new PotionEffect(PotionEffectType.INVISIBILITY, (level * 3) * 20, 0));
		}
	}
}
