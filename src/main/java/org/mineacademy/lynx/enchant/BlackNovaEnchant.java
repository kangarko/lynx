package org.mineacademy.lynx.enchant;

import org.bukkit.Material;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.mineacademy.fo.Common;
import org.mineacademy.fo.Messenger;
import org.mineacademy.fo.annotation.AutoRegister;
import org.mineacademy.fo.enchant.SimpleEnchantment;
import org.mineacademy.fo.remain.Remain;

import lombok.Getter;

// Enchants are registered in bukkit automatically when you load your plugin
// OBS: Removing your plugin will remove them from your items
@AutoRegister(hideIncompatibilityWarnings = true)
public final class BlackNovaEnchant extends SimpleEnchantment /*implements Listener*/ /*uncomment the implements when using @EventHandlers in this class*/ {

	@Getter
	private static final BlackNovaEnchant instance = new BlackNovaEnchant();

	private BlackNovaEnchant() {
		super("Black Nova", 10);
	}

	// OBS! This will be called for all events so you must check if the involved item stack
	// has this enchant using ItemStack#containsEnchantment
	/*@EventHandler
	public void onInventory(InventoryClickEvent event) {
		ItemStack item = event.getCursor();

		if (item != null && item.containsEnchantment(this)) {
			// run your code here
		}
	}*/

	// Called automatically only when the damager is a living entity having a hand item containing this enchant
	@Override
	protected void onDamage(int level, LivingEntity damager, EntityDamageByEntityEvent event) {
		if (!(event.getEntity() instanceof LivingEntity)) {
			return;
		}

		if (damager instanceof Player) {
			final Player player = (Player) damager;
			//PlayerCache cache = PlayerCache.getCache(player); // --> you can connect it to your class system here

			final int cooldown = Remain.getCooldown(player, Material.IRON_SWORD);

			if (cooldown > 0) {
				Messenger.warn(player, "Wait " + Common.plural(cooldown / 20 + 1, "second") + " to use this tool again.");

				event.setCancelled(true);
				return;
			}

			Remain.setCooldown(player, Material.IRON_SWORD, 10 * 20);
		}

		final LivingEntity victim = (LivingEntity) event.getEntity();

		// Edit: If you want the cool sound and particle you seen in the demo, uncomment the lines below
		//CompSound.BLAZE_HIT.play(damager.getLocation());
		//CompParticle.CRIT.spawn(victim.getLocation());

		victim.setFireTicks(3 * 20);
		victim.setVelocity(damager.getEyeLocation().getDirection().multiply(4).setY(5));
	}

	/**
	 * Example of how you can make the enchant lore gold
	 *
	 * @param level
	 * @return
	 */
	/*@Override
	public String getLore(int level) {
		return "&6" + super.getLore(level);
	}*/
}
