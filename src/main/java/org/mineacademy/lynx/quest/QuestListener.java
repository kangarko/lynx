package org.mineacademy.lynx.quest;

import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.mineacademy.fo.annotation.AutoRegister;
import org.mineacademy.lynx.boss.Boss;
import org.mineacademy.lynx.boss.SpawnedBoss;
import org.mineacademy.lynx.quest.model.Quest;
import org.mineacademy.lynx.settings.PlayerData;

@AutoRegister
public final class QuestListener implements Listener {

	@EventHandler
	public void onEntityDeath(EntityDeathEvent event) {
		if (event.getEntity().getKiller() == null)
			return;

		LivingEntity victim = event.getEntity();
		Player killer = victim.getKiller();
		Quest quest = PlayerData.from(killer).getActiveQuest();

		if (quest != null)
			quest.onKill(killer, victim, event);
	}

	@EventHandler(priority = EventPriority.HIGHEST)
	public void onEntityDamage(EntityDamageByEntityEvent event) {
		if (!(event.getDamager() instanceof LivingEntity) || !(event.getEntity() instanceof LivingEntity))
			return;

		LivingEntity damager = (LivingEntity) event.getDamager();
		LivingEntity victim = (LivingEntity) event.getEntity();

		if (damager instanceof Player) {
			Player damagerPlayer = (Player) damager;
			Quest quest = PlayerData.from(damagerPlayer).getActiveQuest();

			if (quest != null)
				quest.onAttack(damagerPlayer, victim, event);
		}

		if (victim instanceof Player) {
			Player victimPlayer = (Player) victim;
			Quest quest = PlayerData.from(victimPlayer).getActiveQuest();

			if (quest != null)
				quest.onDamaged(damager, victimPlayer, event);
		}
	}

	@EventHandler(priority = EventPriority.HIGHEST)
	public void onRightClick(PlayerInteractEntityEvent event) {
		if (!(event.getRightClicked() instanceof LivingEntity))
			return;

		LivingEntity rightClicked = (LivingEntity) event.getRightClicked();
		SpawnedBoss boss = Boss.findBoss(rightClicked);

		if (boss != null) {
			Player player = event.getPlayer();
			PlayerData cache = PlayerData.from(player);
			Quest activeQuest = cache.getActiveQuest();

			if (activeQuest != null) {
				activeQuest.onRightClickBoss(player, cache, boss.getBoss(), event);

				event.setCancelled(true);
			}
		}
	}
}
