package org.mineacademy.lynx.quest;

import java.util.Arrays;

import org.bukkit.conversations.ConversationContext;
import org.bukkit.conversations.Prompt;
import org.bukkit.entity.Player;
import org.mineacademy.fo.Common;
import org.mineacademy.fo.conversation.SimplePrompt;
import org.mineacademy.lynx.quest.model.Quest;
import org.mineacademy.lynx.settings.PlayerData;

public final class QuestStartPrompt extends SimplePrompt {

	private final Quest quest;

	public QuestStartPrompt(final Quest quest) {
		super(false);

		this.quest = quest;
	}

	@Override
	protected String getPrompt(final ConversationContext ctx) {
		return String.join("\n", Arrays.asList(
				"&8" + Common.chatLineSmooth(),
				"&cWelcome to '" + this.quest.getName() + "' quest",
				"&8 ",
				"&cObjective&7: " + this.quest.getDescription(),
				"&7Type &caccept &7to take on this quest, or &cdeny &7to reject it.",
				"&8" + Common.chatLineSmooth()));
	}

	@Override
	protected boolean isInputValid(final ConversationContext context, final String input) {
		return "accept".equals(input) || "deny".equals(input);
	}

	@Override
	protected Prompt acceptValidatedInput(final ConversationContext context, final String input) {
		final Player player = this.getPlayer(context);

		if ("accept".equals(input)) {
			PlayerData.from(player).setActiveQuest(this.quest);

			this.tell(context, "&6You have started this quest.");

		} else
			this.tell(context, "&cYou have decided not to start this quest.");

		return Prompt.END_OF_CONVERSATION;
	}

}