package org.mineacademy.lynx.quest.model;

import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.mineacademy.fo.Common;
import org.mineacademy.fo.PlayerUtil;
import org.mineacademy.fo.remain.CompMaterial;
import org.mineacademy.lynx.boss.Boss;
import org.mineacademy.lynx.settings.PlayerData;

public final class QuestArmoredDelivery extends Quest {

	private static final CompMaterial DELIVERY_ITEM = CompMaterial.DIAMOND_HORSE_ARMOR;

	protected QuestArmoredDelivery() {
		super("Armored Delivery");
	}

	@Override
	public CompMaterial getIcon() {
		return DELIVERY_ITEM;
	}

	@Override
	public String getDescription() {
		return "Deliver a horse armor to Sir Richard at the gate to Stavanger!";
	}

	@Override
	public String getActiveQuestActionBar(Player player) {
		final boolean hasHorseArmor = player.getInventory().contains(DELIVERY_ITEM.getMaterial());

		return hasHorseArmor ? "Sir Richard is waiting for you!" : "Collect Diamond Horse Armor";
	}

	@Override
	public void onRightClickBoss(Player player, PlayerData cache, Boss boss, PlayerInteractEntityEvent event) {

		// TODO customize in your own menu
		if (boss.getName().equals("SirRichard"))
			if (player.getInventory().contains(DELIVERY_ITEM.getMaterial())) {
				cache.completeQuest();

				PlayerUtil.takeFirstOnePiece(player, DELIVERY_ITEM);
				Common.tellNoPrefix(player, "&8[&6NPC&8] &2Thank you very much, you will now be rewarded!");

				Common.runLater(20 * 4, () -> {
					Common.tellNoPrefix(player, "&8[&6NPC&8] &cMuhahaha, that's what you get!");

					player.getWorld().strikeLightningEffect(player.getLocation());
				});

			} else
				Common.tellNoPrefix(player, "&8[&6NPC&8] &cI am waiting for your diamond horse armor!");
	}
}