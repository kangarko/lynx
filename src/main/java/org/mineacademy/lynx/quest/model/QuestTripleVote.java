package org.mineacademy.lynx.quest.model;

import org.bukkit.entity.Player;
import org.mineacademy.fo.Messenger;
import org.mineacademy.fo.remain.CompMaterial;
import org.mineacademy.lynx.settings.PlayerData;

public class QuestTripleVote extends Quest {

	protected QuestTripleVote() {
		super("Triple Vote");
	}

	@Override
	public CompMaterial getIcon() {
		return CompMaterial.TOTEM_OF_UNDYING;
	}

	@Override
	public String getActiveQuestActionBar(Player player) {
		PlayerData cache = PlayerData.from(player);
		int votes = (cache.getCurrentQuestData() != null ? (int) cache.getCurrentQuestData() : 0);

		return "Voted " + votes + "/3X";
	}

	@Override
	public String getDescription() {
		return "Vote for our server at least three times.";
	}

	@Override
	public void onVote(Player player) {
		PlayerData cache = PlayerData.from(player);
		int votes = (cache.getCurrentQuestData() != null ? (int) cache.getCurrentQuestData() : 0);

		cache.setCurrentQuestData(votes + 1);

		if (votes >= 3) {
			cache.completeQuest();

			Messenger.announce(player, "Thank you for voting three times! You'll receive absolutely nothing!");
		}
	}
}
