package org.mineacademy.lynx.quest.model;

import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDeathEvent;
import org.mineacademy.fo.Common;
import org.mineacademy.fo.remain.CompMaterial;
import org.mineacademy.fo.remain.CompSound;
import org.mineacademy.lynx.settings.PlayerData;

public class QuestZombieSlayer extends Quest {

	private static final int THRESHOLD = 3;

	protected QuestZombieSlayer() {
		super("Zombie Slayer");
	}

	@Override
	public CompMaterial getIcon() {
		return CompMaterial.IRON_SWORD;
	}

	@Override
	public String getDescription() {
		return "Kill 3 zombies to complete this quest.";
	}

	@Override
	public String getActiveQuestActionBar(Player player) {
		final PlayerData cache = PlayerData.from(player);
		final int zombiesKilled = (cache.getCurrentQuestData() != null ? (int) cache.getCurrentQuestData() : 0);

		return "Killed " + zombiesKilled + "/" + THRESHOLD + " Zombies";
	}

	@Override
	public void onKill(Player killer, LivingEntity victim, EntityDeathEvent event) {
		final PlayerData cache = PlayerData.from(killer);

		if (victim.getType() == EntityType.ZOMBIE) {
			final int zombiesKilled = (cache.getCurrentQuestData() != null ? (int) cache.getCurrentQuestData() : 0) + 1;

			if (zombiesKilled >= THRESHOLD) {
				cache.completeQuest();

				Common.tell(killer, "&2Congratulations for completing this quest!");
				CompSound.ENTITY_FIREWORK_ROCKET_LAUNCH.play(killer);

			} else {
				cache.setCurrentQuestData(zombiesKilled);

				Common.tell(killer, "&6Only " + (THRESHOLD - zombiesKilled) + " zombie kills left to complete your quest!");
			}
		}
	}
}
