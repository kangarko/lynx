package org.mineacademy.lynx.quest.model;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.mineacademy.fo.Common;
import org.mineacademy.fo.remain.CompMaterial;
import org.mineacademy.fo.remain.CompSound;
import org.mineacademy.lynx.settings.PlayerData;

public class QuestComeHome extends Quest {

	private static Location HOME_LOCATION;

	protected QuestComeHome() {
		super("Come Home");

		HOME_LOCATION = new Location(Bukkit.getWorld("world"), 164, 81, 58);
	}

	@Override
	public CompMaterial getIcon() {
		return CompMaterial.BIRCH_SAPLING;
	}

	@Override
	public String getDescription() {
		return "Reach your home location.";
	}

	@Override
	public String getActiveQuestActionBar(Player player) {
		return "Distance from home: " + Math.round(player.getLocation().distance(HOME_LOCATION)) + " blocks";
	}

	@Override
	public void onTick(Player player, PlayerData cache) {
		if (player.getLocation().distance(HOME_LOCATION) < 5) {
			cache.completeQuest();

			Common.tell(player, "&2Congratulations for completing this quest!");
			CompSound.ENTITY_FIREWORK_ROCKET_LARGE_BLAST_FAR.play(player);
		}
	}
}
