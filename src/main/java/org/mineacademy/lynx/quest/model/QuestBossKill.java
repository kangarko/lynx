package org.mineacademy.lynx.quest.model;

import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDeathEvent;
import org.mineacademy.fo.Common;
import org.mineacademy.fo.remain.CompMaterial;
import org.mineacademy.fo.remain.CompSound;
import org.mineacademy.lynx.boss.Boss;
import org.mineacademy.lynx.boss.SpawnedBoss;
import org.mineacademy.lynx.settings.PlayerData;

public class QuestBossKill extends Quest {

	protected QuestBossKill() {
		super("Boss Killer");
	}

	@Override
	public CompMaterial getIcon() {
		return CompMaterial.SKELETON_SKULL;
	}

	@Override
	public String getDescription() {
		return "Kill Warrior boss to complete this quest.";
	}

	@Override
	public String getActiveQuestActionBar(Player player) {
		return "Go kill a Warrior Boss";
	}

	@Override
	public void onKill(Player killer, LivingEntity victim, EntityDeathEvent event) {
		final PlayerData cache = PlayerData.from(killer);
		final SpawnedBoss boss = Boss.findBoss(victim);

		if (boss != null && this.getName().equals(boss.getBoss().getQuestToCompleteWhenKilled())) {
			cache.completeQuest();

			Common.tell(killer, "&2Congratulations for completing this quest!");
			CompSound.ENTITY_FIREWORK_ROCKET_LARGE_BLAST_FAR.play(killer);
		}
	}
}
