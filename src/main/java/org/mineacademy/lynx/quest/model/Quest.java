package org.mineacademy.lynx.quest.model;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.mineacademy.fo.remain.CompMaterial;
import org.mineacademy.lynx.boss.Boss;
import org.mineacademy.lynx.settings.PlayerData;

import lombok.Getter;

/**
 * The core model class for quest and mission system
 */
public abstract class Quest {

	/**
	 * Stores active quests by their name, quests are singletons
	 */
	private static final Map<String, Quest> byName = new HashMap<>();

	// --------------------------------------------------------------------------------------------------------------
	// Place your quests below, make sure to give them protected constructor to prevent accidental initializing
	// --------------------------------------------------------------------------------------------------------------

	public static final Quest ZOMBIE_SLAYER = new QuestZombieSlayer();
	public static final Quest BOSS_KILLER = new QuestBossKill();
	public static final Quest COME_HOME = new QuestComeHome();
	public static final Quest ARMORED_DELIVERY = new QuestArmoredDelivery();
	public static final Quest TRIPLE_VOTE = new QuestTripleVote();

	// --------------------------------------------------------------------------------------------------------------

	/**
	 * The name of this quest
	 */
	@Getter
	private final String name;

	/**
	 * Create a new quest of that given name
	 *
	 * The name is used when saving the file so please do not use special characters or colors
	 *
	 * @param name
	 */
	protected Quest(String name) {
		this.name = name;

		byName.put(name, this);
	}

	/**
	 * Get the quest icon show in menus
	 *
	 * @return
	 */
	public abstract CompMaterial getIcon();

	/**
	 * Get quest lore (description) shown in menu and quest start prompt
	 *
	 * @return
	 */
	public abstract String getDescription();

	/**
	 * Get the completion message in case there is a player
	 *
	 * @param player
	 *
	 * @return
	 */
	public abstract String getActiveQuestActionBar(Player player);

	// --------------------------------------------------------------------------------------------------------------
	// Events called automatically if the related player is going through this quest
	// --------------------------------------------------------------------------------------------------------------

	/**
	 * Called if the given player has this quest active and kills someone
	 *
	 * @param killer
	 * @param victim
	 * @param event
	 */
	public void onKill(Player killer, LivingEntity victim, EntityDeathEvent event) {
	}

	/**
	 * Called if the given player has this quest active and attacks someone
	 *
	 * @param attacker
	 * @param victim
	 * @param event
	 */
	public void onAttack(Player attacker, LivingEntity victim, EntityDamageByEntityEvent event) {
	}

	/**
	 * Called if something attacks a player that is going through this quest
	 *
	 * @param attacker
	 * @param victim
	 * @param event
	 */
	public void onDamaged(LivingEntity attacker, Player victim, EntityDamageByEntityEvent event) {
	}

	/**
	 * Called automatically when the player doing this quest right clicks a Boss
	 *
	 * @param player
	 * @param cache
	 * @param boss
	 * @param event
	 */
	public void onRightClickBoss(Player player, PlayerData cache, Boss boss, PlayerInteractEntityEvent event) {
	}

	/**
	 * Called when the given player votes from a given site
	 *
	 * @param player
	 */
	public void onVote(Player player) {
	}

	/**
	 * Called automatically from {@link org.mineacademy.lynx.task.QuestTask}
	 *
	 * @param cache
	 * @param player
	 */
	public void onTick(Player player, PlayerData cache) {
	}

	@Override
	public int hashCode() {
		return this.name.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		return obj instanceof Quest && ((Quest) obj).getName().equals(this.name);
	}

	// --------------------------------------------------------------------------------------------------------------
	// Static access
	// --------------------------------------------------------------------------------------------------------------

	/**
	 * Return the quest class from the given name, or null
	 *
	 * @param name
	 * @return
	 */
	public static final Quest getByName(String name) {
		return byName.get(name);
	}

	/**
	 * Return all quests
	 *
	 * @return
	 */
	public static final Collection<Quest> getQuests() {
		return Collections.unmodifiableCollection(byName.values());
	}

	/**
	 * Return all quests as their names
	 *
	 * @return
	 */
	public static final Set<String> getQuestsNames() {
		return Collections.unmodifiableSet(byName.keySet());
	}
}
