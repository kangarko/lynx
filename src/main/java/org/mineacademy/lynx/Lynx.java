package org.mineacademy.lynx;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.entity.ProjectileLaunchEvent;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.mineacademy.fo.Common;
import org.mineacademy.fo.Messenger;
import org.mineacademy.fo.MinecraftVersion;
import org.mineacademy.fo.model.SimpleTime;
import org.mineacademy.fo.plugin.SimplePlugin;
import org.mineacademy.fo.remain.CompMetadata;
import org.mineacademy.fo.remain.Remain;
import org.mineacademy.lynx.api.CowExplodeEvent;
import org.mineacademy.lynx.boss.Boss;
import org.mineacademy.lynx.claim.Claim;
import org.mineacademy.lynx.command.MenuCommand;
import org.mineacademy.lynx.hook.EffectLibHook;
import org.mineacademy.lynx.hook.VotifierHook;
import org.mineacademy.lynx.settings.MenuData;
import org.mineacademy.lynx.settings.PlayerData;
import org.mineacademy.lynx.task.AntiLagTask;
import org.mineacademy.lynx.task.BossSpawningTask;
import org.mineacademy.lynx.task.ClaimTask;
import org.mineacademy.lynx.task.QuestTask;
import org.mineacademy.lynx.task.RankupTask;

/**
 * Welcome to the example Minecraft plugin we create together in the Project Orion
 * training program from minecademy.org. This plugin uses Spigot API is enhanced
 * by our custom Foundation library to help you code faster, using less.
 *
 * @author kangarko
 */
public final class Lynx extends SimplePlugin /* I have disabled antipiracy from now on, if you need to learn about it then see the respective video */ {

	private EffectLibHook effectsLib;

	/**
	 * This method is called automatically each time your plugin starts or when you type /reload
	 */
	@Override
	protected void onPluginStart() {

		//Menu.setSound();
		//Button.setInfoButtonMaterial();

		// DO NOT USE THIS FOR THE KIT SPECIFICALLY BECAUSE IT ALREADY IMPLEMENTS CONFIG SERIALIZABLE
		// SerializeUtil.addSerializer(PlayerData.Kit.class, PlayerData.Kit::getName);

		// Example of logging console messages:

		// A) System.out is great for a quick debug, but not recommended for production.
		//System.out.println("Lynx is now ready to operate!");

		// B) (Recommended) Using the Common class for log. You can set a log prefix shown before
		// all messages here. You can do the same with a tell prefix, shown before messages you send to
		// a player using Common.tell()
		Common.setLogPrefix("&8[&fLynxLog&8]&f");
		Common.setTellPrefix("&5Lynx //&7");

		// Examples of different logging methods
		/*Common.log("Lol this is a log message");
		Common.logNoPrefix("No prefix, kinda like syso");
		Common.warning("Diz is a warning");

		Common.logFramed(
				"Hello this is",
				"a message in a frame!");*/

		// Example of registering events: If you want to listen to actions each player does, you need to
		// put them in a class that "implements Listener" and then register the class here or
		// use the @AutoRegister annotation above the class to do it automatically using Foundation
		//registerEvents(PlayerListener.getInstance());

		/*try {
			Document document = Jsoup.parse(new URL("https://example.com"), 1000);
			Element element = document.getElementsByTag("h1").get(0);
		
			System.out.println("Found element: '" + element.html() + "'");
		
		} catch (IOException ex) {
			throw new RuntimeException(ex);
		}*/

		//LynxAdvancedDatabase.getInstance().connect("jdbc:sqlite:" + FileUtil.getOrMakeFile("database.sqlite").getAbsolutePath());
		//LynxAdvancedDatabase.getInstance().connect("mariadb105.websupport.sk", 3315, "mcserver", "mcserver", "Ub9h+dQfLF");
		//LynxSimpleDatabase.getInstance().connect("mariadb105.websupport.sk", 3315, "mcserver", "mcserver", "YOURPASS");

		Common.log("Lynx has been successfully loaded!");
	}

	@Override
	protected void onPluginStop() {

		// Close custom inventories because bukkit removes temporary metadata from players on /reload
		// to prevent menus from being broken when you reload the server while players have their menus opened
		for (Player online : Remain.getOnlinePlayers()) {

			// Call onClose() in your menus manually, apparently bukkit does not call InventoryCloseEvent here (you may
			// need to check this with your MC version you are using)
			if (CompMetadata.hasMetadata(online, "CustomInventory_" + Lynx.getNamed())) {
				MenuCommand.Menu menu = (MenuCommand.Menu) online.getMetadata("CustomInventory_" + Lynx.getNamed()).get(0).value();

				menu.onClose(online);
			}

			online.closeInventory();
		}
	}

	@Override
	protected void onPluginPreReload() {

		// Close your database here
		//LynxSimpleDatabase.getInstance().close();
	}

	/**
	 * This is invoked when you start the plugin (after onPluginStart), on /reload,
	 * or the {@link #reload()} method.
	 */
	@Override
	protected void onReloadablesStart() {
		//registerCommand(new SpawnEntityCommand());
		//registerCommands(new ChannelCommandGroup());

		//Common.runTimer(20, new Broadcaster());
		Common.runTimer(20, new BossSpawningTask());
		Common.runTimer(20, new RankupTask());
		Common.runTimer(20, new QuestTask());
		Common.runTimer(SimpleTime.from("5 seconds").getTimeTicks(), new AntiLagTask());
		Common.runTimerAsync(20, new ClaimTask());
		//Common.runTimerAsync(20, new ElytraTask());

		/*for (Iterator<UUID> it = PlayerData.getPlayerData().keySet().iterator(); it.hasNext();){
			UUID uniqueId = it.next();
		
			// Database.save()
		
			if (Bukkit.getPlayer(uniqueId) == null)
				it.remove();
		}*/

		MenuData.loadMenus();
		Boss.loadBosses();
		Claim.loadClaims();

		// I just forgot to mention that the version of the library we are using requires MC 1.16+
		// For legacy versions either use older version or our own CompParticle class
		if (MinecraftVersion.atLeast(MinecraftVersion.V.v1_16)) {
			if (this.effectsLib != null)
				this.effectsLib.shutdown();

			this.effectsLib = new EffectLibHook();
			registerEvents(this.effectsLib);
		}

		if (Common.doesPluginExist("Votifier"))
			this.registerEvents(new VotifierHook());
	}

	@Override
	public int getMetricsPluginId() {
		return 15035;
	}

	// ------------------------------------------------------------------------------------------------------------
	// Events examples
	// ------------------------------------------------------------------------------------------------------------

	/**
	 * An example of listening to a chat event. You can listen to the same events multiple times,
	 * you can also specify different event priority to prevent conflicts with plugins listening
	 * to this event too.
	 *
	 * Priorities mean Bukkit will run your code first if the priority is LOWEST, then it will run
	 * all plugins and their codes for the event if the priority is LOW, then NORMAL, then HIGH
	 * then HIGHEST and finally, it will use the MONITOR priority as last.
	 *
	 * @param event
	 */
	/*@EventHandler(priority = EventPriority.LOWEST)
	public void onChatEarly(AsyncPlayerChatEvent event) {
		System.out.println("1 Message: " + event.getMessage());

		event.setCancelled(true);
	}*/

	/**
	 * An example of a simple channel system where player see each other messages only
	 * if they are in the same channel.
	 *
	 * @param event
	 */
	@EventHandler
	public void onChat(AsyncPlayerChatEvent event) {
		final Player player = event.getPlayer();
		final PlayerData cache = PlayerData.from(player);
		final String senderChannel = ChannelManager.getChannel(player);

		if (senderChannel == null) {
			Messenger.error(player, "You have no channel to type into.");

			event.setCancelled(true);
			return;
		}

		event.getRecipients().removeIf(recipient -> !ChannelManager.isJoined(recipient, senderChannel));

		event.setFormat(Common.colorize("&8[&7" + senderChannel + "&8] [" + cache.getRank().getFormattedName() + "&8]")
				+ " %1$s: " + (cache.getColor() != null ? cache.getColor() : "") + "%2$s");
	}

	@EventHandler(priority = EventPriority.MONITOR)
	public void onChatLate(AsyncPlayerChatEvent event) {
		//System.out.println("3 Message: " + event.getMessage() + ", is cancelled? " + event.isCancelled());
	}

	/**
	 * Monitoring events is recommended for logging plugins such as if you want to "spy" which blocks were
	 * broken. Use other priorities for cancelling the event or manipulation.
	 *
	 * @param event
	 */
	@EventHandler
	public void onBlockBreak(BlockBreakEvent event) {
		/*final Player player = event.getPlayer();

		// Examples of different ways you can send messages to the player who broke the block
		Common.tell(player, "&cHello there");
		Messenger.info(player, "Hey dudas");
		Messenger.question(player, "whatsapp?");
		Common.tellTimed(5, player, "&cYou are not entitled to break this block!");

		// Prevents the block from being broken. This will send an additional packet (a message) to the client
		// that the block is still there, on slow internet connections you will see it flicker for a second.
		event.setCancelled(true);*/
	}

	@EventHandler
	public void onArrowLaunch(ProjectileLaunchEvent event) {
		/*if (event.getEntity() instanceof Arrow) {
			Arrow arrow = (Arrow) event.getEntity();

			//if (arrow.getShooter() instanceof Player && !((Player)arrow.getShooter()).hasPermission("lynx.feature.vip"))
			//	return;

			new SimpleRunnable() {
				@Override
				public void run() {
					if (arrow.isDead()) {
						cancel();

						return;
					}

					CompParticle.VILLAGER_HAPPY.spawn(arrow.getLocation());
				}
			}.runTaskTimer(this, 0, 1);
		}*/
	}

	@EventHandler
	public void onEntityClick(PlayerInteractEntityEvent event) {

		// Since Minecraft 1.9, Bukkit calls your code TWICE, once for your right hand (main hand),
		// second for left hand (off hand). We are only interested in running the code for main hand
		// to prevent double explosions since Bukkit calls the event rapidly after each other.
		if (!Remain.isInteractEventPrimaryHand(event))
			return;

		// Uncomment to make function, if the clicked entity is cow, we call our custom event and
		// if our event was not canceled by other developers then we make an explosion at cow's location.

		/*final Entity entity = event.getRightClicked();

		if (entity instanceof Cow) {
			final CowExplodeEvent cowEvent = new CowExplodeEvent(entity);

			// Only run code when event was not canceled by other plugin, and use cowEvent.getPower() which
			// can be set by anyone > this will return the last modification on the highest priority from the last
			// plugin which modified it, in this case it is only us below.
			if (Common.callEvent(cowEvent))
				entity.getWorld().createExplosion(entity.getLocation(), cowEvent.getPower());
		}*/
	}

	/**
	 * Example of listening to a custom event. I will show you how to import other plugins to your source code
	 * later. This time we are listening to our own event we created and called above.
	 *
	 * @param event
	 */
	@EventHandler
	public void onCowExplode(CowExplodeEvent event) {
		System.out.println("Exploding cow, decreasing power from " + event.getPower() + " to half");

		event.setPower(event.getPower() / 2);
	}
}
