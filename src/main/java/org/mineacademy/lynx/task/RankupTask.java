package org.mineacademy.lynx.task;

import org.bukkit.Statistic;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.mineacademy.fo.PlayerUtil;
import org.mineacademy.fo.TimeUtil;
import org.mineacademy.fo.model.Replacer;
import org.mineacademy.fo.model.SimpleRunnable;
import org.mineacademy.fo.model.SimpleScoreboard;
import org.mineacademy.fo.remain.Remain;
import org.mineacademy.lynx.claim.Claim;
import org.mineacademy.lynx.settings.PlayerData;

import lombok.NonNull;

public final class RankupTask extends SimpleRunnable {

	private final RankScoreboard scoreboard = new RankScoreboard();

	@Override
	public void run() {
		for (Player player : Remain.getOnlinePlayers()) {
			PlayerData cache = PlayerData.from(player);

			cache.getRank().upgrade(player);

			if (!this.scoreboard.isViewing(player))
				this.scoreboard.show(player);
		}
	}

	private static class RankScoreboard extends SimpleScoreboard {

		RankScoreboard() {
			this.setTitle("&f&lMINEACADEMY");
			this.addRows(
					"",
					"&7&l> &c&lPlayer Info: ",
					"&7Name: &f{player}",
					"&7Play Time: &f{play_time}",
					"&7Quest: &f{quest}",
					"&7Claim: &f{claim}",
					"",
					"&7&l> &c&lCurrent Rank: ",
					"&7Rank: &f{rank}",
					"",
					"&7&l> &c&lPvP: ",
					"&7Deaths: &f{deaths}",
					"&7Players Killed: &f{player_kills}",
					"&7Zombies Killed: &f{zombie_kills}");
		}

		@Override
		protected String replaceVariables(@NonNull Player player, @NonNull String message) {
			PlayerData data = PlayerData.from(player);
			Claim claim = Claim.findClaim(player.getLocation());

			return Replacer.replaceArray(message,
					"rank", data.getRank(),
					"quest", data.getActiveQuest() == null ? "none" : "&6" + data.getActiveQuest().getName(),
					"deaths", data.getDeaths(),
					"player_kills", data.getPlayerKills(),
					"zombie_kills", PlayerUtil.getStatistic(player, Statistic.KILL_ENTITY, EntityType.ZOMBIE),
					"play_time", TimeUtil.formatTimeShort(Remain.getPlaytimeSeconds(player)),
					"claim", claim != null ? "&6" + claim.getName() : "none");
		}
	}
}
