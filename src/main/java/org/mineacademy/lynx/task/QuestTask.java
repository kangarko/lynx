package org.mineacademy.lynx.task;

import org.bukkit.entity.Player;
import org.mineacademy.fo.model.SimpleRunnable;
import org.mineacademy.fo.remain.Remain;
import org.mineacademy.lynx.quest.model.Quest;
import org.mineacademy.lynx.settings.PlayerData;

/**
 * A runnable task that shows action bar to players
 * as well as ticks their quest progression
 */
public class QuestTask extends SimpleRunnable {

	@Override
	public void run() {
		for (final Player player : Remain.getOnlinePlayers()) {
			final PlayerData cache = PlayerData.from(player);
			final Quest activeQuest = cache.getActiveQuest();

			if (activeQuest != null) {
				// Call the tick method automatically,
				// see QuestComeHome for example use
				activeQuest.onTick(player, cache);

				// If the tick method did not cancel or complete the quest
				// Show progression bar
				if (cache.getActiveQuest() != null)
					Remain.sendActionBar(player, "&8[&6Quest&8] &f" + activeQuest.getActiveQuestActionBar(player) + " &8[&6/&8]");
			}
		}
	}
}