package org.mineacademy.lynx.task;

import org.bukkit.Bukkit;
import org.bukkit.Chunk;
import org.bukkit.World;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.mineacademy.fo.EntityUtil;
import org.mineacademy.fo.MathUtil;
import org.mineacademy.fo.model.SimpleRunnable;
import org.mineacademy.fo.remain.Remain;

public final class AntiLagTask extends SimpleRunnable {

	@Override
	public void run() {

		int maxEntitiesPerChunk = 20;

		for (World world : Bukkit.getWorlds())
			for (Chunk chunk : world.getLoadedChunks()) {
				int chunkEntities = 0;
				int chunkMonsters = 0;

				for (Entity entity : chunk.getEntities()) {
					if (EntityUtil.canBeCleaned(entity))
						if (chunkEntities++ >= maxEntitiesPerChunk) {
							entity.remove();

							System.out.println("Removing excessive item " + entity);
						}

					if (EntityUtil.isAggressive(entity)) {
						if (chunkMonsters++ >= maxEntitiesPerChunk) {
							entity.remove();

							System.out.println("Removing excessive monster " + entity);
						}
					}
				}
			}

		int tps = Remain.getTPS();

		for (Player player : Remain.getOnlinePlayers()) {
			int defaultViewDistance = Bukkit.getViewDistance();

			if (tps < 15)
				Remain.setViewDistance(player, MathUtil.range(defaultViewDistance / 2, 3, 10));
			else
				Remain.setViewDistance(player, defaultViewDistance);
		}
	}
}
