package org.mineacademy.lynx.task;

import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.mineacademy.fo.model.SimpleRunnable;
import org.mineacademy.fo.remain.Remain;

public final class ElytraTask extends SimpleRunnable {

	@Override
	public void run() {
		for (Player player : Remain.getOnlinePlayers()) {
			Location location = player.getLocation();

			if (player.isGliding() && location.getPitch() < -30)
				player.setVelocity(location.getDirection().multiply(1.5));
		}
	}
}
