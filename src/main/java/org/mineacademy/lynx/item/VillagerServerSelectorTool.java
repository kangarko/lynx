package org.mineacademy.lynx.item;

import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.entity.Villager;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.mineacademy.fo.PlayerUtil;
import org.mineacademy.fo.menu.model.ItemCreator;
import org.mineacademy.fo.menu.tool.Tool;
import org.mineacademy.fo.remain.CompMaterial;
import org.mineacademy.fo.remain.CompMetadata;
import org.mineacademy.fo.remain.CompProperty;
import org.mineacademy.fo.remain.CompVillagerProfession;
import org.mineacademy.fo.remain.Remain;
import org.mineacademy.lynx.menu.MinigamesMenu;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * Creates and automatically registers a custom tool/item
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class VillagerServerSelectorTool extends Tool implements Listener {

	/**
	 * The singleton of this class
	 */
	@Getter
	private static final VillagerServerSelectorTool instance = new VillagerServerSelectorTool();

	/**
	 * The ItemStack that Bukkit can give to players as the tool
	 *
	 * @return
	 */
	@Override
	public ItemStack getItem() {
		return ItemCreator.of(
				CompMaterial.VILLAGER_SPAWN_EGG,
				"Villager Server Selector",
				"",
				"Right click block to",
				"summon a villager that",
				"will open minigames",
				"selection menu on click.")
				.glow(true)
				.make();
	}

	/**
	 * What happens when a player clicks air/blocks holding this tool.
	 *
	 * @param event the event
	 */
	@Override
	protected void onBlockClick(PlayerInteractEvent event) {
		if (event.getAction() != Action.RIGHT_CLICK_BLOCK || !Remain.isInteractEventPrimaryHand(event))
			return;

		Player player = event.getPlayer();
		Villager villager = player.getWorld().spawn(event.getClickedBlock().getLocation().add(0.5, 1, 0.5), Villager.class);

		Remain.setCustomName(villager, "&cSelect Server");
		CompVillagerProfession.TOOLSMITH.apply(villager);

		Location copy = villager.getLocation();

		copy.setYaw(PlayerUtil.alignYaw(player.getLocation().getYaw() - 180, false));
		villager.teleport(copy);

		CompProperty.GRAVITY.apply(villager, false);
		CompProperty.AI.apply(villager, false);
		CompProperty.SILENT.apply(villager, true);

		CompMetadata.setMetadata(villager, "OpenMinigamesMenu", "OpenMinigamesMenu");
		event.setCancelled(true);

		if (event.getPlayer().getGameMode() != GameMode.CREATIVE)
			PlayerUtil.takeOnePiece(player, event.getItem());
	}

	@EventHandler
	public void onEntityClick(PlayerInteractEntityEvent event) {
		if (!(event.getRightClicked() instanceof Villager))
			return;

		if (CompMetadata.hasMetadata(event.getRightClicked(), "OpenMinigamesMenu"))
			new MinigamesMenu().displayTo(event.getPlayer());
	}
}
