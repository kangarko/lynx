package org.mineacademy.lynx.item;

import org.bukkit.entity.Ocelot;
import org.bukkit.entity.Player;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.mineacademy.fo.EntityUtil;
import org.mineacademy.fo.menu.model.ItemCreator;
import org.mineacademy.fo.menu.tool.Tool;
import org.mineacademy.fo.remain.CompMaterial;
import org.mineacademy.fo.remain.CompParticle;
import org.mineacademy.fo.remain.CompSound;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * Creates and automatically registers a custom tool/item
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class KittyCannon extends Tool {

	/**
	 * The singleton of this class
	 */
	@Getter
	private static final KittyCannon instance = new KittyCannon();

	/**
	 * The ItemStack that Bukkit can give to players as the tool
	 *
	 * @return
	 */
	@Override
	public ItemStack getItem() {
		return ItemCreator.of(
				CompMaterial.STICK,
				"&dKitty Cannon",
				"",
				"Right click air to",
				"launch flying kittens..",
				"Warning: They explode",
				"upon hitting the ground!")
				.glow(true)
				.make();
	}

	/**
	 * What happens when a player clicks air/blocks holding this tool.
	 *
	 * @param event the event
	 */
	@Override
	protected void onBlockClick(PlayerInteractEvent event) {
		if (event.getAction() != Action.RIGHT_CLICK_AIR)
			return;

		final Player player = event.getPlayer();
		final Ocelot cat = player.getWorld().spawn(player.getEyeLocation(), Ocelot.class);

		cat.setVelocity(player.getEyeLocation().getDirection().multiply(2.0));
		CompSound.ENTITY_CAT_AMBIENT.play(player);

		EntityUtil.trackFlying(cat, () -> CompParticle.END_ROD.spawn(cat.getLocation()));

		EntityUtil.trackFalling(cat, () -> {
			cat.getWorld().createExplosion(cat.getLocation(), 4F);
			cat.remove();

			CompSound.ENTITY_CAT_AMBIENT.play(player, 1F, 2F); // 0.1 -> 1F -> 2F
		});
	}

	/**
	 * Call onBlockClick even when air is clicked (bukkit will cancel the click event if there is no block)
	 *
	 * @return
	 */
	@Override
	protected boolean ignoreCancelled() {
		return false;
	}
}
