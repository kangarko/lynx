package org.mineacademy.lynx.bungee;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.mineacademy.fo.BungeeUtil;
import org.mineacademy.fo.Common;
import org.mineacademy.fo.annotation.AutoRegister;
import org.mineacademy.fo.bungee.BungeeListener;
import org.mineacademy.fo.bungee.message.IncomingMessage;
import org.mineacademy.fo.collection.SerializedMap;
import org.mineacademy.fo.menu.Menu;
import org.mineacademy.fo.remain.Remain;
import org.mineacademy.lynx.menu.MinigamesMenu;

import lombok.Getter;

@AutoRegister
public final class LynxBungeeListener extends BungeeListener {

	@Getter
	private static final LynxBungeeListener instance = new LynxBungeeListener();

	private LynxBungeeListener() {
		super("plugin:lynx", LynxMessage.class);
	}

	@Override
	public void onMessageReceived(Player player, IncomingMessage message) {

		final String serverName = message.getServerName();
		final LynxMessage action = message.getAction();

		if (action == LynxMessage.CHAT_MESSAGE) {
			String senderName = message.readString();
			String chatMessage = message.readString();

			for (Player online : Remain.getOnlinePlayers())
				Common.tellNoPrefix(online, "&8[&7Bungee/" + serverName + "&8] &3" + senderName + "&7: &f" + chatMessage);

			/*OutgoingMessage outgoingMessage = new OutgoingMessage(UUID.randomUUID(), LynxMessage.CHAT_MESSAGE);

			outgoingMessage.writeString(senderName);
			outgoingMessage.writeString(chatMessage);

			outgoingMessage.send(player);*/
		}

		else if (action == LynxMessage.DATA_SYNC) {
			SerializedMap data = message.readMap();

			RemoteGame.loadGames(data);

			for (Player online : Remain.getOnlinePlayers()) {
				Menu menu = Menu.getMenu(online);

				if (menu instanceof MinigamesMenu)
					menu.restartMenu();
			}
		}
	}

	@EventHandler(ignoreCancelled = true, priority = EventPriority.HIGHEST)
	public void onChat(AsyncPlayerChatEvent event) {
		Player player = event.getPlayer();
		String message = event.getMessage();

		BungeeUtil.sendPluginMessage(LynxMessage.CHAT_MESSAGE, player.getName(), message);

		/*OutgoingMessage outgoingMessage = new OutgoingMessage(UUID.randomUUID(), LynxMessage.CHAT_MESSAGE);
		outgoingMessage.writeString(player.getName());
		outgoingMessage.writeString(message);
		outgoingMessage.send(player);*/
	}
}
