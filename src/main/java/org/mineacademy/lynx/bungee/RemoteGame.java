package org.mineacademy.lynx.bungee;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.mineacademy.fo.collection.SerializedMap;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

@ToString
@RequiredArgsConstructor
public final class RemoteGame {

	private final static Map<String, RemoteGame> games = new HashMap<>();

	@Getter
	private final String serverName;

	@Getter
	private String gameName;

	@Getter
	private int players;

	private boolean canPlay;

	private boolean canSpectate;

	public boolean canPlay() {
		return this.canPlay;
	}

	public boolean canSpectate() {
		return this.canSpectate;
	}

	public void loadData(SerializedMap map) {
		this.gameName = map.getString("Game", "none");
		this.players = map.getInteger("Players", 0);
		this.canPlay = map.getBoolean("CanPlay", true);
		this.canSpectate = map.getBoolean("CanSpectate", true);
	}

	//
	// Static
	//

	public static int getGameCount() {
		return games.size();
	}

	public static List<RemoteGame> getGames() {
		return Collections.unmodifiableList(new ArrayList<>(games.values()));
	}

	public static RemoteGame findGame(String name) {
		return games.get(name);
	}

	public static void loadGames(SerializedMap map) {

		for (Map.Entry<String, Object> entry : map) {
			String serverName = entry.getKey();
			SerializedMap data = SerializedMap.of(entry.getValue());

			RemoteGame game = findGame(serverName);

			if (game == null) {
				game = new RemoteGame(serverName);

				games.put(serverName, game);
			}

			game.loadData(data);
		}
	}
}