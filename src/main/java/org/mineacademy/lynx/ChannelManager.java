package org.mineacademy.lynx;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.bukkit.entity.Player;

/**
 * A simple static-only class used to store player unique IDs and their channels for
 * our simple channel system.
 */
public final class ChannelManager {

	/**
	 * Stores a map of channel names by player unique IDs.
	 * <p>
	 * PLEASE NOT THAT THIS IS CLEARED ON /RELOAD OR SERVER RESTART - I will
	 * show you how to permanently store data in the settings video.
	 */
	private static final Map<UUID, String> playerChannels = new HashMap<>();

	/**
	 * Joins the player in channel, if he already had one the old channel is replaced.
	 *
	 * @param player
	 * @param channel
	 */
	public static void join(Player player, String channel) {
		playerChannels.put(player.getUniqueId(), channel);
	}

	/**
	 * Leaves a player from channel, if player had no channel, no action is taken.
	 *
	 * @param player
	 */
	public static void leave(Player player) {
		playerChannels.remove(player.getUniqueId());
	}

	/**
	 * Returns true if the player is in the given channel, by channel's name.
	 *
	 * @param player
	 * @param channelName
	 * @return
	 */
	public static boolean isJoined(Player player, String channelName) {
		final String channel = getChannel(player);

		return channel != null && channel.equals(channelName);
	}

	/**
	 * Return a channel for the player, or null if player has no channel
	 *
	 * @param player
	 * @return
	 */
	public static String getChannel(Player player) {
		return playerChannels.get(player.getUniqueId());
	}
}
