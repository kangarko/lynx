package org.mineacademy.lynx;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum Banana {

	ORANGE("ORANGE"),
	YELLOW("YELLOW");

	@Getter
	private final String key;

	public static Banana fromKey(String key) {
		for (Banana value : values())
			if (value.getKey().equalsIgnoreCase(key))
				return value;

		return null;
	}

	@Override
	public String toString() {
		return this.key;
	}
}
