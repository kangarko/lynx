package org.mineacademy.lynx;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.bukkit.entity.Player;
import org.bukkit.permissions.PermissionAttachment;

/**
 * A simple class storing one permission per player unique ID. This is lost on reloading or restarting,
 * I show you how to store data on disk permanently in Settings video
 */
public final class PermissionManager {

	/**
	 * The map that stores one permission per player (by his unique ID)
	 */
	private static final Map<UUID, PermissionAttachment> playerPermissions = new HashMap<>();

	/**
	 * Set a permission for player, overriding his old one if set
	 *
	 * @param player
	 * @param attachment
	 */
	public static void store(Player player, PermissionAttachment attachment) {
		playerPermissions.put(player.getUniqueId(), attachment);
	}

	/**
	 * Clear previously set permission for player. If he has not been assigned any, no action is taken
	 *
	 * @param player
	 */
	public static void remove(Player player) {
		playerPermissions.remove(player.getUniqueId());
	}

	/**
	 * Return true if the player has been given a permission
	 *
	 * @param player
	 * @return
	 */
	public static boolean hasPermissionSet(Player player) {
		return getPermission(player) != null;
	}

	/**
	 * Return the permission for player or none if not set
	 *
	 * @param player
	 * @return
	 */
	public static PermissionAttachment getPermission(Player player) {
		return playerPermissions.get(player.getUniqueId());
	}
}
