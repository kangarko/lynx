package org.mineacademy.lynx.listener;

import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.player.AsyncPlayerPreLoginEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.ItemStack;
import org.mineacademy.fo.PlayerUtil;
import org.mineacademy.fo.annotation.AutoRegister;
import org.mineacademy.fo.remain.CompMetadata;
import org.mineacademy.fo.remain.Remain;
import org.mineacademy.lynx.Lynx;
import org.mineacademy.lynx.boss.Boss;
import org.mineacademy.lynx.boss.SpawnedBoss;
import org.mineacademy.lynx.command.MenuCommand;
import org.mineacademy.lynx.database.LynxAdvancedDatabase;
import org.mineacademy.lynx.settings.PlayerData;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * An example of a class purely meant to listen for events (keep your code clean).
 * <p>
 * Do not forget to register events with \@AutoRegister or manually in onPluginStart()
 * or onReloadablesStart() in your main plugin's class!
 */
// We use lombok to create a private constructor to prevent you accidentally calling "new PlayerListener",
// instead of using PlayerListener.getInstance() which is preferred if using singleton
@AutoRegister
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class PlayerListener implements Listener {

	/**
	 * You can pass "new PlayerListener()" if not using this class later, but some people
	 * need to call this class throughout their plugin. In this case I recommend using a "singleton",
	 * which means creating only a singleton, which means creating only a single instance of this class
	 * as a final field and calling it anywhere.
	 */
	@Getter
	private static final PlayerListener instance = new PlayerListener();

	/**
	 * An example of listening to players connecting to your server.
	 * <p>
	 * Check out {@link AsyncPlayerPreLoginEvent} and {@link PlayerLoginEvent} for the same
	 * event which is called earlier before the actual connection is fully completed.
	 *
	 * @param event
	 */
	@EventHandler
	public void onJoin(PlayerJoinEvent event) {
		final Player player = event.getPlayer();

		LynxAdvancedDatabase.getInstance().loadCache(player, data -> {

			// Use custom settings file created for each player (we create the
			// file here if it does not exist) and return the tab list name,
			// or simply the player's original name if it was not set
			final String tabListName = data.getTabListName();

			player.setPlayerListName(tabListName);

			System.out.println("Database loaded (on join)");
		});
	}

	/**
	 * Example of listening to the event of player disconnecting
	 *
	 * @param event
	 */
	@EventHandler
	public void onQuit(PlayerQuitEvent event) {
		final Player player = event.getPlayer();

		LynxAdvancedDatabase.getInstance().saveCache(player, cache -> {

			// Save memory by removing the player data -- this won't remove data
			// from your disk, only removes the loaded instance in your plugin!
			// (Next time you call PlayerData#from, it will get loaded back again)
			PlayerData.remove(event.getPlayer());

			System.out.println("Database saved (on exit)");
		});
	}

	/**
	 * An example listener showing you how to check for hidden tags to connect
	 * a custom item and make it spawn an ender dragon when it is clicked.
	 *
	 * @param event
	 */
	@EventHandler
	public void onClick(PlayerInteractEvent event) {
		if (!Remain.isInteractEventPrimaryHand(event))
			return;

		final Player player = event.getPlayer();
		final Block block = event.getClickedBlock();
		final ItemStack hand = event.getItem();

		final String bossName = hand != null ? CompMetadata.getMetadata(hand, "CustomBoss") : null;

		if (bossName != null && block != null) {
			final EntityType entity = EntityType.valueOf(bossName);
			final Location location = block.getLocation().add(0.5, 5, 0.5);

			player.getWorld().spawnEntity(location, entity);

			if (player.getGameMode() != GameMode.CREATIVE)
				PlayerUtil.takeOnePiece(player, hand);

			event.setCancelled(true);
		}
	}

	@EventHandler
	public void onInventoryClose(InventoryCloseEvent event) {
		final Player player = (Player) event.getPlayer();

		if (CompMetadata.hasMetadata(player, "CustomInventory_" + Lynx.getNamed())) {
			final MenuCommand.Menu menu = (MenuCommand.Menu) player.getMetadata("CustomInventory_" + Lynx.getNamed()).get(0).value();

			menu.onClose(player);
			CompMetadata.removeTempMetadata(player, "CustomInventory_" + Lynx.getNamed());
		}
	}

	@EventHandler
	public void onDeath(PlayerDeathEvent event) {
		PlayerData.from(event.getEntity()).increaseDeaths();
	}

	@EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
	public void onKill(EntityDamageByEntityEvent event) {
		if (event.getDamager() instanceof Player && event.getEntity() instanceof LivingEntity) {
			final LivingEntity victim = (LivingEntity) event.getEntity();
			final Player killer = (Player) event.getDamager();
			final PlayerData cache = PlayerData.from(killer);

			final double remainingHealth = Remain.getHealth(victim) - Remain.getFinalDamage(event);

			if (remainingHealth <= 0) {

				if (victim instanceof Player) {
					cache.increasePlayerKills();

				} else {
					final SpawnedBoss victimBoss = Boss.findBoss(victim);

					if (victimBoss != null && cache.getRank().getName().equals(victimBoss.getBoss().getRankToUpgradeFrom()))
						cache.getRank().forceUpgrade(killer);
				}
			}
		}
	}
}
