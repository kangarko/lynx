package org.mineacademy.lynx.listener;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Supplier;

import javax.annotation.Nullable;

import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Egg;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.entity.ThrownExpBottle;
import org.bukkit.entity.ThrownPotion;
import org.bukkit.event.Cancellable;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockBurnEvent;
import org.bukkit.event.block.BlockDispenseEvent;
import org.bukkit.event.block.BlockEvent;
import org.bukkit.event.block.BlockExplodeEvent;
import org.bukkit.event.block.BlockFormEvent;
import org.bukkit.event.block.BlockFromToEvent;
import org.bukkit.event.block.BlockIgniteEvent;
import org.bukkit.event.block.BlockMultiPlaceEvent;
import org.bukkit.event.block.BlockPistonEvent;
import org.bukkit.event.block.BlockPistonExtendEvent;
import org.bukkit.event.block.BlockPistonRetractEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.block.BlockReceiveGameEvent;
import org.bukkit.event.block.BlockSpreadEvent;
import org.bukkit.event.block.CauldronLevelChangeEvent;
import org.bukkit.event.block.EntityBlockFormEvent;
import org.bukkit.event.block.SignChangeEvent;
import org.bukkit.event.entity.CreatureSpawnEvent;
import org.bukkit.event.entity.EntityBreakDoorEvent;
import org.bukkit.event.entity.EntityBreedEvent;
import org.bukkit.event.entity.EntityChangeBlockEvent;
import org.bukkit.event.entity.EntityCombustByEntityEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityEnterBlockEvent;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.event.entity.EntityInteractEvent;
import org.bukkit.event.entity.EntityPickupItemEvent;
import org.bukkit.event.entity.EntityPortalEvent;
import org.bukkit.event.entity.EntityPortalExitEvent;
import org.bukkit.event.entity.EntityShootBowEvent;
import org.bukkit.event.entity.EntityTargetEvent;
import org.bukkit.event.entity.ExpBottleEvent;
import org.bukkit.event.entity.PotionSplashEvent;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.event.entity.ProjectileLaunchEvent;
import org.bukkit.event.hanging.HangingBreakByEntityEvent;
import org.bukkit.event.hanging.HangingBreakEvent;
import org.bukkit.event.hanging.HangingPlaceEvent;
import org.bukkit.event.inventory.InventoryPickupItemEvent;
import org.bukkit.event.player.PlayerBedEnterEvent;
import org.bukkit.event.player.PlayerBucketEmptyEvent;
import org.bukkit.event.player.PlayerBucketEntityEvent;
import org.bukkit.event.player.PlayerBucketFillEvent;
import org.bukkit.event.player.PlayerBucketFishEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerEggThrowEvent;
import org.bukkit.event.player.PlayerFishEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;
import org.bukkit.event.player.PlayerTakeLecternBookEvent;
import org.bukkit.event.raid.RaidTriggerEvent;
import org.bukkit.event.vehicle.VehicleDamageEvent;
import org.bukkit.event.vehicle.VehicleDestroyEvent;
import org.bukkit.event.vehicle.VehicleEnterEvent;
import org.bukkit.event.vehicle.VehicleEntityCollisionEvent;
import org.bukkit.event.world.StructureGrowEvent;
import org.mineacademy.fo.Common;
import org.mineacademy.fo.annotation.AutoRegister;
import org.mineacademy.fo.event.RocketExplosionEvent;
import org.mineacademy.lynx.claim.Claim;

@AutoRegister
public final class ClaimListener implements Listener {

	public ClaimListener() {
		registerEvent("org.bukkit.event.player.PlayerBucketEntityEvent", ModernBucket::new, LegacyFish::new);
		registerEvent("org.bukkit.event.player.PlayerTakeLecternBookEvent", ModernTakeLecternBook::new);

		registerEvent("org.bukkit.event.entity.EntityEnterBlockEvent", ModernEntityEnterBlock::new);
		registerEvent("org.bukkit.event.entity.EntityPickupItemEvent", ModernEntityPickup::new, LegacyPlayerPickup::new);
		registerEvent("org.bukkit.event.entity.EntityBreedEvent", ModernEntityBreed::new);

		registerEvent("org.bukkit.event.block.BlockReceiveGameEvent", ModernBlockReceiveGame::new);
		registerEvent("org.bukkit.event.block.BlockExplodeEvent", ModernBlockExplode::new);
		registerEvent("org.bukkit.event.block.CauldronLevelChangeEvent", ModernCauldronLevelChange::new);

		registerEvent("org.bukkit.event.raid.RaidTriggerEvent", ModernRaidTrigger::new);
	}

	private void registerEvent(String classPath, Supplier<Listener> listener) {
		this.registerEvent(classPath, listener, null);
	}

	/*
	 * Register a class containing events not available in older Minecraft versions
	 */
	private void registerEvent(String classPath, Supplier<Listener> listener, @Nullable Supplier<Listener> fallbackListener) {
		try {
			Class.forName(classPath);

			Common.registerEvents(listener.get());

		} catch (final ClassNotFoundException ex) {
			if (fallbackListener != null)
				try {
					Common.registerEvents(fallbackListener.get());
				} catch (Throwable t) {
					// Completely unavailable
				}
		}
	}

	@EventHandler(priority = EventPriority.LOWEST)
	public void event(BlockBreakEvent event) {
		this.disallowBlockIfNotOwner(event.getPlayer(), event);
	}

	@EventHandler(priority = EventPriority.LOWEST)
	public void event(BlockBurnEvent event) {
		this.disallowIfInClaim(event);
	}

	@EventHandler(priority = EventPriority.LOWEST)
	public void event(BlockDispenseEvent event) {
		this.disallowIfInClaim(event);
	}

	class ModernBlockExplode implements Listener {

		@EventHandler(priority = EventPriority.LOWEST)
		public void event(BlockExplodeEvent event) {
			for (Block block : event.blockList())
				disallowIfInClaim(block, event);
		}
	}

	@EventHandler(priority = EventPriority.LOWEST)
	public void event(BlockFormEvent event) {
		this.disallowIfInClaim(event);
	}

	@EventHandler(priority = EventPriority.LOWEST)
	public void event(BlockFromToEvent event) {
		this.disallowIfInClaim(event.getToBlock(), event);
	}

	@EventHandler(priority = EventPriority.LOWEST)
	public void event(BlockIgniteEvent event) {
		this.disallowIfInClaim(event);
	}

	@EventHandler(priority = EventPriority.LOWEST)
	public void event(BlockMultiPlaceEvent event) {
		this.disallowBlockIfNotOwner(event.getPlayer(), event);
	}

	@EventHandler(priority = EventPriority.LOWEST)
	public void event(BlockPistonExtendEvent event) {
		for (Block block : event.getBlocks())
			this.disallowIfInClaim(block, event);
	}

	@EventHandler(priority = EventPriority.LOWEST)
	public void event(BlockPistonRetractEvent event) {
		try {
			for (Block block : event.getBlocks())
				this.disallowIfInClaim(block, event);

		} catch (NoSuchMethodError ex) {
			// Old MC lack the event.getBlocks method
		}
	}

	@EventHandler
	public void onPistonExtend(final BlockPistonExtendEvent event) {
		preventPistonMovement(event, event.getBlocks());
	}

	@EventHandler
	public void onPistonRetract(final BlockPistonRetractEvent event) {
		try {
			preventPistonMovement(event, event.getBlocks());
		} catch (final NoSuchMethodError ex) {
			// Old MC lack the event.getBlocks method
		}
	}

	/*
	 * Calculate if the blocks being pushed/pulled by piston cross an arena border
	 * and cancel the event if they do
	 */
	private void preventPistonMovement(final BlockPistonEvent event, List<Block> blocks) {
		final BlockFace direction = event.getDirection();
		final Claim pistonClaim = Claim.findClaim(event.getBlock().getLocation());

		// Clone the list otherwise it wont work
		blocks = new ArrayList<>(blocks);

		// Calculate blocks ONE step ahed in the push/pull direction
		for (int i = 0; i < blocks.size(); i++) {
			final Block block = blocks.get(i);

			blocks.set(i, block.getRelative(direction));
		}

		for (final Block block : blocks) {
			final Claim claim = Claim.findClaim(block.getLocation());

			if (claim != null && pistonClaim == null || claim == null && pistonClaim != null) {
				event.setCancelled(true);

				break;
			}
		}
	}

	@EventHandler(priority = EventPriority.LOWEST)
	public void event(BlockPlaceEvent event) {
		this.disallowBlockIfNotOwner(event.getPlayer(), event);
	}

	@EventHandler(priority = EventPriority.LOWEST)
	public void event(BlockSpreadEvent event) {
		this.disallowIfInClaim(event);
	}

	@EventHandler(priority = EventPriority.LOWEST)
	public void event(CreatureSpawnEvent event) {
		this.disallowIfInClaim(event.getEntity(), event);
	}

	@EventHandler(priority = EventPriority.LOWEST)
	public void event(EntityBlockFormEvent event) {
		this.disallowIfInClaim(event.getBlock(), event);
	}

	@EventHandler(priority = EventPriority.LOWEST)
	public void event(EntityBreakDoorEvent event) {
		this.disallowIfInClaim(event.getEntity(), event);
	}

	@EventHandler(priority = EventPriority.LOWEST)
	public void event(EntityChangeBlockEvent event) {
		this.disallowIfInClaim(event.getBlock(), event);
	}

	@EventHandler(priority = EventPriority.LOWEST)
	public void event(EntityCombustByEntityEvent event) {
		if (event.getCombuster() instanceof Player)
			this.disallowIfNotOwner(event.getEntity(), (Player) event.getCombuster(), event);

		else
			this.disallowIfInClaim(event.getEntity(), event);
	}

	@EventHandler(priority = EventPriority.LOWEST)
	public void event(EntityDamageByEntityEvent event) {
		if (event.getDamager() instanceof Player)
			this.disallowIfNotOwner(event.getEntity(), (Player) event.getDamager(), event);

		else
			this.disallowIfInClaim(event.getEntity(), event);
	}

	class ModernEntityEnterBlock implements Listener {

		@EventHandler(priority = EventPriority.LOWEST)
		public void event(EntityEnterBlockEvent event) {
			disallowIfInClaim(event.getBlock(), event);
		}
	}

	class ModernEntityBreed implements Listener {

		@EventHandler
		public void onEntityBreed(final EntityBreedEvent event) {

			if (event.getBreeder() instanceof Player)
				disallowIfNotOwner(event.getEntity(), (Player) event.getBreeder(), event);
		}
	}

	@EventHandler(priority = EventPriority.LOWEST)
	public void event(EntityExplodeEvent event) {
		this.disallowIfInClaim(event.getLocation(), event);
	}

	@EventHandler(priority = EventPriority.LOWEST)
	public void event(EntityInteractEvent event) {
		if (event.getEntity() instanceof Player)
			this.disallowIfNotOwner(event.getBlock().getLocation(), (Player) event.getEntity(), event);

		else
			this.disallowIfInClaim(event.getEntity(), event);
	}

	class ModernEntityPickup implements Listener {

		@EventHandler(priority = EventPriority.LOWEST)
		public void event(EntityPickupItemEvent event) {
			if (event.getEntity() instanceof Player)
				disallowIfNotOwner(event.getItem(), (Player) event.getEntity(), event);
			else
				disallowIfInClaim(event.getItem(), event);
		}
	}

	class LegacyPlayerPickup implements Listener {

		@EventHandler(priority = EventPriority.LOWEST)
		public void event(PlayerPickupItemEvent event) {
			disallowIfNotOwner(event.getItem(), event.getPlayer(), event);
		}
	}

	@EventHandler(priority = EventPriority.LOWEST)
	public void event(EntityPortalEvent event) {
		this.disallowIfInClaim(event.getFrom(), event);
	}

	@EventHandler(priority = EventPriority.LOWEST)
	public void event(EntityPortalExitEvent event) {
		if (event.getEntity() instanceof Player)
			this.disallowIfNotOwner(event.getEntity(), (Player) event.getEntity(), event);
		else
			this.disallowIfInClaim(event.getEntity(), event);
	}

	@EventHandler(priority = EventPriority.LOWEST)
	public void event(EntityShootBowEvent event) {
		if (event.getEntity() instanceof Player)
			this.disallowIfNotOwner(event.getProjectile(), (Player) event.getEntity(), event);
		else
			this.disallowIfInClaim(event.getProjectile(), event);
	}

	@EventHandler(priority = EventPriority.LOWEST)
	public void event(EntityTargetEvent event) {
		this.disallowIfInClaim(event.getTarget(), event);
	}

	@EventHandler(priority = EventPriority.LOWEST)
	public void event(ExpBottleEvent event) {
		ThrownExpBottle exp = event.getEntity();

		// Check for compatibility reasons
		if (event instanceof Cancellable) {
			if (exp.getShooter() instanceof Player)
				this.disallowIfNotOwner(exp, (Player) exp.getShooter(), (Cancellable) event);

			else if (event instanceof Cancellable) // Old MC compatibible
				this.disallowIfInClaim(exp, event);
		}
	}

	@EventHandler(priority = EventPriority.LOWEST)
	public void event(HangingBreakByEntityEvent event) {
		if (event.getRemover() instanceof Player)
			this.disallowIfNotOwner(event.getEntity(), (Player) event.getRemover(), event);
		else
			this.disallowIfInClaim(event.getEntity(), event);
	}

	@EventHandler(priority = EventPriority.LOWEST)
	public void event(HangingBreakEvent event) {
		this.disallowIfInClaim(event.getEntity(), event);
	}

	@EventHandler(priority = EventPriority.LOWEST)
	public void event(HangingPlaceEvent event) {
		this.disallowIfNotOwner(event.getEntity(), event.getPlayer(), event);
	}

	@EventHandler(priority = EventPriority.LOWEST)
	public void event(InventoryPickupItemEvent event) {
		this.disallowIfInClaim(event.getItem(), event);
	}

	@EventHandler
	public void event(final PlayerEggThrowEvent event) {
		final Egg egg = event.getEgg();
		final Claim arena = Claim.findClaim(egg.getLocation());

		if (arena != null && egg.getShooter() instanceof Player) {
			Player hatcher = (Player) egg.getShooter();

			if (!arena.getOwnerId().equals(hatcher.getUniqueId()))
				event.setHatching(false);
		}
	}

	@EventHandler(priority = EventPriority.LOWEST)
	public void event(PlayerBedEnterEvent event) {
		this.disallowIfNotOwner(event.getBed(), event.getPlayer(), event);
	}

	@EventHandler(priority = EventPriority.LOWEST)
	public void event(PlayerBucketEmptyEvent event) {
		this.disallowIfNotOwner(event.getBlockClicked(), event.getPlayer(), event);
	}

	class ModernBucket implements Listener {

		@EventHandler(priority = EventPriority.LOWEST)
		public void event(PlayerBucketEntityEvent event) {
			disallowIfNotOwner(event.getEntity(), event.getPlayer(), event);
		}
	}

	class LegacyFish implements Listener {

		@EventHandler(priority = EventPriority.LOWEST)
		public void event(PlayerBucketFishEvent event) {
			disallowIfNotOwner(event.getEntity(), event.getPlayer(), event);
		}
	}

	@EventHandler(priority = EventPriority.LOWEST)
	public void event(PlayerBucketFillEvent event) {
		this.disallowIfNotOwner(event.getBlockClicked(), event.getPlayer(), event);
	}

	@EventHandler(priority = EventPriority.LOWEST)
	public void event(PlayerDropItemEvent event) {
		this.disallowIfNotOwner(event.getItemDrop(), event.getPlayer(), event);
	}

	@EventHandler(priority = EventPriority.LOWEST)
	public void event(PlayerFishEvent event) {
		this.disallowIfNotOwner(event.getCaught(), event.getPlayer(), event);
	}

	@EventHandler(priority = EventPriority.LOWEST)
	public void event(PlayerInteractEntityEvent event) {
		this.disallowIfNotOwner(event.getRightClicked(), event.getPlayer(), event);
	}

	@EventHandler(priority = EventPriority.LOWEST)
	public void event(PlayerInteractEvent event) {
		if (event.hasBlock())
			this.disallowIfNotOwner(event.getClickedBlock(), event.getPlayer(), event);
	}

	class ModernTakeLecternBook implements Listener {

		@EventHandler(priority = EventPriority.LOWEST)
		public void event(PlayerTakeLecternBookEvent event) {
			disallowIfNotOwner(event.getLectern().getLocation(), event.getPlayer(), event);
		}
	}

	// TODO You may want to expand on this, this will simply prevent all teleportation
	// to a claim by a player who does not own the claim.
	//@EventHandler(priority = EventPriority.LOWEST)
	//public void event(PlayerTeleportEvent event) {
	//	this.disallowIfNotOwner(event.getTo(), event.getPlayer(), event);
	//}

	@EventHandler(priority = EventPriority.LOWEST)
	public void event(PotionSplashEvent event) {
		ThrownPotion projectile = event.getEntity();

		if (projectile.getShooter() instanceof Player)
			this.disallowIfNotOwner(projectile, (Player) projectile.getShooter(), event);

		else
			this.disallowIfInClaim(projectile, event);
	}

	@EventHandler(priority = EventPriority.LOWEST)
	public void event(ProjectileHitEvent event) {
		Claim claim = Claim.findClaim(event.getEntity().getLocation());

		if (claim != null) {
			System.out.println("Cancelling projectile " + event.getClass().getSimpleName());

			event.getEntity().remove();
		}
	}

	@EventHandler(priority = EventPriority.LOWEST)
	public void event(ProjectileLaunchEvent event) {
		Projectile projectile = event.getEntity();

		if (projectile.getShooter() instanceof Player)
			this.disallowIfNotOwner(projectile.getLocation(), (Player) projectile.getShooter(), event);
	}

	@EventHandler(priority = EventPriority.LOWEST)
	public void event(RocketExplosionEvent event) { // custom event from Foundation
		Projectile projectile = event.getProjectile();

		if (projectile.getShooter() instanceof Player)
			this.disallowIfNotOwner(projectile.getLocation(), (Player) projectile.getShooter(), event);
	}

	class ModernRaidTrigger implements Listener {

		@EventHandler(priority = EventPriority.LOWEST)
		public void event(RaidTriggerEvent event) {

			// Handle both cases
			disallowIfNotOwner(event.getRaid().getLocation(), event.getPlayer(), event);
			disallowIfNotOwner(event.getPlayer(), event.getPlayer(), event);
		}
	}

	@EventHandler(priority = EventPriority.LOWEST)
	public void event(SignChangeEvent event) {
		this.disallowBlockIfNotOwner(event.getPlayer(), event);
	}

	@EventHandler(priority = EventPriority.LOWEST)
	public void event(StructureGrowEvent event) {
		this.disallowIfInClaim(event.getLocation(), event);
	}

	@EventHandler(priority = EventPriority.LOWEST)
	public void event(VehicleDamageEvent event) {
		if (event.getAttacker() instanceof Player)
			this.disallowIfNotOwner(event.getVehicle().getLocation(), (Player) event.getAttacker(), event);

		else
			this.disallowIfInClaim(event.getVehicle(), event);
	}

	@EventHandler
	public void onVehicleEnter(final VehicleEnterEvent event) {
		if (event.getEntered() instanceof Player)
			disallowIfNotOwner(event.getVehicle(), (Player) event.getEntered(), event);
	}

	@EventHandler
	public void onVehicleDamage(final VehicleDamageEvent event) {
		if (event.getAttacker() instanceof Player)
			disallowIfNotOwner(event.getVehicle(), (Player) event.getAttacker(), event);
	}

	@EventHandler
	public void onVehicleDestroy(final VehicleDestroyEvent event) {
		if (event.getAttacker() instanceof Player)
			disallowIfNotOwner(event.getVehicle(), (Player) event.getAttacker(), event);
	}

	@EventHandler
	public void onVehicleCollision(final VehicleEntityCollisionEvent event) {
		if (event.getEntity() instanceof Player)
			disallowIfNotOwner(event.getVehicle(), (Player) event.getEntity(), event);
	}

	class ModernBlockReceiveGame implements Listener {

		@EventHandler(priority = EventPriority.LOWEST)
		public void event(BlockReceiveGameEvent event) {

			if (event.getEntity() instanceof Player)
				disallowBlockIfNotOwner((Player) event.getEntity(), event);

			else
				disallowIfInClaim(event.getBlock(), event);
		}
	}

	class ModernCauldronLevelChange implements Listener {

		@EventHandler
		public void event(CauldronLevelChangeEvent event) {
			if (event.getEntity() instanceof Player)
				disallowBlockIfNotOwner((Player) event.getEntity(), event);

			else
				disallowIfInClaim(event.getBlock(), event);
		}
	}

	/*
	 * Disallow the block event if the given player is not an owner of the
	 * claim at the given block location.
	 */
	private <T extends BlockEvent & Cancellable> void disallowBlockIfNotOwner(Player player, T event) {
		if (event.getBlock() != null)
			this.disallowIfNotOwner(event.getBlock(), player, event);
	}

	/*
	 * Disallow the block event if the location is within any claim for any reason.
	 */
	private void disallowIfInClaim(Block block, Cancellable event) {
		if (block != null)
			this.disallowIfInClaim(block.getLocation(), event);
	}

	/*
	 * Disallow the cancellable event if the entity is within any claim for any reason.
	 */
	private void disallowIfInClaim(Entity entity, Cancellable event) {
		if (entity != null)
			this.disallowIfInClaim(entity.getLocation(), event);
	}

	/*
	 * Disallow the block event if the block is within any claim for any reason.
	 */
	private <T extends BlockEvent & Cancellable> void disallowIfInClaim(T event) {
		if (event.getBlock() != null)
			this.disallowIfInClaim(event.getBlock().getLocation(), event);
	}

	/*
	 * Disallow the cancellable event if the location is within any claim for any reason.
	 */
	private void disallowIfInClaim(Location location, Cancellable event) {
		Claim claim = Claim.findClaim(location);

		if (claim != null) {
			System.out.println("Cancelling generic " + event.getClass().getSimpleName());

			event.setCancelled(true);
		}
	}

	/*
	 * Disallow the cancellable event if the given player is not an owner of the
	 * claim at the given block location.
	 */
	private void disallowIfNotOwner(Block blockClicked, Player byWhom, Cancellable event) {
		if (blockClicked != null)
			this.disallowIfNotOwner(blockClicked.getLocation(), byWhom, event);
	}

	/*
	 * Disallow the cancellable event if the given player is not an owner of the
	 * claim at the given entity location.
	 */
	private void disallowIfNotOwner(Entity entityManipulated, Player byWhom, Cancellable event) {
		if (entityManipulated != null)
			this.disallowIfNotOwner(entityManipulated.getLocation(), byWhom, event);
	}

	/*
	 * Disallow the cancellable event if the given player is not an owner of the
	 * claim at the given location.
	 */
	private void disallowIfNotOwner(Location location, Player player, Cancellable event) {
		Claim claim = Claim.findClaim(location);

		if (claim != null && !claim.getOwnerId().equals(player.getUniqueId())) {
			System.out.println("Cancelling " + event.getClass().getSimpleName());

			Common.tellTimedNoPrefix(1, player, "&8[#caaf58Claim&8] &7You can't do this in claim #c5b4e5" + claim.getName() + " &7owned by #c5b4e5" + claim.getOwnerName() + "&7.");
			event.setCancelled(true);
		}
	}
}
