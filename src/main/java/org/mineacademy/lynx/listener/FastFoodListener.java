package org.mineacademy.lynx.listener;

import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityRegainHealthEvent;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.inventory.ItemStack;
import org.mineacademy.fo.MathUtil;
import org.mineacademy.fo.PlayerUtil;
import org.mineacademy.fo.annotation.AutoRegister;
import org.mineacademy.fo.model.Food;
import org.mineacademy.fo.remain.CompSound;
import org.mineacademy.fo.remain.Remain;

@AutoRegister
public final class FastFoodListener implements Listener {

	@EventHandler
	public void onClick(PlayerInteractEvent event) {
		if (!Remain.isInteractEventPrimaryHand(event) || !event.getAction().toString().contains("RIGHT_CLICK"))
			return;

		final Player player = event.getPlayer();

		if (player.getGameMode() == GameMode.SURVIVAL) {
			final ItemStack hand = player.getItemInHand();
			final Food food = Food.getFood(hand);

			final double currentHealth = Remain.getHealth(player);
			final double maxHealth = Remain.getMaxHealth(player);

			if (food != null) {

				if (currentHealth < maxHealth) {
					final double newHealth = MathUtil.range(Remain.getHealth(player) + food.getHealthPoints(), 0, maxHealth);

					player.setHealth(newHealth);
					CompSound.ENTITY_GENERIC_EAT.play(player);

					PlayerUtil.takeOnePiece(player, hand);
				}

				event.setCancelled(true);
			}
		}
	}

	@EventHandler(priority = EventPriority.HIGHEST)
	public void onFoodLevelChange(FoodLevelChangeEvent event) {
		event.setFoodLevel(20);
	}

	@EventHandler
	public void onJoin(PlayerJoinEvent event) {
		event.getPlayer().setFoodLevel(20);
	}

	@EventHandler(priority = EventPriority.HIGHEST)
	public void onHealthChange(EntityRegainHealthEvent event) {
		final EntityRegainHealthEvent.RegainReason reason = event.getRegainReason();

		if (reason == EntityRegainHealthEvent.RegainReason.REGEN || reason == EntityRegainHealthEvent.RegainReason.SATIATED) {
			event.setCancelled(true);
		}
	}
}