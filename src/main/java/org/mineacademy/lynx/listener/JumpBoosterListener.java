package org.mineacademy.lynx.listener;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import org.bukkit.Effect;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerToggleFlightEvent;
import org.mineacademy.fo.Common;
import org.mineacademy.fo.annotation.AutoRegister;
import org.mineacademy.fo.model.SimpleRunnable;
import org.mineacademy.fo.remain.CompMaterial;
import org.mineacademy.fo.remain.CompSound;
import org.mineacademy.fo.remain.Remain;

@AutoRegister
public final class JumpBoosterListener implements Listener {

	private final Set<UUID> boostedPlayers = new HashSet<>();

	public JumpBoosterListener() {
		Common.runTimer(1, new BoostTask());
	}

	private class BoostTask extends SimpleRunnable {
		@Override
		public void run() {
			for (final Player player : Remain.getOnlinePlayers())
				if (player.getGameMode() == GameMode.SURVIVAL) {
					final boolean isFalling = player.getVelocity().getY() < 0.1;

					if (player.isOnGround()) {
						player.setAllowFlight(true);

						boostedPlayers.remove(player.getUniqueId());
					} else if (isFalling)
						player.setAllowFlight(false);
				}
		}
	}

	@EventHandler
	public void onFlightToggle(PlayerToggleFlightEvent event) {
		final Player player = event.getPlayer();
		final Location location = player.getLocation();

		if (player.getGameMode() == GameMode.SURVIVAL) {
			event.setCancelled(true);

			player.setAllowFlight(false);
			player.setFlying(false);

			if (!this.boostedPlayers.contains(player.getUniqueId())) {
				player.playEffect(location, Effect.ENDER_SIGNAL, null);
				CompSound.ENTITY_ENDER_DRAGON_AMBIENT.play(player);

				player.setVelocity(location.getDirection().multiply(2).setY(1));
				this.boostedPlayers.add(player.getUniqueId());
			}
		}
	}

	@EventHandler
	public void onInteract(PlayerInteractEvent event) {
		final Player player = event.getPlayer();
		final Block block = event.getClickedBlock();

		if (event.getAction() == Action.PHYSICAL && block != null && CompMaterial.isWoodPressurePlate(block.getType())) {
			//if (Claim.findClaim(player.getLocation()).isValid()) {
			player.setVelocity(player.getLocation().getDirection().multiply(3).setY(3));

			Common.runLater(2, () -> this.boostedPlayers.add(player.getUniqueId()));
			//}
		}
	}

	@EventHandler
	public void onInteract(EntityDamageEvent event) {
		final Entity entity = event.getEntity();
		final UUID uuid = entity.getUniqueId();

		if (this.boostedPlayers.remove(uuid) && event.getCause() == EntityDamageEvent.DamageCause.FALL) {
			final float fallDamage = entity.getFallDistance();

			if (fallDamage < 15)
				event.setCancelled(true);
			else
				event.setDamage(event.getDamage() / 2);
		}
	}
}