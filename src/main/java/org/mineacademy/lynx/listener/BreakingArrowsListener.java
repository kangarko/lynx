package org.mineacademy.lynx.listener;

import org.bukkit.block.Block;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.mineacademy.fo.annotation.AutoRegister;
import org.mineacademy.fo.remain.CompMaterial;
import org.mineacademy.fo.remain.CompSound;
import org.mineacademy.fo.remain.Remain;

@AutoRegister
public final class BreakingArrowsListener implements Listener {

	@EventHandler
	public void onProjectileHit(ProjectileHitEvent event) {
		final Projectile projectile = event.getEntity();
		final Block hitBlock = Remain.getHitBlock(event);

		if (projectile instanceof Arrow && projectile.getShooter() instanceof Player
				&& !CompMaterial.isAir(hitBlock) && hitBlock.getType().toString().contains("GLASS")) {

			hitBlock.setType(CompMaterial.AIR.getMaterial());
			CompSound.BLOCK_GLASS_BREAK.play(hitBlock.getLocation());

			projectile.remove();
		}
	}
}