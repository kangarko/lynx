package org.mineacademy.lynx.listener;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerChangedWorldEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.mineacademy.fo.Common;
import org.mineacademy.fo.MathUtil;
import org.mineacademy.fo.annotation.AutoRegister;
import org.mineacademy.fo.collection.StrictMap;
import org.mineacademy.fo.remain.CompAttribute;
import org.mineacademy.fo.remain.Remain;
import org.mineacademy.lynx.settings.PlayerData;

@AutoRegister
public final class CustomHealthListener implements Listener {

	private static final StrictMap<String, Double /*20*/> WORLD_MAX_HEALTH = new StrictMap<>();

	static {
		// <3 equals 2 health points
		// a normal player has 10 <3 that means he has 20 health points
		WORLD_MAX_HEALTH.put("world", 40.D);
		WORLD_MAX_HEALTH.put("nature", 10.D);
	}

	@EventHandler
	public void onJoin(PlayerJoinEvent event) {
		setMaxHealth(event.getPlayer());
	}

	@EventHandler
	public void onWorldChange(PlayerChangedWorldEvent event) {

		final Player player = event.getPlayer();
		final PlayerData cache = PlayerData.from(player);

		// Save old health
		cache.setHealth(event.getFrom(), Remain.getHealth(player));
		Common.log("Storing health to " + Remain.getHealth(player));

		// Restore saved health
		Common.runLater(() -> {
			final double newHealth = cache.getHealth(player.getWorld());
			Common.log("Setting new health to " + newHealth);

			setMaxHealth(event.getPlayer());
			player.setHealth(MathUtil.range(newHealth, 0, CompAttribute.MAX_HEALTH.get(player)));
		});
	}

	private void setMaxHealth(Player player) {
		final Double health = WORLD_MAX_HEALTH.get(player.getWorld().getName());

		if (health != null)
			CompAttribute.MAX_HEALTH.set(player, health);
	}
}