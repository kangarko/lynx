package org.mineacademy.lynx.listener;

import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityCombustEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.mineacademy.fo.HealthBarUtil;
import org.mineacademy.fo.annotation.AutoRegister;
import org.mineacademy.fo.remain.Remain;
import org.mineacademy.lynx.boss.Boss;
import org.mineacademy.lynx.boss.SpawnedBoss;

@AutoRegister
public final class BossListener implements Listener {

	@EventHandler
	public void onBossDamage(EntityDamageByEntityEvent event) {
		Entity damager = event.getDamager();

		if (!(event.getEntity() instanceof LivingEntity))
			return;

		LivingEntity victim = (LivingEntity) event.getEntity();
		SpawnedBoss damagerBoss = Boss.findBoss(damager);

		if (damagerBoss != null)
			damagerBoss.getBoss().onBossAttack(victim, event);

		SpawnedBoss victimBoss = Boss.findBoss(victim);

		if (victimBoss != null && damager instanceof Player) {

			/*Player player = (Player) damager;
			ItemStack hand = player.getItemInHand();

			if (hand.containsEnchantment(HideEnchant.getInstance())) {
				// do your code
			}*/

			HealthBarUtil.display((Player) damager, victim, Remain.getFinalDamage(event));
		}
	}

	@EventHandler(ignoreCancelled = true)
	public void onBurn(EntityCombustEvent event) {
		SpawnedBoss boss = Boss.findBoss(event.getEntity());

		if (boss != null)
			event.setCancelled(true);
	}
}
