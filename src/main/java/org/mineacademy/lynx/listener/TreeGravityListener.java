package org.mineacademy.lynx.listener;

import java.util.List;

import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.FallingBlock;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.util.Vector;
import org.mineacademy.fo.BlockUtil;
import org.mineacademy.fo.MathUtil;
import org.mineacademy.fo.annotation.AutoRegister;
import org.mineacademy.fo.remain.CompMaterial;
import org.mineacademy.fo.remain.CompSound;
import org.mineacademy.fo.remain.Remain;

@AutoRegister
public final class TreeGravityListener implements Listener {

	@EventHandler
	public void onBlockBreak(BlockBreakEvent event) {
		final Block block = event.getBlock();
		final Player player = event.getPlayer();

		if (!BlockUtil.isLogOnGround(block))
			return;

		if (player.getGameMode() != GameMode.SURVIVAL)
			return;

		// Get all other logs plus other leaves
		final List<Block> treeBlocks = BlockUtil.getTreePartsUp(block);

		for (final Block treeBlock : treeBlocks) {
			//if (treeBlock.getX() == block.getX() && treeBlock.getZ() == block.getZ() && CompMaterial.isLog(treeBlock.getType())) {
			final FallingBlock falling = Remain.spawnFallingBlock(treeBlock);

			if (CompMaterial.isLeaves(treeBlock.getType()))
				falling.setDropItem(false);

			treeBlock.setType(Material.AIR);

			CompSound.BLOCK_CHEST_CLOSE.play(player, 1F, 0.1F);

			final Vector direction = player.getLocation().getDirection();
			final double multiplier = MathUtil.range((treeBlock.getY() - block.getY()) / 5D, 0.1D, 5.5D);
			falling.setVelocity(direction.multiply(multiplier));
			//}
		}
	}
}