package org.mineacademy.lynx.listener;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.bukkit.Location;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.mineacademy.fo.annotation.AutoRegister;
import org.mineacademy.fo.remain.Remain;

@AutoRegister
public final class RespawnListener implements Listener {

	private Map<UUID, Location> deathLocations = new HashMap<>();

	@EventHandler
	public void onDeath(PlayerDeathEvent event) {
		this.deathLocations.put(event.getEntity().getUniqueId(), event.getEntity().getLocation());

		Remain.respawn(event.getEntity());
	}

	@EventHandler
	public void onRespawn(PlayerRespawnEvent event) {
		Location deathLocation = this.deathLocations.remove(event.getPlayer().getUniqueId());

		if (deathLocation != null)
			event.setRespawnLocation(deathLocation);
	}
}
