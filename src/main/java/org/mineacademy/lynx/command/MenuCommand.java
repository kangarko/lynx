package org.mineacademy.lynx.command;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.Sign;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.Merchant;
import org.bukkit.inventory.MerchantRecipe;
import org.mineacademy.fo.Common;
import org.mineacademy.fo.annotation.AutoRegister;
import org.mineacademy.fo.command.SimpleCommand;
import org.mineacademy.fo.menu.model.ItemCreator;
import org.mineacademy.fo.model.SimpleComponent;
import org.mineacademy.fo.remain.CompMaterial;
import org.mineacademy.fo.remain.CompMetadata;
import org.mineacademy.fo.remain.Remain;
import org.mineacademy.lynx.Lynx;
import org.mineacademy.lynx.menu.PreferencesMenu;
import org.mineacademy.lynx.settings.MenuData;

/**
 * An example of creating a command that creates a menu of tools
 */
@AutoRegister
public final class MenuCommand extends SimpleCommand {

	public MenuCommand() {
		super("menu");

		setUsage("<merch/book/rules/sign/workbench/inventory/fo/file>");
		setMinArguments(1);
	}

	// See SpawnEntityCommand for help and comments.
	@Override
	protected void onCommand() {
		checkConsole();

		final Player player = getPlayer();
		final String param = args[0].toLowerCase();

		if ("merch".equals(param)) {
			final Merchant merchant = Bukkit.createMerchant(Common.colorize("&4Custom Villager Shop"));
			final List<MerchantRecipe> recipes = new ArrayList<>();

			final MerchantRecipe first = new MerchantRecipe(new ItemStack(Material.DIAMOND, 5), 1);
			first.addIngredient(new ItemStack(Material.BREAD, 20));
			recipes.add(first);

			final MerchantRecipe second = new MerchantRecipe(new ItemStack(Material.ARROW, 1), 999);
			second.addIngredient(new ItemStack(Material.APPLE, 3));
			recipes.add(second);

			merchant.setRecipes(recipes);

			player.openMerchant(merchant, true);

		} else if ("book".equals(param)) {
			final ItemStack book = ItemCreator.of(
					CompMaterial.WRITTEN_BOOK, "Story Of My Life", "", "Click to read!")
					.bookAuthor("Notch")
					.bookTitle("How I Created Minecraft")
					.bookPages("Page one!\n&cPage one line two :)", "Page two!")
					.make();

			Remain.openBook(player, book);

		} else if ("rules".equals(param)) {
			SimpleComponent
					.of("&7Click ")

					.append("&6[here]")
					.onHover("Click to read rules!")
					.onClickRunCmd("/menu book")

					.append(" &7to read our server rules.")
					.send(player);

		} else if ("sign".equals(param)) {
			final Block target = Remain.getTargetBlock(player, 5);

			if (target.getState() instanceof Sign)
				Remain.openSign(player, target);
			else
				tellError("You must be looking at a sign!");

		} else if ("workbench".equals(param)) {
			player.openWorkbench(null, true);

		} else if ("inventory".equals(param)) {
			final String title = "&9Admin Panel";
			final Inventory inventory = Bukkit.createInventory(player, 9, Common.colorize(title));
			inventory.setItem(1, ItemCreator.of(CompMaterial.ARROW, "&6Nice Arrow").make());

			final Menu menu = new Menu();
			menu.inventory = inventory;

			// You can store the entire menu object for the player as his opened inventory
			CompMetadata.setTempMetadata(player, "CustomInventory_" + Lynx.getNamed(), menu);
			player.openInventory(inventory);

		} else if ("fo".equals(param))
			new PreferencesMenu(player).displayTo(player);

		else if ("file".equals(param)) {
			checkArgs(2, "Please specify the menu name to open. Available: " + Common.join(MenuData.getMenuNames()));

			final MenuData data = MenuData.findMenu(args[1]);
			checkNotNull(data, "No such menu: '{1}'. Available: " + Common.join(MenuData.getMenuNames()));

			data.displayTo(player);
		} else
			returnInvalidArgs();
	}

	@Override
	protected List<String> tabComplete() {

		switch (this.args.length) {
			case 1:
				return completeLastWord("merch", "book", "rules", "sign", "workbench", "inventory", "fo", "file");

			case 2:
				return "file".equalsIgnoreCase(args[0]) ? completeLastWord(MenuData.getMenuNames()) : NO_COMPLETE;
		}

		return NO_COMPLETE;
	}

	// A very primitive example of a linked menu system (please see Foundation-based approach in the next
	// video in Menus and GUIs to be able to extend this and create your own system in a month or two
	public static class Menu {

		Inventory inventory;

		public void onClose(Player player) {
			System.out.println("Closing custom menu!");
		}
	}
}
