package org.mineacademy.lynx.command;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.mineacademy.fo.Common;
import org.mineacademy.fo.ItemUtil;
import org.mineacademy.fo.annotation.AutoRegister;
import org.mineacademy.fo.command.SimpleCommand;
import org.mineacademy.fo.model.SimpleComponent;
import org.mineacademy.fo.remain.Remain;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;

/**
 * An example of creating a command that creates a menu of tools
 */
@AutoRegister
public final class ComponentCommand extends SimpleCommand {

	public ComponentCommand() {
		super("component");

		setUsage("<native/fo>");
		setMinArguments(1);
	}

	// See SpawnEntityCommand for help and comments.
	@Override
	protected void onCommand() {
		checkConsole();

		String type = args[0].toLowerCase();

		if ("native".equals(type)) {
			TextComponent first = new TextComponent(TextComponent.fromLegacyText(Common.colorize("&cHello world&7, I hold ")));

			first.setClickEvent(new ClickEvent(ClickEvent.Action.OPEN_URL, "https://mineacademy.org"));
			first.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, TextComponent.fromLegacyText(Common.colorize("&6Click this text\nto visit our website!"))));

			ItemStack item = getPlayer().getItemInHand();
			boolean air = item.getType() == Material.AIR;

			TextComponent second = new TextComponent(TextComponent.fromLegacyText(Common.colorize(air ? "Air" : ItemUtil.bountifyCapitalized(item.getType()))));
			second.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_ITEM, new BaseComponent[] { new TextComponent(Remain.toJson(item)) }));
			second.setColor(ChatColor.GRAY);

			TextComponent main = new TextComponent("");

			main.addExtra(first);
			main.addExtra(second);

			getPlayer().spigot().sendMessage(main);

		} else if ("foundation".equals(type)) {

			ItemStack item = getPlayer().getItemInHand();
			boolean air = item.getType() == Material.AIR;

			SimpleComponent
					.of("&cHello world&7, I hold ")
					.onClickRunCmd("Hello everyone!")
					.onHover("&6Click this text\nto run a secret command!")

					.append(air ? "Air" : ItemUtil.bountifyCapitalized(item.getType()))
					.onHover(item)

					.send(getPlayer());
		}
	}
}
