package org.mineacademy.lynx.command;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Player;
import org.mineacademy.fo.BlockUtil;
import org.mineacademy.fo.Common;
import org.mineacademy.fo.RandomUtil;
import org.mineacademy.fo.annotation.AutoRegister;
import org.mineacademy.fo.command.SimpleCommand;
import org.mineacademy.fo.model.SimpleRunnable;
import org.mineacademy.fo.remain.CompMaterial;
import org.mineacademy.fo.remain.CompProperty;
import org.mineacademy.fo.remain.CompSound;

@AutoRegister
public final class RandomTeleportCommand extends SimpleCommand {

	private static final int LOCATION_SEARCH_ATTEMPTS = 10;
	private static final int HEIGHT_DECREASE_PER_TELEPORT = 80;

	public RandomTeleportCommand() {
		super("randomtp/wild/build");

		this.setTellPrefix("&8[&dRandomTp&8]&7");
		this.setCooldown(8, TimeUnit.SECONDS);
		this.setCooldownMessage("Please wait {duration} second(s) before running this command again!");
		this.setMinArguments(1);
		this.setUsage("<range> [player]");
	}

	@Override
	protected void onCommand() {

		final int range = findNumber(0 /*args[0]*/, 100, 10_000, "Please specify a whole number from {min} to {max}.");
		final Player player = args.length == 2 ? this.findPlayer(args[1]) : this.isPlayer() ? this.getPlayer() : null;
		this.checkNotNull(player, "If you are a console, you must specify the target player.");

		this.tell("A random location is being searched for...");

		final Location randomLocation = this.findRandomLocation(player.getWorld().getSpawnLocation(), range);
		this.checkNotNull(randomLocation, "Could not find any suitable location, try again later.");

		// DO NOT USE > always loads the chunk
		//if (!randomLocation.getChunk().isLoaded())
		// Use the alternative approach instead where you get the world first
		// We convert block location into a chunk location by dividing it by 16 (chunk is 16X16 blocks wide)
		if (randomLocation.getWorld().isChunkLoaded(randomLocation.getBlockX() / 16, randomLocation.getBlockZ() / 16))
			randomLocation.getChunk().load(true);

		randomLocation.setYaw(0);
		randomLocation.setPitch(90);

		// Fix: It's pointless to send particle effect because the player will teleport immediatelly and never see it,
		// instead we send it on line 149
		//player.playEffect(player.getLocation(), Effect.ENDER_SIGNAL, null);
		CompSound.ENTITY_ARROW_HIT_PLAYER.play(player);

		this.teleportToLocation(randomLocation);
	}

	private Location findRandomLocation(Location centerLocation, int range) {

		for (int i = 0; i < LOCATION_SEARCH_ATTEMPTS; i++) {
			final Location location = RandomUtil.nextLocation(centerLocation, range, false);
			final int highestPointY = BlockUtil.findHighestBlock(location, material -> !CompMaterial.isLeaves(material));

			if (highestPointY != -1) {
				location.setY(highestPointY);

				final Block block = location.getBlock();
				final Block blockAbove = block.getRelative(BlockFace.UP);
				final Block blockBelow = block.getRelative(BlockFace.DOWN);

				if (blockBelow.getType().isSolid() && CompMaterial.isAir(block) && CompMaterial.isAir(blockAbove))
					return location;

			}
		}

		return null;
	}

	private void teleportToLocation(Location destination) {
		Common.runTimer(20 * 2, 15 /*repeat every 0.75 second*/, new SimpleRunnable() {

			private int currentHeight = destination.getWorld().getMaxHeight();
			private boolean firstTeleport = false;
			private float previousWalkSpeed, previousFlySpeed;

			@Override
			public void run() {
				final Player player = getPlayer();

				if (!this.firstTeleport) {
					this.previousWalkSpeed = player.getWalkSpeed();
					this.previousFlySpeed = player.getFlySpeed();

					player.setWalkSpeed(0F);
					player.setFlySpeed(0F);
					player.setAllowFlight(true);
					player.setFlying(true);

					//player.setGravity(false);
					//player.setInvulnerable(true);
					CompProperty.GRAVITY.apply(player, false);
					CompProperty.INVULNERABLE.apply(player, true);

					this.firstTeleport = true;
				}

				if (this.currentHeight <= HEIGHT_DECREASE_PER_TELEPORT) {
					this.cancel();

					final Location groundLocation = player.getLocation().subtract(0, this.currentHeight, 0);

					groundLocation.add(0.5, 0, 0.5);
					groundLocation.setYaw(90);
					groundLocation.setPitch(0);

					player.teleport(groundLocation);

					player.setWalkSpeed(this.previousWalkSpeed);
					player.setFlySpeed(this.previousFlySpeed);
					player.setAllowFlight(false);
					player.setFlying(false);

					CompProperty.GRAVITY.apply(player, true);
					CompProperty.INVULNERABLE.apply(player, false);

					// TODO Old Minecraft releases may take damage in survival, you need to give player
					// invisible metadata at the start of the teleport and cancel the EntityDamageByXEvent (2 events)
					// when player has them. Remove the metadata here on finish.

					return;
				}

				this.currentHeight -= HEIGHT_DECREASE_PER_TELEPORT;
				player.teleport(destination.clone().add(0, this.currentHeight, 0));

				// Fix: Show the animation after the teleport by delaying it
				Common.runLater(() -> {
					player.playEffect(player.getLocation(), Effect.ENDER_SIGNAL, null);

					CompSound.ENTITY_ENDER_DRAGON_AMBIENT.play(player);
				});
			}
		});
	}

	@Override
	protected List<String> tabComplete() {
		return this.args.length == 1 ? this.completeLastWord(100) : this.args.length == 2 ? this.completeLastWordPlayerNames() : NO_COMPLETE;
	}
}
