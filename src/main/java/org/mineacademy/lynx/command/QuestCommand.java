package org.mineacademy.lynx.command;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.bukkit.entity.Player;
import org.mineacademy.fo.Common;
import org.mineacademy.fo.ReflectionUtil;
import org.mineacademy.fo.TimeUtil;
import org.mineacademy.fo.annotation.AutoRegister;
import org.mineacademy.fo.command.SimpleCommand;
import org.mineacademy.fo.model.ChatPaginator;
import org.mineacademy.lynx.menu.QuestsMenu;
import org.mineacademy.lynx.quest.QuestStartPrompt;
import org.mineacademy.lynx.quest.model.Quest;
import org.mineacademy.lynx.settings.PlayerData;

@AutoRegister
public final class QuestCommand extends SimpleCommand {

	public QuestCommand() {
		super("quest");

		setMinArguments(1);
	}

	@Override
	protected String[] getMultilineUsageMessage() {
		return new String[]{
				"/{label} menu - Display quests menu.",
				"/{label} start <player> <quest> - Start quest for a player.",
				"/{label} complete [player] - Complete quest for player.",
				"/{label} cancel [player] - Cancel quest for a player.",
				"/{label} stats [player] - List completed quests for player.",
		};
	}

	enum Param {
		MENU, START, COMPLETE, CANCEL, STATS
	}

	@Override
	protected void onCommand() {

		Param param = findEnum(Param.class, args[0], String.join("\n", getMultilineUsageMessage()));

		if (param == Param.MENU) {
			checkConsole();

			QuestsMenu.showTo(getPlayer());

		} else {
			Quest quest = null;
			Player target = args.length == 2 ? findPlayer(args[1]) : isPlayer() ? getPlayer() : null;

			if (param == Param.START) {
				checkBoolean(args.length > 2, "Usage: /{label} {sublabel} <player> <quest>");

				// /quest start yourname "Armored Delivery" < we get everything after the second argument
				String questName = joinArgs(2);
				quest = Quest.getByName(questName);
				checkNotNull(quest, "No such quest '" + questName + "'. Available: " + Common.join(Quest.getQuestsNames()));
			}

			checkNotNull(target, "When running from console, specify target name.");

			PlayerData targetCache = PlayerData.from(target);
			Quest activeQuest = targetCache.getActiveQuest();

			if (param == Param.START) {
				checkBoolean(activeQuest == null,
						target.getName() + " already has a pending quest '" + (activeQuest == null /*is never null but java can't figure this out*/ ? ""
								: activeQuest.getName()) + "'.");

				new QuestStartPrompt(quest).show(target);
				//targetCache.setActiveQuest(quest);
				//tellSuccess("Starting '" + quest.getName() + "' quest for " + target.getName());
			}

			if (param == Param.STATS) {
				List<String> lines = new ArrayList<>();

				for (Map.Entry<String, Long> entry : targetCache.getCompletedQuests().entrySet())
					lines.add(" &8- &6" + entry.getKey() + " &7completed on &6" + TimeUtil.getFormattedDate(entry.getValue()));

				checkBoolean(!lines.isEmpty(), "You have no completed quests.");

				new ChatPaginator()
						.setHeader(
								"&8" + Common.chatLineSmooth(),
								"<center>&6Completed Quests",
								"&8" + Common.chatLineSmooth())
						.setPages(Common.toArray(lines))
						.send(sender);

				return;
			}

			if (param != Param.START)
				checkNotNull(activeQuest, target.getName() + " has no pending quest.");

			if (param == Param.COMPLETE) {
				tellSuccess("Completing '" + activeQuest.getName() + "' quest for " + target.getName());

				targetCache.completeQuest();
			} else if (param == Param.CANCEL) {
				tellSuccess("Cancelling '" + activeQuest.getName() + "' quest for " + target.getName());

				targetCache.cancelQuest();
			}
		}
	}

	@Override
	protected List<String> tabComplete() {

		if (args.length == 1)
			return completeLastWord(Param.values());

		else if ((args.length == 2 || args.length == 3) && isPlayer()) {
			Param param = ReflectionUtil.lookupEnumSilent(Param.class, args[0].toUpperCase());

			if (param != null)
				return param == Param.START && args.length == 3 ? completeLastWord(Quest.getQuestsNames()) : completeLastWordPlayerNames();
		}

		return NO_COMPLETE;
	}
}