package org.mineacademy.lynx.command;

import org.bukkit.entity.EntityType;
import org.mineacademy.fo.annotation.AutoRegister;
import org.mineacademy.fo.command.SimpleCommand;
import org.mineacademy.fo.menu.model.ItemCreator;
import org.mineacademy.fo.remain.CompColor;
import org.mineacademy.fo.remain.CompEnchantment;
import org.mineacademy.fo.remain.CompMaterial;
import org.mineacademy.fo.remain.CompPotionEffectType;

/**
 * An example command giving you custom items and skulls.
 */
@AutoRegister
public final class CustomItemsCommand extends SimpleCommand {

	public CustomItemsCommand() {
		super("customitems");
	}

	// See SpawnEntityCommand for help and comments.
	@Override
	protected void onCommand() {
		checkConsole();

		/*ItemStack sunFlower = new ItemStack(CompMaterial.SUNFLOWER.getMaterial());
		ItemMeta meta = sunFlower.getItemMeta();

		meta.setDisplayName(ChatColor.translateAlternateColorCodes('&', "&cDeadly Sunflower"));
		meta.setLore(Arrays.asList(
				"",
				"Click an entity",
				"to deal massive damage."));

		meta.addItemFlags(ItemFlag.HIDE_ENCHANTS);

		sunFlower.setItemMeta(meta);
		sunFlower.addUnsafeEnchantment(Enchantment.DAMAGE_ALL, 10);

		getPlayer().getInventory().addItem(sunFlower);*/

		ItemCreator.of(CompMaterial.SUNFLOWER,
				"&cDeadly Sunflower",
				"",
				"&d&oClick an entity to",
				"&d&odeal massive damage.")
				.hideTags(true)
				.enchant(CompEnchantment.DAMAGE_ALL, 10)
				.give(getPlayer());

		ItemCreator.of(CompMaterial.LEATHER_CHESTPLATE,
				"Lynxplate",
				"",
				"Wear this to be a",
				"part of the blue team.")
				.color(CompColor.BLUE)
				.give(getPlayer());

		ItemCreator.ofPotion(CompPotionEffectType.JUMP,
				5 * 60 * 20, 5,
				"Kangaroo",
				"",
				"Drink this to",
				"jump into infinite",
				"heights (careful!)")
				.give(getPlayer());

		ItemCreator.of(
				CompMaterial.PLAYER_HEAD,
				"Creator of Minecraft")
				.skullUrl("http://textures.minecraft.net/texture/733b6c907f1c2a1ae54f90aafbc9e561f2f4dd4ec4b73e56d54955bc1dfcc2a0")
				//.skullOwner("kangarko")
				.give(getPlayer());

		ItemCreator.ofEgg(
				EntityType.ENDERMAN,
				"Spawn Dragon",
				"",
				"Click any ground block",
				"to summon a dragon!")
				.tag("CustomBoss", EntityType.ENDER_DRAGON.toString()) // See PlayerListener for sample use
				.give(getPlayer());
	}
}
