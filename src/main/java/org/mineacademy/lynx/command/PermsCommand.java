package org.mineacademy.lynx.command;

import org.bukkit.permissions.PermissionAttachment;
import org.mineacademy.fo.annotation.AutoRegister;
import org.mineacademy.fo.command.SimpleCommand;
import org.mineacademy.lynx.Lynx;
import org.mineacademy.lynx.PermissionManager;

/**
 * An example of creating a command that gives/removes permissions from players.
 */
@AutoRegister
public final class PermsCommand extends SimpleCommand {

	public PermsCommand() {
		super("permtest");

		setDescription("Test your own permission syste");
		setUsage("<add/remove/check>");
		setMinArguments(1);
	}

	// See SpawnEntityCommand for help and comments.
	@Override
	protected void onCommand() {
		checkConsole();

		String param = args[0].toLowerCase();

		if ("add".equals(param)) {
			checkBoolean(!PermissionManager.hasPermissionSet(getPlayer()), "You have already set your permission");
			PermissionAttachment attachment = getPlayer().addAttachment(Lynx.getInstance(), "lynx.command.fire", true);

			PermissionManager.store(getPlayer(), attachment);
			tellSuccess("Your permission was granted!");

		} else if ("remove".equals(param)) {
			PermissionAttachment attachment = PermissionManager.getPermission(getPlayer());
			checkNotNull(attachment, "You do not have set your permission");

			getPlayer().removeAttachment(attachment);
			PermissionManager.remove(getPlayer());

			tellSuccess("Your permission was removed!");

		} else if ("check".equals(param)) {
			tellInfo(PermissionManager.hasPermissionSet(getPlayer()) ? "You have a permission set" : "You do not have a permission set");

		} else
			returnInvalidArgs();
	}
}
