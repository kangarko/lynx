package org.mineacademy.lynx.command;

import java.util.List;

import org.bukkit.entity.EntityType;
import org.mineacademy.fo.Common;
import org.mineacademy.fo.annotation.AutoRegister;
import org.mineacademy.fo.command.SimpleCommand;
import org.mineacademy.lynx.boss.Boss;
import org.mineacademy.lynx.menu.BossMenu;

@AutoRegister
public final class BossCommand extends SimpleCommand {

	public BossCommand() {
		super("boss");

		setMinArguments(1);
	}

	@Override
	protected String[] getMultilineUsageMessage() {
		return new String[]{
				"/{label} spawn <bossName> - Spawn the Boss at your location.",
				"/{label} new <bossName> <entityType> - Create a new Boss.",
				"/{label} menu - Open main Boss menu."
		};
	}

	@Override
	protected void onCommand() {
		this.checkConsole();

		String param = args[0];

		if ("spawn".equals(param)) {
			checkArgs(2, "Please also specify the name of the Boss.");

			Boss boss = Boss.findBoss(args[1]);
			checkNotNull(boss, "Could not find boss '{0}'. Available: " + Common.join(Boss.getBossesNames()));

			boss.spawn(getPlayer().getLocation());

		} else if ("new".equals(param) || "create".equals(param)) {
			checkArgs(3, "Usage: /{label} {sublabel} <bossName> <entityType>");

			String bossName = args[1];
			EntityType entityType = findEnum(EntityType.class, args[2], "Invalid entity type {2}! Available: " + Boss.getValidEntities());
			checkBoolean(Boss.getValidEntities().contains(entityType), "The selected boss entity is not valid!");

			Boss.createBoss(bossName, entityType);
			tellSuccess("Boss {1} has been successfully created!");

		} else if ("menu".equals(param)) {
			new BossMenu().displayTo(getPlayer());
		} else
			returnInvalidArgs();
	}

	@Override
	protected List<String> tabComplete() {
		return NO_COMPLETE;
	}
}
