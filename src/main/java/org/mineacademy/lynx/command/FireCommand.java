package org.mineacademy.lynx.command;

import org.bukkit.entity.Player;
import org.mineacademy.fo.annotation.AutoRegister;
import org.mineacademy.fo.command.SimpleCommand;

/**
 * An example of creating a command that sets players on fire depending on their permissions.
 */
@AutoRegister
public final class FireCommand extends SimpleCommand {

	public FireCommand() {
		super("fire/setfire");

		setDescription("Set a player on fire");
		setMinArguments(1);
	}

	// See SpawnEntityCommand for help and comments.
	@Override
	protected void onCommand() {
		Player target = findPlayer(args[0]);
		checkBoolean(!hasPerm(target, "lynx.bypass.fire"), "You cannot set that player on fire!");

		target.setFireTicks(20 * 2);
		tellSuccess("{0} has been set on fire");

		target.setFireTicks(20 * 2);
	}
}
