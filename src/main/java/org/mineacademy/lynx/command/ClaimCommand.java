package org.mineacademy.lynx.command;

import java.util.List;

import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.mineacademy.fo.Common;
import org.mineacademy.fo.ReflectionUtil;
import org.mineacademy.fo.annotation.AutoRegister;
import org.mineacademy.fo.command.SimpleCommand;
import org.mineacademy.lynx.claim.Claim;
import org.mineacademy.lynx.settings.PlayerData;

@AutoRegister
public final class ClaimCommand extends SimpleCommand {

	public ClaimCommand() {
		super("claim");

		setMinArguments(1);
	}

	@Override
	protected String[] getMultilineUsageMessage() {
		return new String[]{
				"/{label} find [playerName] - Find claim at player's location.",
				"/{label} tp <name> [playerName] - Teleport to claim center.",
				"/{label} create <name> - Create a new claim.",
				"/{label} remove <name> - Remove an existing claim.",
				"/{label} getowner <name> - Get owner of a claim",
				"/{label} setowner <name> [playerName] - Change owner of a claim.",

		};
	}

	enum Param {
		FIND,
		TP,
		CREATE,
		REMOVE,
		GETOWNER,
		SETOWNER
	}

	@Override
	protected void onCommand() {

		Param param = findEnum(Param.class, args[0], "Invalid param '{0}', available: " + Common.join(Param.values(), p -> p.toString().toLowerCase()));

		if (param == Param.FIND) {
			checkArgs(args.length <= 2);

			Player target = args.length == 2 ? findPlayer(args[1]) : isPlayer() ? getPlayer() : null;
			checkNotNull(target, "When running from the console, specify player name.");

			Claim claim = Claim.findClaim(target.getLocation());
			checkNotNull(claim, "No claim at " + target.getName() + "'s location.");

			tellSuccess("Found claim: " + claim.getName());
			return;
		}

		checkBoolean(args.length == 2 || args.length == 3, "Please specify claim name.");
		Claim claim = Claim.findClaim(args[1]);

		if (param == Param.CREATE) {
			checkConsole();
			checkBoolean(claim == null, "Claim '{1}' already exists!");

			PlayerData cache = PlayerData.from(getPlayer());
			checkBoolean(cache.getRegion().isWhole(), "Please set both region points using /tools first!");

			List<Block> blocks = cache.getRegion().getBlocks();
			checkBoolean(blocks.size() < 25_000 || getPlayer().hasPermission("lynx.claim.vip"),
					"Your claim must be smaller than 25k blocks (current: " + (blocks.size() / 1000) + "k)");

			for (Block block : blocks)
				checkBoolean(Claim.findClaim(block.getLocation()) == null, "There is a conflicting claim at " + Common.shortLocation(block.getLocation()) + "!");

			Claim.createClaim(args[1], getPlayer(), cache.getRegion());
			tellSuccess("Claim {1} has been created and protected!");
			return;
		}

		checkNotNull(claim, "No such claim '{1}'. Available: " + Common.join(Claim.getClaimsNames()));

		Player target = args.length == 3 ? findPlayer(args[2]) : isPlayer() ? getPlayer() : null;

		if (param == Param.SETOWNER || param == Param.TP)
			checkNotNull(target, "When running from the console, specify player name.");

		if (param == Param.TP) {
			target.teleport(claim.getCenter());

			tellInfo("Teleporting " + target.getName() + " to claim {1}.");

		} else if (param == Param.REMOVE) {
			Claim.removeClaim(claim);

			tellWarn("Claim {1} has been removed!");
		} else if (param == Param.GETOWNER)
			tellInfo("Owner of claim '{1}' is " + claim.getOwnerName());

		else if (param == Param.SETOWNER) {
			claim.setOwner(target);

			tellInfo("Set owner of claim {1} to " + target.getName() + ".");
		}
	}

	@Override
	protected List<String> tabComplete() {

		if (args.length == 1)
			return completeLastWord(Param.values());

		if (args.length == 2) {
			Param param = ReflectionUtil.lookupEnumSilent(Param.class, args[0].toUpperCase());

			return param != Param.FIND ? completeLastWord(Claim.getClaimsNames()) : completeLastWordPlayerNames();
		}

		// TODO for 3 args length

		return NO_COMPLETE;
	}
}
