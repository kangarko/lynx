package org.mineacademy.lynx.command;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.bukkit.Location;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.mineacademy.fo.Common;
import org.mineacademy.fo.annotation.AutoRegister;
import org.mineacademy.fo.command.SimpleCommand;

/**
 * An example of creating a custom command, don't forget to place \@AutoRegister over
 * it or manually register it in onPluginStart() method in your main plugin's class.
 */
@AutoRegister
public final class SpawnEntityCommand extends SimpleCommand {

	/**
	 * Constructor to create a new object from this command that will be used each time
	 * we call the command by its name, or shortcuts (called aliases) set in super().
	 *
	 * This command can be run if I type /spawnentity, /spawnmob or /spawne in the game chat.
	 */
	public SpawnEntityCommand() {
		super("spawnentity|spawnmob|spawne");

		// You can specify different command properties here, they will be shown in the game
		setDescription("Spawn a mob at the given location.");

		// You can set a multi-line usage below in a separate method
		//setUsage("One line usage");

		// The command will automatically display "invalid usage" message when it has less than two
		// arguments. Everything after /spawnentity separated by a space is an argument, example:
		// "/spawnentity zombie kangarko" then zombie is argument[0] and kangarko is argument[2], together 2 arguments
		setMinArguments(2);

		// We set your permission automatically in the "yourPlugin.command.label" format, uncomment to set a custom one
		//setPermission("lynxnative.command.spawnentity");

	}

	/**
	 * Show a usage message to a sender of how this command can be used.
	 *
	 * TIP: You can use "sender" or permissions checks to only show the parts of the command
	 * for which the sender has permissions.
	 */
	@Override
	protected String[] getMultilineUsageMessage() {
		final List<String> usage = new ArrayList<>();

		if (hasPerm("lynx.command.spawnentity.player"))
			usage.add("/spawnentity <mobType> <playerName> - Spawn a mob at the player's location.");

		if (hasPerm("lynx.command.spawnentity.location"))
			usage.add("/spawnentity <mobType> <x> <y> <z> - Spawns a mob at the given location.");

		return Common.toArray(usage);
	}

	/**
	 * Code below is called automatically when the given sender runs this command from what you
	 * specified in the super() in constructor above such as /spawnentity and its aliases.
	 *
	 * You can use "sender" and "args" array here. The args[] is the message after /spawnentity separated
	 * by spaces, such as: /spawnentity creeper kangarko means args[0] is "creeper" and args[1] is "kangarko"
	 * together they have a length of 2 since they are two arguments.
	 */
	@Override
	protected void onCommand() {

		// Automatically prevent this command from running in the console and send error message (customizable in the
		// settings video) to the sender
		checkConsole();

		// Automatically cast the sender to player since we have blocked non-player access above
		final Player player = getPlayer();

		// Automatically find the mob type from the first argument using the EntityType class, if player types
		// something invalid then the No such entity by name message will be sent and command is returned.
		final EntityType entityType = findEnum(EntityType.class, args[0], "No such entity by name '{enum}'.");

		// Automatically check that the mob is valid, if not then send the error message and return command
		checkBoolean(entityType.isAlive() && entityType.isSpawnable(), "Entity " + entityType.getName() + " is not alive or spawnable!");

		// If 4 arguments are provided we assume location three points, x, y and z: /spawnentity <mobType> <x> <y> <z>
		if (args.length == 4) {

			// Automatically parse numbers from arguments, send and error message and return command if they are invalid
			final int x = findNumber(1, "The X position must be a whole number! You typed: '{1}'.");
			final int y = findNumber(2, "The Y position must be a whole number! You typed: '{2}'.");
			final int z = findNumber(3, "The Z position must be a whole number! You typed: '{3}'.");

			player.getWorld().spawnEntity(new Location(player.getWorld(), x, y, z), entityType);

		}

		// If two arguments are typed, assume mobType and the player to spawn the mob at: /spawnentity <mobType> <targetPlayer>
		else if (args.length == 2) {

			// Automatically find online player, return command and send an error message if not online (customizable, see settings video)
			final Player targetPlayer = findPlayer(args[1]);

			targetPlayer.getWorld().spawnEntity(targetPlayer.getLocation(), entityType);
		}
	}

	/**
	 * Called automatically when typing command and pressing TAB to help you complete its arguments.
	 *
	 * Returns a list of suggestions. Return "null" for all player names. Return empty list or NO_COMPLETE
	 * for no suggestions.
	 *
	 * WE WILL AUTOMATICALLY ONLY RETURN SUGGESTIONS THAT YOUR RELEVANT ARGUMENT STARTS WITH
	 */
	@Override
	protected List<String> tabComplete() {

		// Return empty list when sender is console
		if (!isPlayer())
			return new ArrayList<>();

		final Player player = (Player) sender;

		switch (args.length) {

			// If we are completing the first argument, in our case mob type, proceed
			case 1:

				// Use Java 8 streams API to pick valid mobs from EntityType and collect to a list
				// WE WILL AUTOMATICALLY ONLY RETURN SUGGESTIONS THAT YOUR RELEVANT ARGUMENT STARTS WITH
				// such as typing "skel" will suggest "skeleton" but not "zombie" using the completeLastWord method.
				return completeLastWord(Arrays
						.stream(EntityType.values())
						.filter(type -> type.isSpawnable() && type.isAlive())
						.collect(Collectors.toList()));

			case 2:
				// Suggest online player names or block X position
				return completeLastWord(Common.getPlayerNames(), player.getLocation().getBlockX());

			case 3:
				return completeLastWord(player.getLocation().getBlockY());

			case 4:
				return completeLastWord(player.getLocation().getBlockZ());
		}

		return NO_COMPLETE;
	}
}
