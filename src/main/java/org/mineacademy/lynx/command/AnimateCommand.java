package org.mineacademy.lynx.command;

import java.util.List;

import org.bukkit.entity.Player;
import org.mineacademy.fo.Common;
import org.mineacademy.fo.MathUtil;
import org.mineacademy.fo.PlayerUtil;
import org.mineacademy.fo.annotation.AutoRegister;
import org.mineacademy.fo.command.SimpleCommand;
import org.mineacademy.fo.model.Replacer;
import org.mineacademy.fo.model.SimpleScoreboard;
import org.mineacademy.fo.remain.CompBarColor;
import org.mineacademy.fo.remain.CompBarStyle;
import org.mineacademy.fo.remain.CompMaterial;
import org.mineacademy.fo.remain.CompToastStyle;
import org.mineacademy.fo.remain.Remain;
import org.mineacademy.fo.slider.ColoredTextSlider;

import lombok.NonNull;

/**
 * An example of creating a command that sends animated notifications to players.
 */
@AutoRegister
public final class AnimateCommand extends SimpleCommand {

	public AnimateCommand() {
		super("animate");

		setUsage("<action/title/tab/boss/toast/scoreboard>");
		setMinArguments(1);
	}

	// See SpawnEntityCommand for help and comments.
	@Override
	protected void onCommand() {
		checkConsole();

		Player player = getPlayer();
		String type = args[0].toLowerCase();

		if ("action".equals(type)) {
			Remain.sendActionBar(player, "&7[ &dWelcome to the server! &7]");

			Common.runLater(2 * 20, () -> Remain.sendActionBar(player, "&7[ &fType /help for more info! &7]"));

		} else if ("title".equals(type))
			Remain.sendTitle(player, 10, 2 * 20, 0, "&6Welcome", "&7Visit our website mineacademy.org!");

		else if ("tab".equals(type))
			Remain.sendTablist(player, "\n&6[MineAcademy]\nOnline players: &c" + Remain.getOnlinePlayers().size() + "\n", "\n&7Receive reward for \n&7voting at mineacademy.org/vote");

		else if ("boss".equals(type))
			Remain.sendBossbarTimed(player, Common.colorize("&cThis is a special announcement!"), 6, CompBarColor.RED, CompBarStyle.SEGMENTED_6);

		else if ("toast".equals(type))
			Remain.sendToast(player, "&6Hey " + player.getName() + ",\nYou've got a new mail!", CompMaterial.SUNFLOWER, CompToastStyle.GOAL);

		else if ("scoreboard".equals(type)) {

			ColoredTextSlider slider = ColoredTextSlider
					.from("Information Board")
					.primaryColor("&6")
					.secondaryColor("&c")
					.width(4);

			SimpleScoreboard board = new SimpleScoreboard() {

				@Override
				protected String replaceVariables(@NonNull Player player, @NonNull String message) {
					return Replacer.replaceArray(message,
							"online", Remain.getOnlinePlayers().size(),
							"health", MathUtil.formatTwoDigits(Remain.getHealth(player)),
							"ping", PlayerUtil.getPing(player));
				}

				@Override
				protected void onUpdate() {
					this.setTitle(slider.next());
				}
			};

			board.setUpdateDelayTicks(1);

			board.addRows("",
					"&cOnline player:",
					"&7{online}",
					"",
					"&cHealth:",
					"&7{health}",
					"",
					"&cPing:",
					"&7{ping}");

			board.show(player);
		}

	}

	@Override
	protected List<String> tabComplete() {
		return completeLastWord("action", "title", "tab", "boss", "toast", "scoreboard");
	}
}
