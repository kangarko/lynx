package org.mineacademy.lynx.command;

import org.mineacademy.fo.annotation.AutoRegister;
import org.mineacademy.fo.command.SimpleCommand;
import org.mineacademy.lynx.settings.PlayerData;

/**
 * An example of creating a command that sets players on fire depending on their permissions.
 */
@AutoRegister
public final class SetTabCommand extends SimpleCommand {

	public SetTabCommand() {
		super("settab");

		setUsage("<tab>");
		setMinArguments(1);
	}

	// See SpawnEntityCommand for help and comments.
	@Override
	protected void onCommand() {
		checkConsole();
		String tabName = args[0];

		//Debugger.printValues(args);

		getPlayer().setPlayerListName(tabName);
		PlayerData.from(getPlayer()).setTabListName(tabName);

		tellSuccess("Your name has been updated to '" + tabName + "'.");
	}
}
