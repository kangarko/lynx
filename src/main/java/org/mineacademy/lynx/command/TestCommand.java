package org.mineacademy.lynx.command;

import java.util.Arrays;

import org.bukkit.ChatColor;
import org.bukkit.entity.EntityType;
import org.bukkit.inventory.ItemStack;
import org.mineacademy.fo.Common;
import org.mineacademy.fo.PlayerUtil;
import org.mineacademy.fo.annotation.AutoRegister;
import org.mineacademy.fo.command.SimpleCommand;
import org.mineacademy.fo.menu.model.ItemCreator;
import org.mineacademy.fo.model.Tuple;
import org.mineacademy.fo.remain.CompEnchantment;
import org.mineacademy.fo.remain.CompItemFlag;
import org.mineacademy.fo.remain.CompMaterial;
import org.mineacademy.lynx.database.LynxAdvancedDatabase;
import org.mineacademy.lynx.settings.PlayerData;
import org.mineacademy.lynx.settings.PlayerData.Kit;

/**
 * An example of creating a command that sets players on fire depending on their permissions.
 */
@AutoRegister
public final class TestCommand extends SimpleCommand {

	public TestCommand() {
		super("test");
	}

	@Override
	protected void onCommand() {
		PlayerData cache = PlayerData.from(getPlayer());

		cache.setHealth(17.5);
		cache.setTabListName("Herobrine");
		cache.setKit("Heroboots", Arrays.asList(ItemCreator.of(CompMaterial.DIAMOND_AXE,
				"Nice Axe", "", "Kill a cow").enchant(CompEnchantment.ARROW_DAMAGE, 1).make()));

		cache.setColor(ChatColor.RED);
		cache.setEggs(Arrays.asList(ItemCreator.ofEgg(EntityType.COW).name("&cSPAWN THIS COW").lore("", "Click this egg", "to spawn a cow.").glow(true).flags(CompItemFlag.HIDE_ATTRIBUTES).make()));
		cache.setEggChances(Arrays.asList(new Tuple<>(ItemCreator.ofEgg(EntityType.ZOMBIE).name("&8[&4 Spawn Zombie Warrior &8]").make(), 0.25)));

		LynxAdvancedDatabase database = LynxAdvancedDatabase.getInstance();

		database.saveCache(getPlayer(), savedCache -> {
			tellInfo("Database saved. Loading back...");

			database.loadCache(getPlayer(), loadedCache -> {
				Kit kit = cache.getKit();

				tellSuccess("Database loaded back. Health: " + loadedCache.getHealth() + ", tab list name: "
						+ loadedCache.getTabListName() + ", kit: " + (kit == null ? "none" : kit.getName()));

				if (kit != null && kit.getItems() != null)
					getPlayer().getInventory().addItem(kit.getItems().toArray(new ItemStack[kit.getItems().size()]));

				if (cache.getEggs() != null)
					getPlayer().getInventory().addItem(cache.getEggs().toArray(new ItemStack[cache.getEggs().size()]));

				if (cache.getEggChances() != null)
					PlayerUtil.addItems(getPlayer().getInventory(), Common.convert(cache.getEggChances(), Tuple::getKey));
			});
		});
	}
}
