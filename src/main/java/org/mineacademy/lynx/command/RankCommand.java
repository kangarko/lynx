package org.mineacademy.lynx.command;

import java.util.List;

import org.bukkit.entity.Player;
import org.mineacademy.fo.Common;
import org.mineacademy.fo.Messenger;
import org.mineacademy.fo.annotation.AutoRegister;
import org.mineacademy.fo.command.SimpleCommand;
import org.mineacademy.lynx.settings.PlayerData;

@AutoRegister
public final class RankCommand extends SimpleCommand {

	public RankCommand() {
		super("rank");

		setUsage("[upgrade/playerName]");
	}

	// See SpawnEntityCommand for help and comments.
	@Override
	protected void onCommand() {
		checkConsole();

		Player player = getPlayer();
		PlayerData cache = PlayerData.from(player);

		if (args.length == 0)
			returnTell("Your rank: " + cache.getRank().getFormattedName());

		if (args.length == 1) {
			String param = args[0];

			if ("upgrade".equals(param)) {
				checkNotNull(cache.getRank().getNext(), "&6You have reached the maximum rank, congrats!");

				if (!cache.getRank().upgrade(player))
					returnTell("You do not qualify for a rank upgrade to " + cache.getRank().getNext().getName());
			} else {
				PlayerData.poll(args[0],
						otherCache -> {
							if (otherCache == null) {
								Messenger.error(player, "Unable to find player: " + args[0] + ".");

								return;
							}

							tell(otherCache.getPlayerName() + "'s rank: " + otherCache.getRank().getFormattedName());
						});
			}
		}
	}

	@Override
	protected List<String> tabComplete() {
		return args.length == 1 ? completeLastWord("upgrade", Common.getPlayerNames()) : NO_COMPLETE;
	}
}
