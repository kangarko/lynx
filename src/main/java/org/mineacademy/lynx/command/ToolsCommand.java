package org.mineacademy.lynx.command;

import org.mineacademy.fo.annotation.AutoRegister;
import org.mineacademy.fo.command.SimpleCommand;
import org.mineacademy.fo.menu.MenuTools;
import org.mineacademy.lynx.item.KittyCannon;
import org.mineacademy.lynx.item.RegionTool;
import org.mineacademy.lynx.item.VillagerServerSelectorTool;

/**
 * An example of creating a command that creates a menu of tools
 */
@AutoRegister
public final class ToolsCommand extends SimpleCommand {

	public ToolsCommand() {
		super("tools");
	}

	// See SpawnEntityCommand for help and comments.
	@Override
	protected void onCommand() {
		checkConsole();

		// Create anonymous class from the MenuTools allowing easy menu creation
		// to give players certain items with one click
		new MenuTools() {

			@Override
			protected Object[] compileTools() {
				// Return Tools, Tool classes or ItemStacks here
				return new Object[] {
						KittyCannon.class, RegionTool.class, VillagerServerSelectorTool.class
				};
			}

			// Displays the menu to the player
		}.displayTo(getPlayer());
	}
}
