package org.mineacademy.lynx.command;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.Statistic;
import org.bukkit.entity.EntityType;
import org.mineacademy.fo.Common;
import org.mineacademy.fo.ItemUtil;
import org.mineacademy.fo.MathUtil;
import org.mineacademy.fo.PlayerUtil;
import org.mineacademy.fo.TimeUtil;
import org.mineacademy.fo.annotation.AutoRegister;
import org.mineacademy.fo.command.SimpleCommand;
import org.mineacademy.fo.exception.FoException;
import org.mineacademy.fo.model.ChatPaginator;
import org.mineacademy.fo.model.SimpleComponent;
import org.mineacademy.fo.model.Tuple;
import org.mineacademy.fo.remain.Remain;
import org.mineacademy.lynx.settings.PlayerData;

@AutoRegister
public final class StatsCommand extends SimpleCommand {

	public StatsCommand() {
		super("stats");

		setMinArguments(1);
		setUsage("<" + Common.join(Param.values(), "/") + "> [player]");
	}

	@Override
	protected void onCommand() {

		final Param param = findEnum(Param.class, args[0], "Usage: /{label} <" + Common.join(Param.values(), "/") + "> [player]");

		// Get statistic about a specific player
		if (args.length == 2) {

			// /stats playtime kangarko
			findOfflinePlayer(args[1], otherPlayer -> {
				PlayerData cache = PlayerData.fromPlayerFile(otherPlayer.getUniqueId());

				switch (param) {
					case DEATHS:
						tell(otherPlayer.getName() + " died " + Common.plural(cache.getDeaths(), "time") + ".");
						break;

					case PLAYER_KILLS:
						tell(otherPlayer.getName() + " killed " + Common.plural(cache.getPlayerKills(), "player") + ".");
						break;

					case KTD:
						tell(otherPlayer.getName() + " kill-to-death ratio is "
								+ MathUtil.formatOneDigit((double) cache.getDeaths() / (double) cache.getPlayerKills()) + ".");
						break;

					case PLAYTIME:
						long playtimeTicks = Remain.getPlaytimeMinutes(otherPlayer);

						tell(otherPlayer.getName() + " played for " + TimeUtil.formatTimeShort(playtimeTicks * 60).replace(" 0s", "") + ".");
						break;

					case ZOMBIE_KILLS:
						long zombieKills = PlayerUtil.getStatistic(otherPlayer, Statistic.KILL_ENTITY, EntityType.ZOMBIE);

						tell(otherPlayer.getName() + " killed " + Common.plural(zombieKills, "zombie") + ".");
						break;

					default:
						throw new FoException("Unimplemented statistic for " + param);
				}
			});

			return;
		}

		PlayerData.pollAll(data -> {

			List<Tuple<String, Double>> stats = new ArrayList<>();

			Common.runAsync(() -> {
				for (PlayerData cache : data) {

					final OfflinePlayer offlinePlayer = Remain.getOfflinePlayerByUUID(cache.getUuid());
					double statisticsAmount;

					switch (param) {
						case DEATHS:
							statisticsAmount = cache.getDeaths();
							break;

						case PLAYER_KILLS:
							statisticsAmount = cache.getPlayerKills();
							break;

						case KTD:
							statisticsAmount = cache.getDeaths() == 0 || cache.getPlayerKills() == 0 ? 0 : (double) cache.getDeaths() / (double) cache.getPlayerKills();
							break;

						case PLAYTIME:
							statisticsAmount = Remain.getPlaytimeMinutes(offlinePlayer) * 60;
							break;

						case ZOMBIE_KILLS:
							statisticsAmount = PlayerUtil.getStatistic(offlinePlayer, Statistic.KILL_ENTITY, EntityType.ZOMBIE);
							break;

						default:
							throw new FoException("Unimplemented statistic for " + param);
					}

					stats.add(new Tuple<>(cache.getPlayerName(), statisticsAmount));
				}

				stats.sort((first, second) -> second.getValue().compareTo(first.getValue()));

				int position = 1;
				List<SimpleComponent> pages = new ArrayList<>();

				for (Tuple<String, Double> entry : stats) {
					String playerName = entry.getKey();
					double statisticValue = entry.getValue();
					String row = param == Param.KTD ? MathUtil.formatOneDigit(statisticValue) :
							param == Param.PLAYTIME ? TimeUtil.formatTimeShort((long) statisticValue) :
									String.valueOf(Math.round(statisticValue));

					pages.add(SimpleComponent.of("&f#" + position++ + " &7- &f" + playerName + " &7(" + row + ")").onHover("Hover me!"));
				}

				new ChatPaginator(10, ChatColor.DARK_GRAY)
						.setHeader(
								"&8" + Common.chatLineSmooth(),
								"<center>&6Top Player " + ItemUtil.bountifyCapitalized(param),
								"&8" + Common.chatLineSmooth())
						.setPages(pages)
						.send(sender);
			});

		});
	}

	enum Param {
		PLAYTIME,
		PLAYER_KILLS,
		ZOMBIE_KILLS,
		DEATHS,
		KTD
	}

	@Override
	protected List<String> tabComplete() {

		if (args.length == 1)
			return completeLastWord(Param.values());

		if (args.length == 2)
			return completeLastWordPlayerNames();

		return NO_COMPLETE;
	}
}