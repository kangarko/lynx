package org.mineacademy.lynx.command.channel;

import org.mineacademy.fo.command.SimpleCommandGroup;
import org.mineacademy.fo.command.SimpleSubCommand;
import org.mineacademy.lynx.ChannelManager;

/**
 * A subcommand registered in the {@link ChannelCommandGroup} class.
 */
public final class ChannelLeaveCommand extends SimpleSubCommand {

	protected ChannelLeaveCommand(SimpleCommandGroup parent) {
		super(parent, "leave|l");

		setMinArguments(1);
	}

	// See ChannelJoinCommand
	@Override
	protected void onCommand() {
		checkConsole();
		final String name = args[0];
		checkBoolean(ChannelManager.isJoined(getPlayer(), name), "You are not joined in {0} channel.");

		ChannelManager.leave(getPlayer());
		tellWarn("You have left {0} channel.");
	}
}
