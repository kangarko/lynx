package org.mineacademy.lynx.command.channel;

import org.mineacademy.fo.annotation.AutoRegister;
import org.mineacademy.fo.command.ReloadCommand;
import org.mineacademy.fo.command.SimpleCommandGroup;

/**
 * A command group has one main command label such as /channel and allows you to create subcommands
 * easily such as /channel join and /channel leave without all being in one class.
 * <p>
 * Foundation automatically creates /channel ? pagination and displays help, checks for permissions etc.
 */
// Registered automatically, or you can register it manually in onPluginStart() or onReloadablesStart() method
// in your main plugin's class.
@AutoRegister
public final class ChannelCommandGroup extends SimpleCommandGroup {

	/**
	 * Create a new object from which all commands starting with /channel or /ch will be handled.
	 * <p>
	 * I recommend seeing the training video where I talk about "main command label" and Command_Aliases key
	 * from settings.yml that is used if you do not put this constructor here.
	 */
	public ChannelCommandGroup() {
		super("channel/ch");
	}

	/**
	 * You must register all of your subcommands here.
	 */
	@Override
	protected void registerSubcommands() {
		registerSubcommand(new ChannelJoinCommand(this));
		registerSubcommand(new ChannelLeaveCommand(this));
		registerSubcommand(new ReloadCommand());
	}

	/**
	 * If you just type /channel or /ch without any subcommands the you can completely
	 * override what is printed here. SimpleComponent class makes it easy to create clickable/hoverable components.
	 */
	/*@Override
	protected List<SimpleComponent> getNoParamsHeader() {
		return Arrays.asList(
				SimpleComponent.of("&cWelcome to the channel command. Type /channel ? for more info."),
				SimpleComponent.of("&cPlugin made by YOURNAME"));
	}*/

	/**
	 * IF YOU DO NOT override getNoParamsHeader, you can customize the credits line here.
	 */
	/*@Override
	protected String getCredits() {
		return "Visit my awesome server at myserver.com";
	}*/

	/**
	 * If you type /channel ? or /ch ? then you can override the header of the help output.
	 */
	/*@Override
	protected String[] getHelpHeader() {
		return new String[]{
				"&6The help for your channel command:"
		};
	}*/
}
