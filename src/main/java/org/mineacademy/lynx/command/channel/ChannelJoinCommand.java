package org.mineacademy.lynx.command.channel;

import org.mineacademy.fo.command.SimpleCommandGroup;
import org.mineacademy.fo.command.SimpleSubCommand;
import org.mineacademy.lynx.ChannelManager;

/**
 * A subcommand registered in the {@link ChannelCommandGroup} class.
 */
public final class ChannelJoinCommand extends SimpleSubCommand {

	/**
	 * Create a new object whose code is run when player types /channel join, /channel j
	 * /ch join or /ch j
	 *
	 * @param parent
	 */
	protected ChannelJoinCommand(SimpleCommandGroup parent) {

		// Set the subcommand label and aliases in the super
		// TIP: You can also NOT SPECIFY the SimpleCommandGroup parent here if your command
		// group's label equals to the first in the Command_Aliases key list
		// in settings.yml then we assume this subcommand is owned by that "main" command group.
		super(parent, "join|j");

		setDescription("Join a channel");
		setUsage("<channel>");
		setMinArguments(1);
	}

	/**
	 * Run automatically when this subcommand is typed. See {@link SpawnEntityCommand} for more help and my comments.
	 */
	@Override
	protected void onCommand() {
		checkConsole();

		final String name = args[0];
		checkBoolean(!ChannelManager.isJoined(getPlayer(), name), "You are already joined in {0} channel.");

		ChannelManager.join(getPlayer(), name);
		tellSuccess("You have joined {0} channel.");
	}
}
