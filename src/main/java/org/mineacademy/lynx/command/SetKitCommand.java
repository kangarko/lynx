package org.mineacademy.lynx.command;

import java.util.Arrays;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.mineacademy.fo.annotation.AutoRegister;
import org.mineacademy.fo.command.SimpleCommand;
import org.mineacademy.lynx.settings.PlayerData;

/**
 * An example of creating a command that sets players on fire depending on their permissions.
 */
@AutoRegister
public final class SetKitCommand extends SimpleCommand {

	public SetKitCommand() {
		super("setkit");

	}

	// See SpawnEntityCommand for help and comments.
	@Override
	protected void onCommand() {
		checkConsole();

		PlayerData.from(getPlayer()).setKit("Warrior",
				Arrays.asList(new ItemStack(Material.ARROW), new ItemStack(Material.BOW)));

		tellSuccess("Kit given!");
	}
}
