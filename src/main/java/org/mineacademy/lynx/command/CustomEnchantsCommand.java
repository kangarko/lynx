package org.mineacademy.lynx.command;

import org.mineacademy.fo.annotation.AutoRegister;
import org.mineacademy.fo.command.SimpleCommand;
import org.mineacademy.fo.menu.model.ItemCreator;
import org.mineacademy.fo.remain.CompMaterial;
import org.mineacademy.lynx.enchant.BlackNovaEnchant;
import org.mineacademy.lynx.enchant.HideEnchant;

@AutoRegister
public final class CustomEnchantsCommand extends SimpleCommand {

	public CustomEnchantsCommand() {
		super("customenchants|ce");
	}

	@Override
	protected void onCommand() {
		checkConsole();

		ItemCreator.of(CompMaterial.BOW, "Custom Enchanted Bow")
				.enchant(HideEnchant.getInstance())
				.give(getPlayer());

		ItemCreator.of(CompMaterial.IRON_SWORD, "&6Custom Sword")
				.enchant(BlackNovaEnchant.getInstance())
				.give(getPlayer());

		//ItemStack itemStack = new ItemStack(Material.BOW);
		//itemStack.addEnchantment(HideEnchant.getInstance(), 1);
	}
}
