package org.mineacademy.lynx.boss;

import java.util.Collection;
import java.util.Set;
import java.util.TreeSet;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.inventory.ItemStack;
import org.mineacademy.fo.Common;
import org.mineacademy.fo.Valid;
import org.mineacademy.fo.collection.StrictList;
import org.mineacademy.fo.remain.CompAttribute;
import org.mineacademy.fo.remain.CompMetadata;
import org.mineacademy.fo.remain.Remain;
import org.mineacademy.fo.settings.ConfigItems;
import org.mineacademy.fo.settings.YamlConfig;

import lombok.Getter;
import lombok.NonNull;

@Getter
public class Boss extends YamlConfig {

	private static ConfigItems<Boss> loadedBosses = ConfigItems.fromFolder("bosses", Boss.class);

	private static final String NBT_TAG = "LynxBoss";

	/*private static final Boss WARRIOR = new Boss("Warrior", EntityType.ZOMBIE) {{
		setHealth(50.D);
		setDamageMultiplier(3.D);
	}};*/

	private EntityType type;

	private Double health;
	private Double damageMultiplier;
	private String rankToUpgradeFrom;
	private String questToCompleteWhenKilled;

	// Called when ConfigItems class loads your boss from your bosses/ folder
	private Boss(String name) {
		this(name, null);
	}

	// Called from the game when you create new bosses
	private Boss(String name, EntityType type) {
		this.type = type;

		this.loadConfiguration(NO_DEFAULT, "bosses/" + name + ".yml");
	}

	public void spawn(Location location) {
		final LivingEntity entity = (LivingEntity) location.getWorld().spawnEntity(location, this.type);

		CompMetadata.setMetadata(entity, NBT_TAG, this.getName());

		this.applyProperties(entity);
	}

	public void applyProperties(LivingEntity entity) {
		Remain.setCustomName(entity, this.getName());

		if (this.health != null) {
			CompAttribute.MAX_HEALTH.set(entity, this.health);
			entity.setHealth(this.health);
		}
	}

	@Override
	protected void onLoad() {

		// Type is not null when Boss is created through API
		if (this.type == null) {
			Valid.checkBoolean(isSet("Type"), "Corrupted Boss file: " + this.getFileName() + ", lacks the 'Type' key to determine EntityType for the boss.");

			this.type = get("Type", EntityType.class);
		} else {
			// Do nothing, it has been set when we created the new boss
		}

		this.health = getDouble("Health");
		this.damageMultiplier = getDouble("Damage_Multiplier");
		this.rankToUpgradeFrom = getString("Rank_To_Upgrade_From");
		this.questToCompleteWhenKilled = getString("Quest_To_Complete_When_Killed");
		// etc.

		this.save(); // save all values to file when you create a boss
	}

	@Override
	protected void onSave() {
		this.set("Type", this.type);
		this.set("Health", Common.getOrDefault(this.health, 20D));
		this.set("Damage_Multiplier", Common.getOrDefault(this.damageMultiplier, 1D));
		this.set("Rank_To_Upgrade_From", this.rankToUpgradeFrom);
		this.set("Quest_To_Complete_When_Killed", this.questToCompleteWhenKilled);
		// etc.

		// Iterate through all alive bosses on all worlds and reapply their properties
		//this.applyProperties();
	}

	public void setHealth(Double health) {
		this.health = health;

		this.save();
	}

	public void setDamageMultiplier(Double damageMultiplier) {
		this.damageMultiplier = damageMultiplier;

		this.save();
	}

	public void setRankToUpgradeFrom(String rankToUpgradeFrom) {
		this.rankToUpgradeFrom = rankToUpgradeFrom;

		this.save();
	}

	public void setQuestToCompleteWhenKilled(String questToCompleteWhenKilled) {
		this.questToCompleteWhenKilled = questToCompleteWhenKilled;

		this.save();
	}

	public void onBossAttack(LivingEntity target, EntityDamageByEntityEvent event) {
		if (this.damageMultiplier != null)
			event.setDamage(Remain.getFinalDamage(event) * this.damageMultiplier);
	}

	/* ------------------------------------------------------------------------------- */
	/* Static */
	/* ------------------------------------------------------------------------------- */

	/**
	 * Get valid entity types we can create Bosses from
	 *
	 * @return
	 */
	public static Set<EntityType> getValidEntities() {
		final Set<EntityType> types = new TreeSet<>(EntityType::compareTo);

		for (final EntityType entity : EntityType.values())
			if (entity.isSpawnable() && entity.isAlive())
				types.add(entity);

		return types;
	}

	/**
	 * @param name
	 * @param type
	 * @return
	 * @see ConfigItems#loadOrCreateItem(String)
	 */
	public static Boss createBoss(@NonNull final String name, @NonNull final EntityType type) {
		return loadedBosses.loadOrCreateItem(name, () -> new Boss(name, type));
	}

	/**
	 * @see ConfigItems#loadItems()
	 */
	public static void loadBosses() {
		loadedBosses.loadItems();
	}

	/**
	 * Remove boss by name
	 *
	 * @param bossName
	 */
	public static void removeBoss(String bossName) {
		final Boss boss = findBoss(bossName);
		Valid.checkNotNull(boss, "Cannot remove non existing Boss " + bossName);

		removeBoss(boss);
	}

	/**
	 * @param boss
	 * @see ConfigItems#removeItem(org.mineacademy.fo.settings.YamlConfig)
	 */
	public static void removeBoss(final Boss boss) {
		loadedBosses.removeItem(boss);
	}

	/**
	 * @param name
	 * @return
	 * @see ConfigItems#isItemLoaded(String)
	 */
	public static boolean isBossLoaded(final String name) {
		return loadedBosses.isItemLoaded(name);
	}

	/**
	 * Find alive bosses on all worlds
	 *
	 * @return
	 */
	public static StrictList<SpawnedBoss> findBossesAlive() {
		final StrictList<SpawnedBoss> found = new StrictList<>();

		for (final World world : Bukkit.getWorlds())
			found.addAll(findBossesAliveIn(world));

		return found;
	}

	/**
	 * Find alive bosses on in world
	 *
	 * @param world
	 * @return
	 */
	public static StrictList<SpawnedBoss> findBossesAliveIn(World world) {
		final StrictList<SpawnedBoss> found = new StrictList<>();

		for (final Entity entity : world.getLivingEntities()) {
			final SpawnedBoss boss = findBoss(entity);

			if (boss != null)
				found.add(boss);
		}

		return found;
	}

	/**
	 * Find alive bosses in range
	 *
	 * @param center
	 * @param radius
	 * @return
	 */
	public static StrictList<SpawnedBoss> findBossesInRange(Location center, double radius) {
		final StrictList<SpawnedBoss> found = new StrictList<>();

		for (final Entity entity : Remain.getNearbyEntities(center, radius)) {
			final SpawnedBoss boss = findBoss(entity);

			if (boss != null)
				found.add(boss);
		}

		return found;
	}

	/**
	 * @param bossName
	 * @return
	 * @see ConfigItems#findItem(String)
	 */
	public static Boss findBoss(@NonNull final String bossName) {
		return loadedBosses.findItem(bossName);
	}

	/**
	 * Find Boss from entity
	 *
	 * @param entity
	 * @return
	 */
	public static SpawnedBoss findBoss(final Entity entity) {
		final String bossName = CompMetadata.getMetadata(entity, NBT_TAG);
		final Boss boss = bossName != null ? findBoss(bossName) : null;

		return boss != null ? new SpawnedBoss(boss, (LivingEntity) entity) : null;
	}

	/**
	 * Find Boss from itemstack
	 *
	 * @param itemStack
	 * @return
	 */
	public static Boss findBoss(final ItemStack itemStack) {
		final String bossName = findBossName(itemStack);

		return bossName != null ? findBoss(bossName) : null;
	}

	/**
	 * Find Boss name from itemstack, regardless if it is installed on server or not
	 *
	 * @param itemStack
	 * @return
	 * @deprecated internal use only
	 */
	@Deprecated
	public static String findBossName(ItemStack itemStack) {
		return CompMetadata.getMetadata(itemStack, NBT_TAG);
	}

	/**
	 * @return
	 * @see ConfigItems#getItems()
	 */
	public static Collection<Boss> getBosses() {
		return loadedBosses.getItems();
	}

	/**
	 * @return
	 * @see ConfigItems#getItemNames()
	 */
	public static Set<String> getBossesNames() {
		return loadedBosses.getItemNames();
	}
}
