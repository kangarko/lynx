package org.mineacademy.lynx.boss;

import org.bukkit.entity.LivingEntity;

import lombok.Data;

@Data
public class SpawnedBoss {
	private final Boss boss;
	private final LivingEntity entity;
}
